<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('active');
            $table->integer('prefix_id')->unsigned();
            $table->foreign('prefix_id')->references('id')->on('phds')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('specialist_id')->unsigned()->default(0)->nullable();
            $table->foreign('specialist_id')->references('id')->on('specialists')->onDelete('cascade');
            $table->integer('sub_specialist_id')->unsigned()->default(0)->nullable();
            $table->foreign('sub_specialist_id')->references('id')->on('sub_specialists')->onDelete('cascade');
            $table->string('name');
            $table->string('lastname');
            $table->string('telephone');
            $table->integer('provinces_id')->unsigned();
            $table->foreign('provinces_id')->references('id')->on('provinces')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('cities_id')->unsigned();
            $table->foreign('cities_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');
            $table->text('info');
            $table->json('offday');
            $table->integer('until');
            $table->integer('limit');
            $table->time('open')->nullable();
            $table->time('close')->nullable();
            $table->float('lat',10,6);
            $table->float('lon',10,6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publics');
    }
}
