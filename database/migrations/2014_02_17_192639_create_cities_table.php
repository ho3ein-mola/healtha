<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num');
            $table->integer('province')->unsigned();
            $table->foreign('province')->references('id')->on('provinces')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->float('latitude',10,6)->nullable();
            $table->float('longitude',10,6)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
