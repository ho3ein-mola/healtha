<?php

use Illuminate\Database\Seeder;

class SubSpeceilistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('sub_specialists')->get()->count() == 0) {
            DB::table('sub_specialists')->insert([
                [
                    'specialist_id' => 9,
                    'name' => "جراحي دست"
                ],
                [
                    'specialist_id' => 9,
                    'name' => "جراحي زانو"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "قلب و عروق"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "غدد داخلی و متابولیسم"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "گوارش و کبد"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "هماتولوژی و انکولوژی"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "نفرولوژی"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "ریه"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "روماتولوژی"
                ],
                [
                    'specialist_id' => 13,
                    'name' => "عفوني وگرمسيري"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "گوارش کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "عفونی کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "ایمونولوژی و آلرژی کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "نفرولوژی کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "متابولیسم کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "قلب کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "هماتولوژی و انکولوژی کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "اعصاب کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "ریه کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "روماتولوژی کودکان"
                ],
                [
                    'specialist_id' => 23,
                    'name' => "متابوليک ارثي"
                ],
                [
                    'specialist_id' => 27,
                    'name' => "الکتروميوگرافي و هدايت عصبي محيطي"
                ],
                [
                    'specialist_id' => 28,
                    'name' => "بيهوشي قلب و مراقبتهاي ويژه"
                ],
                [
                    'specialist_id' => 30,
                    'name' => "جراحي کليه،مجاري ادراري و تناسلي اوروانکولوژي"
                ],
                [
                    'specialist_id' => 33,
                    'name' => "زنان و زايمان جراحي کانسر"
                ],
                [
                    'specialist_id' => 33,
                    'name' => "زنان وزايمان نازايي و آي.وي.اف"
                ],
                [
                    'specialist_id' => 33,
                    'name' => "زنان وزايمان آي.وي.اف و لاپاراسکوپي"
                ],
                [
                    'specialist_id' => 34,
                    'name' => "اتولوژی و نورواتولوژی گوش"
                ],
                [
                    'specialist_id' => 35,
                    'name' => "جراحی قلب و عروق"
                ],
                [
                    'specialist_id' => 35,
                    'name' => "جراحی پلاستیک، ترمیمی و سوختگی"
                ],
                [
                    'specialist_id' => 35,
                    'name' => "جراحی قلب و عروق و پیوند اعضا"
                ],
                [
                    'specialist_id' => 35,
                    'name' => "جراحی کودکان"
                ],
                [
                    'specialist_id' => 35,
                    'name' => "جراحی توراکس قفسه سینه"
                ],
                [
                    'specialist_id' => 35,
                    'name' => "جراحي عروق و تروما"
                ],
            ]);
        } else {
            echo "\e[31mTable is not empty, therefore NOT ";
        }
    }
}
