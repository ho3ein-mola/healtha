<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PhdsTableSeeder::class);
        $this->call(SpecialistTableSeeder::class);
        $this->call(SubSpeceilistTableSeeder::class);
        $this->call(CountyDatabaseSeeder::class);
        $this->call(ProvinceDatabaseSeeder::class);
        $this->call(CityTableSeeder::class);

    }
}
