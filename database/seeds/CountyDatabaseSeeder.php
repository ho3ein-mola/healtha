<?php

use Illuminate\Database\Seeder;

class CountyDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('countries')->get()->count() == 0) {
            DB::table('countries')->insert([
                [
                    'name' => 'ایران',
                    'latitude' => 32.56163037,
                    'longitude' => 54.40429688
                ]
            ]);
        } else {
            echo "\e[31mTable is not empty, therefore NOT ";
        }
    }
}
