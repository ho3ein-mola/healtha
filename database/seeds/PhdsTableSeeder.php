<?php

use Illuminate\Database\Seeder;

class PhdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('phds')->get()->count() == 0) {
            DB::table('phds')->insert([
                ['name' => 'پزشک عمومی'],
                ['name' => 'پزشک متخصص'],
                ['name' => 'پزشک فوق متخصص'],
                ['name' => 'دندانپزشک عمومی'],
                ['name' => 'دندانپزشک متخصص'],
                ['name' => 'روانپزشک عمومی'],
                ['name' => 'روانپزشک متخصص'],
            ]);
        } else {
            echo "\e[31mTable is not empty, therefore NOT ";
        }


    }
}
