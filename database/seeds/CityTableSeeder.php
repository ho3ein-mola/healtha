<?php

use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return vonum
     */
    public function run()
    {
        if (DB::table('cities')->get()->count() == 0) {
            DB::table('cities')->insert([
                        [
                            "num"=>1,
                            "province"=>1,
                            "name"=>" اذرشهر",
                            "latitude"=>37.75888900,
                            "longitude"=>45.97833300
                        ],
                        [
                            "num"=>2,
                            "province"=>1,
                            "name"=>" اسکو",
                            "latitude"=>37.91583300,
                            "longitude"=>46.12361100
                        ],
                        [
                            "num"=>3,
                            "province"=>1,
                            "name"=>" اهر",
                            "latitude"=>38.48943050,
                            "longitude"=>47.06835750
                        ],
                        [
                            "num"=>4,
                            "province"=>1,
                            "name"=>" بستان اباد",
                            "latitude"=>37.85000000,
                            "longitude"=>46.83333300
                        ],
                        [
                            "num"=>5,
                            "province"=>1,
                            "name"=>" بناب",
                            "latitude"=>37.34027800,
                            "longitude"=>46.05611100
                        ],
                        [
                            "num"=>6,
                            "province"=>1,
                            "name"=>" تبریز",
                            "latitude"=>38.06666700,
                            "longitude"=>46.30000000
                        ],
                        [
                            "num"=>7,
                            "province"=>1,
                            "name"=>" جلفا",
                            "latitude"=>38.94027800,
                            "longitude"=>45.63083300
                        ],
                        [
                            "num"=>8,
                            "province"=>1,
                            "name"=>" چار اویماق",
                            "latitude"=>37.12990520,
                            "longitude"=>47.02456860
                        ],
                        [
                            "num"=>9,
                            "province"=>1,
                            "name"=>" سراب",
                            "latitude"=>37.94083300,
                            "longitude"=>47.53666700
                        ],
                        [
                            "num"=>10,
                            "province"=>1,
                            "name"=>" شبستر",
                            "latitude"=>38.18027800,
                            "longitude"=>45.70277800
                        ],
                        [
                            "num"=>11,
                            "province"=>1,
                            "name"=>" عجبشیر",
                            "latitude"=>37.47750000,
                            "longitude"=>45.89416700
                        ],
                        [
                            "num"=>12,
                            "province"=>1,
                            "name"=>" کلیبر",
                            "latitude"=>38.86944400,
                            "longitude"=>47.03555600
                        ],
                        [
                            "num"=>13,
                            "province"=>1,
                            "name"=>" مراغه",
                            "latitude"=>37.38916700,
                            "longitude"=>46.23750000
                        ],
                        [
                            "num"=>14,
                            "province"=>1,
                            "name"=>" مرند",
                            "latitude"=>38.42511700,
                            "longitude"=>45.76963600
                        ],
                        [
                            "num"=>15,
                            "province"=>1,
                            "name"=>" ملکان",
                            "latitude"=>37.14562500,
                            "longitude"=>46.16852420
                        ],
                        [
                            "num"=>16,
                            "province"=>1,
                            "name"=>" میانه",
                            "latitude"=>37.42111100,
                            "longitude"=>47.71500000
                        ],
                        [
                            "num"=>17,
                            "province"=>1,
                            "name"=>" ورزقان",
                            "latitude"=>38.50972200,
                            "longitude"=>46.65444400
                        ],
                        [
                            "num"=>18,
                            "province"=>1,
                            "name"=>" هریس",
                            "latitude"=>29.77518250,
                            "longitude"=>-95.31025050
                        ],
                        [
                            "num"=>19,
                            "province"=>1,
                            "name"=>"هشترود",
                            "latitude"=>37.47777800,
                            "longitude"=>47.05083300
                        ],
                        [
                            "num"=>20,
                            "province"=>2,
                            "name"=>" ارومیه",
                            "latitude"=>37.55527800,
                            "longitude"=>45.07250000
                        ],
                        [
                            "num"=>21,
                            "province"=>2,
                            "name"=>" اشنویه",
                            "latitude"=>37.03972200,
                            "longitude"=>45.09833300
                        ],
                        [
                            "num"=>22,
                            "province"=>2,
                            "name"=>" بوکان",
                            "latitude"=>36.52111100,
                            "longitude"=>46.20888900
                        ],
                        [
                            "num"=>23,
                            "province"=>2,
                            "name"=>" پیرانشهر",
                            "latitude"=>36.69444400,
                            "longitude"=>45.14166700
                        ],
                        [
                            "num"=>24,
                            "province"=>2,
                            "name"=>" تکاب",
                            "latitude"=>36.40083300,
                            "longitude"=>47.11333300
                        ],
                        [
                            "num"=>25,
                            "province"=>2,
                            "name"=>" چالدران",
                            "latitude"=>39.06498370,
                            "longitude"=>44.38446790
                        ],
                        [
                            "num"=>26,
                            "province"=>2,
                            "name"=>" خوی",
                            "latitude"=>38.55027800,
                            "longitude"=>44.95222200
                        ],
                        [
                            "num"=>27,
                            "province"=>2,
                            "name"=>" سردشت",
                            "latitude"=>36.15527800,
                            "longitude"=>45.47888900
                        ],
                        [
                            "num"=>28,
                            "province"=>2,
                            "name"=>" سلماس",
                            "latitude"=>38.19722200,
                            "longitude"=>44.76527800
                        ],
                        [
                            "num"=>29,
                            "province"=>2,
                            "name"=>" شاهین دژ",
                            "latitude"=>36.67916700,
                            "longitude"=>46.56694400
                        ],
                        [
                            "num"=>30,
                            "province"=>2,
                            "name"=>" ماکو",
                            "latitude"=>39.29527800,
                            "longitude"=>44.51666700
                        ],
                        [
                            "num"=>31,
                            "province"=>2,
                            "name"=>" مهاباد",
                            "latitude"=>36.76305600,
                            "longitude"=>45.72222200
                        ],
                        [
                            "num"=>32,
                            "province"=>2,
                            "name"=>" میاندواب",
                            "latitude"=>36.96944400,
                            "longitude"=>46.10277800
                        ],
                        [
                            "num"=>33,
                            "province"=>2,
                            "name"=>" نقده",
                            "latitude"=>36.95527800,
                            "longitude"=>45.38805600
                        ],
                        [
                            "num"=>34,
                            "province"=>3,
                            "name"=>" اردبیل",
                            "latitude"=>38.48532760,
                            "longitude"=>47.89112090
                        ],
                        [
                            "num"=>35,
                            "province"=>3,
                            "name"=>" بیله سوار",
                            "latitude"=>39.35677750,
                            "longitude"=>47.94907650
                        ],
                        [
                            "num"=>36,
                            "province"=>3,
                            "name"=>" پارس اباد",
                            "latitude"=>39.64833300,
                            "longitude"=>47.91750000
                        ],
                        [
                            "num"=>37,
                            "province"=>3,
                            "name"=>" خلخال",
                            "latitude"=>37.61888900,
                            "longitude"=>48.52583300
                        ],
                        [
                            "num"=>38,
                            "province"=>3,
                            "name"=>" کوثر",
                            "latitude"=>31.86768660,
                            "longitude"=>54.33798020
                        ],
                        [
                            "num"=>39,
                            "province"=>3,
                            "name"=>" گرمی",
                            "latitude"=>39.03722670,
                            "longitude"=>47.92770210
                        ],
                        [
                            "num"=>40,
                            "province"=>3,
                            "name"=>" مشگین",
                            "latitude"=>38.39888900,
                            "longitude"=>47.68194400
                        ],
                        [
                            "num"=>41,
                            "province"=>3,
                            "name"=>" نمین",
                            "latitude"=>38.42694400,
                            "longitude"=>48.48388900
                        ],
                        [
                            "num"=>42,
                            "province"=>3,
                            "name"=>" نیر",
                            "latitude"=>38.03472200,
                            "longitude"=>47.99861100
                        ],
                        [
                            "num"=>43,
                            "province"=>4,
                            "name"=>" اران و بیدگل",
                            "latitude"=>34.05777800,
                            "longitude"=>51.48416700
                        ],
                        [
                            "num"=>44,
                            "province"=>4,
                            "name"=>" اردستان",
                            "latitude"=>33.37611100,
                            "longitude"=>52.36944400
                        ],
                        [
                            "num"=>45,
                            "province"=>4,
                            "name"=>" اصفهان",
                            "latitude"=>32.65462750,
                            "longitude"=>51.66798260
                        ],
                        [
                            "num"=>46,
                            "province"=>4,
                            "name"=>" برخوار و میمه",
                            "latitude"=>32.83333300,
                            "longitude"=>51.77500000
                        ],
                        [
                            "num"=>47,
                            "province"=>4,
                            "name"=>" تیران و کرون",
                            "latitude"=>null,
                            "longitude"=>null
                        ],
                        [
                            "num"=>48,
                            "province"=>4,
                            "name"=>" چادگان",
                            "latitude"=>32.76833300,
                            "longitude"=>50.62861100
                        ],
                        [
                            "num"=>49,
                            "province"=>4,
                            "name"=>" خمینی شهر",
                            "latitude"=>32.70027800,
                            "longitude"=>51.52111100
                        ],
                        [
                            "num"=>50,
                            "province"=>4,
                            "name"=>" خوانسار",
                            "latitude"=>33.22055600,
                            "longitude"=>50.31500000
                        ],
                        [
                            "num"=>51,
                            "province"=>4,
                            "name"=>" سمیرم",
                            "latitude"=>31.39883460,
                            "longitude"=>51.56759300
                        ],
                        [
                            "num"=>52,
                            "province"=>4,
                            "name"=>" شاهین شهر و میمه",
                            "latitude"=>33.12718520,
                            "longitude"=>51.51500770
                        ],
                        [
                            "num"=>53,
                            "province"=>4,
                            "name"=>" شهر رضا",
                            "latitude"=>36.29244520,
                            "longitude"=>59.56771490
                        ],
                        [
                            "num"=>54,
                            "province"=>4,
                            "name"=>"دهاقان",
                            "latitude"=>31.94000000,
                            "longitude"=>51.64777800
                        ],
                        [
                            "num"=>55,
                            "province"=>4,
                            "name"=>" فریدن",
                            "latitude"=>33.02148290,
                            "longitude"=>50.30690880
                        ],
                        [
                            "num"=>56,
                            "province"=>4,
                            "name"=>" فریدون شهر",
                            "latitude"=>32.94111100,
                            "longitude"=>50.12111100
                        ],
                        [
                            "num"=>57,
                            "province"=>4,
                            "name"=>" فلاورجان",
                            "latitude"=>32.55527800,
                            "longitude"=>51.50972200
                        ],
                        [
                            "num"=>58,
                            "province"=>4,
                            "name"=>" کاشان",
                            "latitude"=>33.98503580,
                            "longitude"=>51.40996250
                        ],
                        [
                            "num"=>59,
                            "province"=>4,
                            "name"=>" گلپایگان",
                            "latitude"=>33.45361100,
                            "longitude"=>50.28833300
                        ],
                        [
                            "num"=>60,
                            "province"=>4,
                            "name"=>" لنجان",
                            "latitude"=>32.47501680,
                            "longitude"=>51.30508510
                        ],
                        [
                            "num"=>61,
                            "province"=>4,
                            "name"=>" مبارکه",
                            "latitude"=>32.34638900,
                            "longitude"=>51.50444400
                        ],
                        [
                            "num"=>62,
                            "province"=>4,
                            "name"=>" نائین",
                            "latitude"=>32.85994450,
                            "longitude"=>53.08783210
                        ],
                        [
                            "num"=>63,
                            "province"=>4,
                            "name"=>" نجف اباد",
                            "latitude"=>32.63242310,
                            "longitude"=>51.36799140
                        ],
                        [
                            "num"=>64,
                            "province"=>4,
                            "name"=>" نطنز",
                            "latitude"=>33.51333300,
                            "longitude"=>51.91638900
                        ],
                        [
                            "num"=>65,
                            "province"=>5,
                            "name"=>" ساوجبلاق",
                            "latitude"=>38.37879410,
                            "longitude"=>47.49743590
                        ],
                        [
                            "num"=>66,
                            "province"=>5,
                            "name"=>" کرج",
                            "latitude"=>35.84001880,
                            "longitude"=>50.93909060
                        ],
                        [
                            "num"=>67,
                            "province"=>5,
                            "name"=>" نظراباد",
                            "latitude"=>35.95222200,
                            "longitude"=>50.60750000
                        ],
                        [
                            "num"=>68,
                            "province"=>5,
                            "name"=>"طالقان",
                            "latitude"=>33.27291710,
                            "longitude"=>52.19853140
                        ],
                        [
                            "num"=>69,
                            "province"=>6,
                            "name"=>" ابدانان",
                            "latitude"=>32.99250000,
                            "longitude"=>47.41972200
                        ],
                        [
                            "num"=>70,
                            "province"=>6,
                            "name"=>" ایلام",
                            "latitude"=>33.29576180,
                            "longitude"=>46.67053400
                        ],
                        [
                            "num"=>71,
                            "province"=>6,
                            "name"=>" ایوان",
                            "latitude"=>33.82722200,
                            "longitude"=>46.30972200
                        ],
                        [
                            "num"=>72,
                            "province"=>6,
                            "name"=>" دره شهر",
                            "latitude"=>33.13972200,
                            "longitude"=>47.37611100
                        ],
                        [
                            "num"=>73,
                            "province"=>6,
                            "name"=>" دهلران",
                            "latitude"=>32.69416700,
                            "longitude"=>47.26777800
                        ],
                        [
                            "num"=>74,
                            "province"=>6,
                            "name"=>" شیران و چرداول",
                            "latitude"=>33.69383480,
                            "longitude"=>46.74784930
                        ],
                        [
                            "num"=>75,
                            "province"=>6,
                            "name"=>" مهران",
                            "latitude"=>33.12222200,
                            "longitude"=>46.16472200
                        ],
                        [
                            "num"=>76,
                            "province"=>7,
                            "name"=>" بوشهر",
                            "latitude"=>28.92338370,
                            "longitude"=>50.82031400
                        ],
                        [
                            "num"=>77,
                            "province"=>7,
                            "name"=>" تنگستان",
                            "latitude"=>28.98375470,
                            "longitude"=>50.83307080
                        ],
                        [
                            "num"=>78,
                            "province"=>7,
                            "name"=>" جم",
                            "latitude"=>27.82777800,
                            "longitude"=>52.32694400
                        ],
                        [
                            "num"=>79,
                            "province"=>7,
                            "name"=>" دشتستان",
                            "latitude"=>29.26666700,
                            "longitude"=>51.21666700
                        ],
                        [
                            "num"=>80,
                            "province"=>7,
                            "name"=>" دشتی",
                            "latitude"=>35.78451450,
                            "longitude"=>51.43479610
                        ],
                        [
                            "num"=>81,
                            "province"=>7,
                            "name"=>" دیر",
                            "latitude"=>27.84000000,
                            "longitude"=>51.93777800
                        ],
                        [
                            "num"=>82,
                            "province"=>7,
                            "name"=>" دیلم",
                            "latitude"=>30.11826320,
                            "longitude"=>50.22612270
                        ],
                        [
                            "num"=>83,
                            "province"=>7,
                            "name"=>" کنگان",
                            "latitude"=>27.83704370,
                            "longitude"=>52.06454730
                        ],
                        [
                            "num"=>84,
                            "province"=>7,
                            "name"=>" گناوه",
                            "latitude"=>29.57916700,
                            "longitude"=>50.51694400
                        ],
                        [
                            "num"=>85,
                            "province"=>8,
                            "name"=>" اسلام شهر",
                            "latitude"=>35.54458050,
                            "longitude"=>51.23024570
                        ],
                        [
                            "num"=>86,
                            "province"=>8,
                            "name"=>" پاکدشت",
                            "latitude"=>35.46689130,
                            "longitude"=>51.68606250
                        ],
                        [
                            "num"=>87,
                            "province"=>8,
                            "name"=>" تهران",
                            "latitude"=>35.69611100,
                            "longitude"=>51.42305600
                        ],
                        [
                            "num"=>88,
                            "province"=>8,
                            "name"=>" دماوند",
                            "latitude"=>35.94674940,
                            "longitude"=>52.12754810
                        ],
                        [
                            "num"=>89,
                            "province"=>8,
                            "name"=>" رباط کریم",
                            "latitude"=>35.48472200,
                            "longitude"=>51.08277800
                        ],
                        [
                            "num"=>90,
                            "province"=>8,
                            "name"=>" ری",
                            "latitude"=>48.34446630,
                            "longitude"=>-103.16518450
                        ],
                        [
                            "num"=>91,
                            "province"=>8,
                            "name"=>" شمیرانات",
                            "latitude"=>35.95480210,
                            "longitude"=>51.59916430
                        ],
                        [
                            "num"=>92,
                            "province"=>8,
                            "name"=>" شهریار",
                            "latitude"=>35.65972200,
                            "longitude"=>51.05916700
                        ],
                        [
                            "num"=>93,
                            "province"=>8,
                            "name"=>" فیروزکوه",
                            "latitude"=>35.43867100,
                            "longitude"=>60.80938700
                        ],
                        [
                            "num"=>94,
                            "province"=>8,
                            "name"=>" ورامین",
                            "latitude"=>35.32524070,
                            "longitude"=>51.64719870
                        ],
                        [
                            "num"=>95,
                            "province"=>9,
                            "name"=>" اردل",
                            "latitude"=>31.99972200,
                            "longitude"=>50.66166700
                        ],
                        [
                            "num"=>96,
                            "province"=>9,
                            "name"=>" بروجن",
                            "latitude"=>31.96527800,
                            "longitude"=>51.28722200
                        ],
                        [
                            "num"=>97,
                            "province"=>9,
                            "name"=>" شهرکرد",
                            "latitude"=>32.32555600,
                            "longitude"=>50.86444400
                        ],
                        [
                            "num"=>98,
                            "province"=>9,
                            "name"=>" فارسان",
                            "latitude"=>32.25820660,
                            "longitude"=>50.57050880
                        ],
                        [
                            "num"=>99,
                            "province"=>9,
                            "name"=>" کوهرنگ",
                            "latitude"=>32.55583640,
                            "longitude"=>51.67872520
                        ],
                        [
                            "num"=>100,
                            "province"=>9,
                            "name"=>" لردگان",
                            "latitude"=>31.51027800,
                            "longitude"=>50.82944400
                        ],
                        [
                            "num"=>101,
                            "province"=>10,
                            "name"=>" بیرجند",
                            "latitude"=>32.86490390,
                            "longitude"=>59.22624720
                        ],
                        [
                            "num"=>102,
                            "province"=>10,
                            "name"=>" درمیان",
                            "latitude"=>33.03394050,
                            "longitude"=>60.11847970
                        ],
                        [
                            "num"=>103,
                            "province"=>10,
                            "name"=>" سرایان",
                            "latitude"=>33.86027800,
                            "longitude"=>58.52166700
                        ],
                        [
                            "num"=>104,
                            "province"=>10,
                            "name"=>" سر بیشه",
                            "latitude"=>32.57555600,
                            "longitude"=>59.79833300
                        ],
                        [
                            "num"=>105,
                            "province"=>10,
                            "name"=>" فردوس",
                            "latitude"=>34.01861100,
                            "longitude"=>58.17222200
                        ],
                        [
                            "num"=>106,
                            "province"=>10,
                            "name"=>" قائن",
                            "latitude"=>33.72666700,
                            "longitude"=>59.18444400
                        ],
                        [
                            "num"=>107,
                            "province"=>10,
                            "name"=>" نهبندان",
                            "latitude"=>31.54194400,
                            "longitude"=>60.03638900
                        ],
                        [
                            "num"=>108,
                            "province"=>11,
                            "name"=>" برد سکن",
                            "latitude"=>35.26083300,
                            "longitude"=>57.96972200
                        ],
                        [
                            "num"=>109,
                            "province"=>11,
                            "name"=>" بجستان",
                            "latitude"=>34.51638900,
                            "longitude"=>58.18444400
                        ],
                        [
                            "num"=>110,
                            "province"=>11,
                            "name"=>" تایباد",
                            "latitude"=>34.74000000,
                            "longitude"=>60.77555600
                        ],
                        [
                            "num"=>111,
                            "province"=>11,
                            "name"=>" تحت جلگه",
                            "latitude"=>null,
                            "longitude"=>null
                        ],
                        [
                            "num"=>112,
                            "province"=>11,
                            "name"=>" تربت جام",
                            "latitude"=>35.24388900,
                            "longitude"=>60.62250000
                        ],
                        [
                            "num"=>113,
                            "province"=>11,
                            "name"=>" تربت حیدریه",
                            "latitude"=>35.27388900,
                            "longitude"=>59.21944400
                        ],
                        [
                            "num"=>114,
                            "province"=>11,
                            "name"=>" چناران",
                            "latitude"=>36.64555600,
                            "longitude"=>59.12111100
                        ],
                        [
                            "num"=>115,
                            "province"=>11,
                            "name"=>" جغتای",
                            "latitude"=>36.57888530,
                            "longitude"=>57.25121500
                        ],
                        [
                            "num"=>116,
                            "province"=>11,
                            "name"=>" جوین",
                            "latitude"=>36.63622380,
                            "longitude"=>57.50799120
                        ],
                        [
                            "num"=>117,
                            "province"=>11,
                            "name"=>" خلیل اباد",
                            "latitude"=>35.25583300,
                            "longitude"=>58.28638900
                        ],
                        [
                            "num"=>118,
                            "province"=>11,
                            "name"=>" خواف",
                            "latitude"=>34.57638900,
                            "longitude"=>60.14083300
                        ],
                        [
                            "num"=>119,
                            "province"=>11,
                            "name"=>" درگز",
                            "latitude"=>37.44444400,
                            "longitude"=>59.10805600
                        ],
                        [
                            "num"=>120,
                            "province"=>11,
                            "name"=>" رشتخوار",
                            "latitude"=>34.97472200,
                            "longitude"=>59.62361100
                        ],
                        [
                            "num"=>121,
                            "province"=>11,
                            "name"=>" زاوه",
                            "latitude"=>35.27473220,
                            "longitude"=>59.46777270
                        ],
                        [
                            "num"=>122,
                            "province"=>11,
                            "name"=>" سبزوار",
                            "latitude"=>36.21518230,
                            "longitude"=>57.66782280
                        ],
                        [
                            "num"=>123,
                            "province"=>11,
                            "name"=>" سرخس",
                            "latitude"=>36.54500000,
                            "longitude"=>61.15777800
                        ],
                        [
                            "num"=>124,
                            "province"=>11,
                            "name"=>" فریمان",
                            "latitude"=>35.70694400,
                            "longitude"=>59.85000000
                        ],
                        [
                            "num"=>125,
                            "province"=>11,
                            "name"=>" قوچان",
                            "latitude"=>37.10611100,
                            "longitude"=>58.50944400
                        ],
                        [
                            "num"=>126,
                            "province"=>11,
                            "name"=>"طرقبه شاندیز",
                            "latitude"=>36.35488410,
                            "longitude"=>59.43839550
                        ],
                        [
                            "num"=>127,
                            "province"=>11,
                            "name"=>" کاشمر",
                            "latitude"=>35.23833300,
                            "longitude"=>58.46555600
                        ],
                        [
                            "num"=>128,
                            "province"=>11,
                            "name"=>" کلات",
                            "latitude"=>34.19833300,
                            "longitude"=>58.54444400
                        ],
                        [
                            "num"=>129,
                            "province"=>11,
                            "name"=>" گناباد",
                            "latitude"=>34.35277800,
                            "longitude"=>58.68361100
                        ],
                        [
                            "num"=>130,
                            "province"=>11,
                            "name"=>" مشهد",
                            "latitude"=>36.26046230,
                            "longitude"=>59.61675490
                        ],
                        [
                            "num"=>131,
                            "province"=>11,
                            "name"=>" مه ولات",
                            "latitude"=>35.02108290,
                            "longitude"=>58.78181160
                        ],
                        [
                            "num"=>132,
                            "province"=>11,
                            "name"=>" نیشابور",
                            "latitude"=>36.21408650,
                            "longitude"=>58.79609150
                        ],
                        [
                            "num"=>133,
                            "province"=>12,
                            "name"=>" اسفراین",
                            "latitude"=>37.07638900,
                            "longitude"=>57.51000000
                        ],
                        [
                            "num"=>134,
                            "province"=>12,
                            "name"=>" بجنورد",
                            "latitude"=>37.47500000,
                            "longitude"=>57.33333300
                        ],
                        [
                            "num"=>135,
                            "province"=>12,
                            "name"=>" جاجرم",
                            "latitude"=>36.95000000,
                            "longitude"=>56.38000000
                        ],
                        [
                            "num"=>136,
                            "province"=>12,
                            "name"=>" شیروان",
                            "latitude"=>37.40923570,
                            "longitude"=>57.92761840
                        ],
                        [
                            "num"=>137,
                            "province"=>12,
                            "name"=>" فاروج",
                            "latitude"=>37.23111100,
                            "longitude"=>58.21888900
                        ],
                        [
                            "num"=>138,
                            "province"=>12,
                            "name"=>" مانه و سملقان",
                            "latitude"=>37.66206140,
                            "longitude"=>56.74120700
                        ],
                        [
                            "num"=>139,
                            "province"=>13,
                            "name"=>" ابادان",
                            "latitude"=>30.34729600,
                            "longitude"=>48.29340040
                        ],
                        [
                            "num"=>140,
                            "province"=>13,
                            "name"=>" امیدیه",
                            "latitude"=>30.74583300,
                            "longitude"=>49.70861100
                        ],
                        [
                            "num"=>141,
                            "province"=>13,
                            "name"=>" اندیمشک",
                            "latitude"=>32.46000000,
                            "longitude"=>48.35916700
                        ],
                        [
                            "num"=>142,
                            "province"=>13,
                            "name"=>" اهواز",
                            "latitude"=>31.31832720,
                            "longitude"=>48.67061870
                        ],
                        [
                            "num"=>143,
                            "province"=>13,
                            "name"=>" ایذه",
                            "latitude"=>31.83416700,
                            "longitude"=>49.86722200
                        ],
                        [
                            "num"=>144,
                            "province"=>13,
                            "name"=>" باغ ملک",
                            "latitude"=>32.39472060,
                            "longitude"=>51.59653280
                        ],
                        [
                            "num"=>145,
                            "province"=>13,
                            "name"=>" بندرماهشهر",
                            "latitude"=>30.55888900,
                            "longitude"=>49.19805600
                        ],
                        [
                            "num"=>146,
                            "province"=>13,
                            "name"=>" بهبهان",
                            "latitude"=>30.59583300,
                            "longitude"=>50.24166700
                        ],
                        [
                            "num"=>147,
                            "province"=>13,
                            "name"=>" خرمشهر",
                            "latitude"=>30.42562190,
                            "longitude"=>48.18911850
                        ],
                        [
                            "num"=>148,
                            "province"=>13,
                            "name"=>" دزفول",
                            "latitude"=>32.38307770,
                            "longitude"=>48.42358410
                        ],
                        [
                            "num"=>149,
                            "province"=>13,
                            "name"=>" دشت ازادگان",
                            "latitude"=>31.55805600,
                            "longitude"=>48.18083300
                        ],
                        [
                            "num"=>150,
                            "province"=>13,
                            "name"=>" رامشیر",
                            "latitude"=>30.89565210,
                            "longitude"=>49.40947010
                        ],
                        [
                            "num"=>151,
                            "province"=>13,
                            "name"=>" رامهرمز",
                            "latitude"=>31.28000000,
                            "longitude"=>49.60361100
                        ],
                        [
                            "num"=>152,
                            "province"=>13,
                            "name"=>" شادگان",
                            "latitude"=>30.64972200,
                            "longitude"=>48.66472200
                        ],
                        [
                            "num"=>153,
                            "province"=>13,
                            "name"=>" شوش",
                            "latitude"=>32.19416700,
                            "longitude"=>48.24361100
                        ],
                        [
                            "num"=>154,
                            "province"=>13,
                            "name"=>" شوشتر",
                            "latitude"=>32.04555600,
                            "longitude"=>48.85666700
                        ],
                        [
                            "num"=>155,
                            "province"=>13,
                            "name"=>" گتوند",
                            "latitude"=>32.25138900,
                            "longitude"=>48.81611100
                        ],
                        [
                            "num"=>156,
                            "province"=>13,
                            "name"=>" لالی",
                            "latitude"=>32.32888900,
                            "longitude"=>49.09361100
                        ],
                        [
                            "num"=>157,
                            "province"=>13,
                            "name"=>" مسجد سلیمان",
                            "latitude"=>31.93638900,
                            "longitude"=>49.30388900
                        ],
                        [
                            "num"=>158,
                            "province"=>13,
                            "name"=>" هندیجان",
                            "latitude"=>30.23638900,
                            "longitude"=>49.71194400
                        ],
                        [
                            "num"=>159,
                            "province"=>14,
                            "name"=>" ابهر",
                            "latitude"=>36.14666700,
                            "longitude"=>49.21805600
                        ],
                        [
                            "num"=>160,
                            "province"=>14,
                            "name"=>" ایجرود",
                            "latitude"=>36.41609280,
                            "longitude"=>48.24692490
                        ],
                        [
                            "num"=>161,
                            "province"=>14,
                            "name"=>" خدابنده",
                            "latitude"=>36.11472200,
                            "longitude"=>48.59111100
                        ],
                        [
                            "num"=>162,
                            "province"=>14,
                            "name"=>" خرمدره",
                            "latitude"=>36.20305600,
                            "longitude"=>49.18694400
                        ],
                        [
                            "num"=>163,
                            "province"=>14,
                            "name"=>" زنجان",
                            "latitude"=>36.50181850,
                            "longitude"=>48.39881860
                        ],
                        [
                            "num"=>164,
                            "province"=>14,
                            "name"=>" طارم",
                            "latitude"=>28.18042870,
                            "longitude"=>55.74533670
                        ],
                        [
                            "num"=>165,
                            "province"=>14,
                            "name"=>" ماه نشان",
                            "latitude"=>36.74444400,
                            "longitude"=>47.67250000
                        ],
                        [
                            "num"=>166,
                            "province"=>15,
                            "name"=>" دامغان",
                            "latitude"=>36.16833300,
                            "longitude"=>54.34805600
                        ],
                        [
                            "num"=>167,
                            "province"=>15,
                            "name"=>" سمنان",
                            "latitude"=>35.22555850,
                            "longitude"=>54.43421380
                        ],
                        [
                            "num"=>168,
                            "province"=>15,
                            "name"=>" شاهرود",
                            "latitude"=>36.41805600,
                            "longitude"=>54.97638900
                        ],
                        [
                            "num"=>169,
                            "province"=>15,
                            "name"=>" گرمسار",
                            "latitude"=>35.21833300,
                            "longitude"=>52.34083300
                        ],
                        [
                            "num"=>170,
                            "province"=>15,
                            "name"=>" مهدی شهر",
                            "latitude"=>35.70000000,
                            "longitude"=>53.35000000
                        ],
                        [
                            "num"=>171,
                            "province"=>16,
                            "name"=>" ایرانشهر",
                            "latitude"=>27.20250000,
                            "longitude"=>60.68472200
                        ],
                        [
                            "num"=>172,
                            "province"=>16,
                            "name"=>" چابهار",
                            "latitude"=>25.29194400,
                            "longitude"=>60.64305600
                        ],
                        [
                            "num"=>173,
                            "province"=>16,
                            "name"=>" خاش",
                            "latitude"=>28.21666700,
                            "longitude"=>61.20000000
                        ],
                        [
                            "num"=>174,
                            "province"=>16,
                            "name"=>" دلگان",
                            "latitude"=>27.60773570,
                            "longitude"=>59.47209040
                        ],
                        [
                            "num"=>175,
                            "province"=>16,
                            "name"=>" زابل",
                            "latitude"=>31.02861100,
                            "longitude"=>61.50111100
                        ],
                        [
                            "num"=>176,
                            "province"=>16,
                            "name"=>" زاهدان",
                            "latitude"=>29.49638900,
                            "longitude"=>60.86277800
                        ],
                        [
                            "num"=>177,
                            "province"=>16,
                            "name"=>" زهک",
                            "latitude"=>30.89388900,
                            "longitude"=>61.68027800
                        ],
                        [
                            "num"=>178,
                            "province"=>16,
                            "name"=>" سراوان",
                            "latitude"=>27.37083300,
                            "longitude"=>62.33416700
                        ],
                        [
                            "num"=>179,
                            "province"=>16,
                            "name"=>" سرباز",
                            "latitude"=>26.63083300,
                            "longitude"=>61.25638900
                        ],
                        [
                            "num"=>180,
                            "province"=>16,
                            "name"=>" کنارک",
                            "latitude"=>25.36027800,
                            "longitude"=>60.39944400
                        ],
                        [
                            "num"=>181,
                            "province"=>16,
                            "name"=>" نیکشهر",
                            "latitude"=>26.41847190,
                            "longitude"=>60.21107520
                        ],
                        [
                            "num"=>182,
                            "province"=>17,
                            "name"=>" اباده",
                            "latitude"=>31.16083300,
                            "longitude"=>52.65055600
                        ],
                        [
                            "num"=>183,
                            "province"=>17,
                            "name"=>" ارسنجان",
                            "latitude"=>29.91250000,
                            "longitude"=>53.30861100
                        ],
                        [
                            "num"=>184,
                            "province"=>17,
                            "name"=>" استهبان",
                            "latitude"=>29.12666700,
                            "longitude"=>54.04222200
                        ],
                        [
                            "num"=>185,
                            "province"=>17,
                            "name"=>" اقلید",
                            "latitude"=>30.89888900,
                            "longitude"=>52.68666700
                        ],
                        [
                            "num"=>186,
                            "province"=>17,
                            "name"=>" بوانات",
                            "latitude"=>30.48559070,
                            "longitude"=>53.59333040
                        ],
                        [
                            "num"=>187,
                            "province"=>17,
                            "name"=>" پاسارگاد",
                            "latitude"=>30.20330750,
                            "longitude"=>53.17901000
                        ],
                        [
                            "num"=>188,
                            "province"=>17,
                            "name"=>" جهرم",
                            "latitude"=>28.50000000,
                            "longitude"=>53.56055600
                        ],
                        [
                            "num"=>189,
                            "province"=>17,
                            "name"=>" خرم بید",
                            "latitude"=>32.67083450,
                            "longitude"=>51.64702790
                        ],
                        [
                            "num"=>190,
                            "province"=>17,
                            "name"=>" خنج",
                            "latitude"=>27.89138900,
                            "longitude"=>53.43444400
                        ],
                        [
                            "num"=>191,
                            "province"=>17,
                            "name"=>" داراب",
                            "latitude"=>28.75194400,
                            "longitude"=>54.54444400
                        ],
                        [
                            "num"=>192,
                            "province"=>17,
                            "name"=>" زرین دشت",
                            "latitude"=>28.35450470,
                            "longitude"=>54.41780060
                        ],
                        [
                            "num"=>193,
                            "province"=>17,
                            "name"=>" سپیدان",
                            "latitude"=>30.24252820,
                            "longitude"=>51.99241850
                        ],
                        [
                            "num"=>194,
                            "province"=>17,
                            "name"=>" شیراز",
                            "latitude"=>29.59176770,
                            "longitude"=>52.58369820
                        ],
                        [
                            "num"=>195,
                            "province"=>17,
                            "name"=>" فراشبند",
                            "latitude"=>28.87138900,
                            "longitude"=>52.09166700
                        ],
                        [
                            "num"=>196,
                            "province"=>17,
                            "name"=>" فسا",
                            "latitude"=>28.93833300,
                            "longitude"=>53.64833300
                        ],
                        [
                            "num"=>197,
                            "province"=>17,
                            "name"=>" فیروزاباد",
                            "latitude"=>28.84388900,
                            "longitude"=>52.57083300
                        ],
                        [
                            "num"=>198,
                            "province"=>17,
                            "name"=>" قیر و کارزین",
                            "latitude"=>28.42998000,
                            "longitude"=>53.09516000
                        ],
                        [
                            "num"=>199,
                            "province"=>17,
                            "name"=>" کازرون",
                            "latitude"=>29.61944400,
                            "longitude"=>51.65416700
                        ],
                        [
                            "num"=>200,
                            "province"=>17,
                            "name"=>" لارستان",
                            "latitude"=>27.68111100,
                            "longitude"=>54.34027800
                        ],
                        [
                            "num"=>201,
                            "province"=>17,
                            "name"=>" لامرد",
                            "latitude"=>27.34237710,
                            "longitude"=>53.18035580
                        ],
                        [
                            "num"=>202,
                            "province"=>17,
                            "name"=>" مرودشت",
                            "latitude"=>29.87416700,
                            "longitude"=>52.80250000
                        ],
                        [
                            "num"=>203,
                            "province"=>17,
                            "name"=>" ممسنی",
                            "latitude"=>31.96003450,
                            "longitude"=>50.51226520
                        ],
                        [
                            "num"=>204,
                            "province"=>17,
                            "name"=>" مهر",
                            "latitude"=>27.55599310,
                            "longitude"=>52.88472050
                        ],
                        [
                            "num"=>205,
                            "province"=>17,
                            "name"=>" نی ریز",
                            "latitude"=>29.19888900,
                            "longitude"=>54.32777800
                        ],
                        [
                            "num"=>206,
                            "province"=>18,
                            "name"=>" ابیک",
                            "latitude"=>36.04000000,
                            "longitude"=>50.53111100
                        ],
                        [
                            "num"=>207,
                            "province"=>18,
                            "name"=>" البرز",
                            "latitude"=>35.99604670,
                            "longitude"=>50.92892460
                        ],
                        [
                            "num"=>208,
                            "province"=>18,
                            "name"=>" بوئین زهرا",
                            "latitude"=>35.76694400,
                            "longitude"=>50.05777800
                        ],
                        [
                            "num"=>209,
                            "province"=>18,
                            "name"=>" تاکستان",
                            "latitude"=>36.06972200,
                            "longitude"=>49.69583300
                        ],
                        [
                            "num"=>210,
                            "province"=>18,
                            "name"=>" قزوین",
                            "latitude"=>36.08813170,
                            "longitude"=>49.85472660
                        ],
                        [
                            "num"=>211,
                            "province"=>19,
                            "name"=>" قم",
                            "latitude"=>34.63994430,
                            "longitude"=>50.87594190
                        ],
                        [
                            "num"=>212,
                            "province"=>20,
                            "name"=>" بانه",
                            "latitude"=>35.99859990,
                            "longitude"=>45.88234280
                        ],
                        [
                            "num"=>213,
                            "province"=>20,
                            "name"=>" بیجار",
                            "latitude"=>32.73527800,
                            "longitude"=>59.46666700
                        ],
                        [
                            "num"=>214,
                            "province"=>20,
                            "name"=>" دیواندره",
                            "latitude"=>35.91388900,
                            "longitude"=>47.02388900
                        ],
                        [
                            "num"=>215,
                            "province"=>20,
                            "name"=>" سرواباد",
                            "latitude"=>35.31250000,
                            "longitude"=>46.36694400
                        ],
                        [
                            "num"=>216,
                            "province"=>20,
                            "name"=>" سقز",
                            "latitude"=>36.24638900,
                            "longitude"=>46.26638900
                        ],
                        [
                            "num"=>217,
                            "province"=>20,
                            "name"=>" سنندج",
                            "latitude"=>35.32187480,
                            "longitude"=>46.98616470
                        ],
                        [
                            "num"=>218,
                            "province"=>20,
                            "name"=>" قروه",
                            "latitude"=>35.16789340,
                            "longitude"=>47.80382720
                        ],
                        [
                            "num"=>219,
                            "province"=>20,
                            "name"=>" کامیاران",
                            "latitude"=>34.79555600,
                            "longitude"=>46.93555600
                        ],
                        [
                            "num"=>220,
                            "province"=>20,
                            "name"=>" مریوان",
                            "latitude"=>35.52694400,
                            "longitude"=>46.17638900
                        ],
                        [
                            "num"=>221,
                            "province"=>21,
                            "name"=>" بافت",
                            "latitude"=>29.23305600,
                            "longitude"=>56.60222200
                        ],
                        [
                            "num"=>222,
                            "province"=>21,
                            "name"=>" بردسیر",
                            "latitude"=>29.92750000,
                            "longitude"=>56.57222200
                        ],
                        [
                            "num"=>223,
                            "province"=>21,
                            "name"=>" بم",
                            "latitude"=>29.10611100,
                            "longitude"=>58.35694400
                        ],
                        [
                            "num"=>224,
                            "province"=>21,
                            "name"=>" جیرفت",
                            "latitude"=>28.67511240,
                            "longitude"=>57.73715690
                        ],
                        [
                            "num"=>225,
                            "province"=>21,
                            "name"=>" راور",
                            "latitude"=>31.26555600,
                            "longitude"=>56.80555600
                        ],
                        [
                            "num"=>226,
                            "province"=>21,
                            "name"=>" رفسنجان",
                            "latitude"=>30.40666700,
                            "longitude"=>55.99388900
                        ],
                        [
                            "num"=>227,
                            "province"=>21,
                            "name"=>" رودبار جنوب",
                            "latitude"=>36.82412890,
                            "longitude"=>49.42372740
                        ],
                        [
                            "num"=>228,
                            "province"=>21,
                            "name"=>" زرند",
                            "latitude"=>30.81277800,
                            "longitude"=>56.56388900
                        ],
                        [
                            "num"=>229,
                            "province"=>21,
                            "name"=>" سیرجان",
                            "latitude"=>29.45866760,
                            "longitude"=>55.67140510
                        ],
                        [
                            "num"=>230,
                            "province"=>21,
                            "name"=>" شهر بابک",
                            "latitude"=>30.11638900,
                            "longitude"=>55.11861100
                        ],
                        [
                            "num"=>231,
                            "province"=>21,
                            "name"=>" عنبراباد",
                            "latitude"=>28.47833330,
                            "longitude"=>57.84138890
                        ],
                        [
                            "num"=>232,
                            "province"=>21,
                            "name"=>" قلعه گنج",
                            "latitude"=>27.52361100,
                            "longitude"=>57.88111100
                        ],
                        [
                            "num"=>233,
                            "province"=>21,
                            "name"=>" کرمان",
                            "latitude"=>29.48500890,
                            "longitude"=>57.64390480
                        ],
                        [
                            "num"=>234,
                            "province"=>21,
                            "name"=>" کوهبنان",
                            "latitude"=>31.41027800,
                            "longitude"=>56.28250000
                        ],
                        [
                            "num"=>235,
                            "province"=>21,
                            "name"=>" کهنوج",
                            "latitude"=>27.94676030,
                            "longitude"=>57.70625720
                        ],
                        [
                            "num"=>236,
                            "province"=>21,
                            "name"=>" منوجان",
                            "latitude"=>27.44756260,
                            "longitude"=>57.50516160
                        ],
                        [
                            "num"=>237,
                            "province"=>22,
                            "name"=>" اسلام اباد غرب",
                            "latitude"=>33.72938820,
                            "longitude"=>73.09314610
                        ],
                        [
                            "num"=>238,
                            "province"=>22,
                            "name"=>" پاوه",
                            "latitude"=>35.04333300,
                            "longitude"=>46.35638900
                        ],
                        [
                            "num"=>239,
                            "province"=>22,
                            "name"=>" ثلاث باباجانی",
                            "latitude"=>34.73583710,
                            "longitude"=>46.14939690
                        ],
                        [
                            "num"=>240,
                            "province"=>22,
                            "name"=>" جوانرود",
                            "latitude"=>34.80666700,
                            "longitude"=>46.48861100
                        ],
                        [
                            "num"=>241,
                            "province"=>22,
                            "name"=>" دالاهو",
                            "latitude"=>34.28416700,
                            "longitude"=>46.24222200
                        ],
                        [
                            "num"=>242,
                            "province"=>22,
                            "name"=>" روانسر",
                            "latitude"=>34.71208920,
                            "longitude"=>46.65129980
                        ],
                        [
                            "num"=>243,
                            "province"=>22,
                            "name"=>" سرپل ذهاب",
                            "latitude"=>34.46111100,
                            "longitude"=>45.86277800
                        ],
                        [
                            "num"=>244,
                            "province"=>22,
                            "name"=>" سنقر",
                            "latitude"=>34.78361100,
                            "longitude"=>47.60027800
                        ],
                        [
                            "num"=>245,
                            "province"=>22,
                            "name"=>" صحنه",
                            "latitude"=>34.48138900,
                            "longitude"=>47.69083300
                        ],
                        [
                            "num"=>246,
                            "province"=>22,
                            "name"=>" قصر شیرین",
                            "latitude"=>34.51590310,
                            "longitude"=>45.57768590
                        ],
                        [
                            "num"=>247,
                            "province"=>22,
                            "name"=>" کرمانشاه",
                            "latitude"=>34.45762330,
                            "longitude"=>46.67053400
                        ],
                        [
                            "num"=>248,
                            "province"=>22,
                            "name"=>" کنگاور",
                            "latitude"=>34.50416700,
                            "longitude"=>47.96527800
                        ],
                        [
                            "num"=>249,
                            "province"=>22,
                            "name"=>" گیلان غرب",
                            "latitude"=>34.14222200,
                            "longitude"=>45.92027800
                        ],
                        [
                            "num"=>250,
                            "province"=>22,
                            "name"=>" هرسین",
                            "latitude"=>34.27191490,
                            "longitude"=>47.60461830
                        ],
                        [
                            "num"=>251,
                            "province"=>23,
                            "name"=>" بویر احمد",
                            "latitude"=>30.72458600,
                            "longitude"=>50.84563230
                        ],
                        [
                            "num"=>252,
                            "province"=>23,
                            "name"=>" بهمئی",
                            "latitude"=>null,
                            "longitude"=>null
                        ],
                        [
                            "num"=>253,
                            "province"=>23,
                            "name"=>" دنا",
                            "latitude"=>30.95166660,
                            "longitude"=>51.43750000
                        ],
                        [
                            "num"=>254,
                            "province"=>23,
                            "name"=>" کهگیلویه",
                            "latitude"=>null,
                            "longitude"=>null
                        ],
                        [
                            "num"=>255,
                            "province"=>23,
                            "name"=>" گچساران",
                            "latitude"=>30.35000000,
                            "longitude"=>50.80000000
                        ],
                        [
                            "num"=>256,
                            "province"=>24,
                            "name"=>" ازادشهر",
                            "latitude"=>37.08694400,
                            "longitude"=>55.17388900
                        ],
                        [
                            "num"=>257,
                            "province"=>24,
                            "name"=>" اق قلا",
                            "latitude"=>37.01388900,
                            "longitude"=>54.45500000
                        ],
                        [
                            "num"=>258,
                            "province"=>24,
                            "name"=>" بندر گز",
                            "latitude"=>36.77496500,
                            "longitude"=>53.94617490
                        ],
                        [
                            "num"=>259,
                            "province"=>24,
                            "name"=>" بندر ترکمن",
                            "latitude"=>36.90166700,
                            "longitude"=>54.07083300
                        ],
                        [
                            "num"=>260,
                            "province"=>24,
                            "name"=>" رامیان",
                            "latitude"=>37.01611100,
                            "longitude"=>55.14111100
                        ],
                        [
                            "num"=>261,
                            "province"=>24,
                            "name"=>" علی اباد",
                            "latitude"=>36.30822600,
                            "longitude"=>74.61954740
                        ],
                        [
                            "num"=>262,
                            "province"=>24,
                            "name"=>" کرد کوی",
                            "latitude"=>36.79414260,
                            "longitude"=>54.11027400
                        ],
                        [
                            "num"=>263,
                            "province"=>24,
                            "name"=>" کلاله",
                            "latitude"=>37.38083300,
                            "longitude"=>55.49166700
                        ],
                        [
                            "num"=>264,
                            "province"=>24,
                            "name"=>" گرگان",
                            "latitude"=>36.84564270,
                            "longitude"=>54.43933630
                        ],
                        [
                            "num"=>265,
                            "province"=>24,
                            "name"=>" گنبد کاووس",
                            "latitude"=>37.25000000,
                            "longitude"=>55.16722200
                        ],
                        [
                            "num"=>266,
                            "province"=>24,
                            "name"=>" مینو دشت",
                            "latitude"=>37.22888900,
                            "longitude"=>55.37472200
                        ],
                        [
                            "num"=>267,
                            "province"=>25,
                            "name"=>" استارا",
                            "latitude"=>38.42916700,
                            "longitude"=>48.87194400
                        ],
                        [
                            "num"=>268,
                            "province"=>25,
                            "name"=>" استانه اشرفیه",
                            "latitude"=>37.25980220,
                            "longitude"=>49.94366210
                        ],
                        [
                            "num"=>269,
                            "province"=>25,
                            "name"=>" املش",
                            "latitude"=>37.09163340,
                            "longitude"=>50.18693770
                        ],
                        [
                            "num"=>270,
                            "province"=>25,
                            "name"=>" بندر انزلی",
                            "latitude"=>37.47244670,
                            "longitude"=>49.45873120
                        ],
                        [
                            "num"=>271,
                            "province"=>25,
                            "name"=>" رشت",
                            "latitude"=>37.28083300,
                            "longitude"=>49.58305600
                        ],
                        [
                            "num"=>272,
                            "province"=>25,
                            "name"=>" رضوانشهر",
                            "latitude"=>37.55067500,
                            "longitude"=>49.14098010
                        ],
                        [
                            "num"=>273,
                            "province"=>25,
                            "name"=>" رودبار",
                            "latitude"=>36.82412890,
                            "longitude"=>49.42372740
                        ],
                        [
                            "num"=>274,
                            "province"=>25,
                            "name"=>" رودسر",
                            "latitude"=>37.13784150,
                            "longitude"=>50.28361990
                        ],
                        [
                            "num"=>275,
                            "province"=>25,
                            "name"=>" سیاهکل",
                            "latitude"=>37.15277800,
                            "longitude"=>49.87083300
                        ],
                        [
                            "num"=>276,
                            "province"=>25,
                            "name"=>" شفت",
                            "latitude"=>39.63063100,
                            "longitude"=>-78.92954200
                        ],
                        [
                            "num"=>277,
                            "province"=>25,
                            "name"=>" صومعه سرا",
                            "latitude"=>37.31166700,
                            "longitude"=>49.32194400
                        ],
                        [
                            "num"=>278,
                            "province"=>25,
                            "name"=>" طوالش",
                            "latitude"=>37.00000000,
                            "longitude"=>48.42222220
                        ],
                        [
                            "num"=>279,
                            "province"=>25,
                            "name"=>" فومن",
                            "latitude"=>37.22388900,
                            "longitude"=>49.31250000
                        ],
                        [
                            "num"=>280,
                            "province"=>25,
                            "name"=>" لاهیجان",
                            "latitude"=>37.20722200,
                            "longitude"=>50.00388900
                        ],
                        [
                            "num"=>281,
                            "province"=>25,
                            "name"=>" لنگرود",
                            "latitude"=>37.19694400,
                            "longitude"=>50.15361100
                        ],
                        [
                            "num"=>282,
                            "province"=>25,
                            "name"=>" ماسال",
                            "latitude"=>37.36211850,
                            "longitude"=>49.13147690
                        ],
                        [
                            "num"=>283,
                            "province"=>26,
                            "name"=>" ازنا",
                            "latitude"=>33.45583300,
                            "longitude"=>49.45555600
                        ],
                        [
                            "num"=>284,
                            "province"=>26,
                            "name"=>" الیگودرز",
                            "latitude"=>33.40055600,
                            "longitude"=>49.69500000
                        ],
                        [
                            "num"=>285,
                            "province"=>26,
                            "name"=>" بروجرد",
                            "latitude"=>33.89419930,
                            "longitude"=>48.76703300
                        ],
                        [
                            "num"=>286,
                            "province"=>26,
                            "name"=>" پلدختر",
                            "latitude"=>33.15361100,
                            "longitude"=>47.71361100
                        ],
                        [
                            "num"=>287,
                            "province"=>26,
                            "name"=>" خرم اباد",
                            "latitude"=>33.48777800,
                            "longitude"=>48.35583300
                        ],
                        [
                            "num"=>288,
                            "province"=>26,
                            "name"=>" دورود",
                            "latitude"=>33.49550280,
                            "longitude"=>49.06317430
                        ],
                        [
                            "num"=>289,
                            "province"=>17,
                            "name"=>" لامرد",
                            "latitude"=>27.34237710,
                            "longitude"=>53.18035580
                        ],
                        [
                            "num"=>290,
                            "province"=>17,
                            "name"=>" مرودشت",
                            "latitude"=>29.87416700,
                            "longitude"=>52.80250000
                        ],
                        [
                            "num"=>291,
                            "province"=>17,
                            "name"=>" ممسنی",
                            "latitude"=>31.96003450,
                            "longitude"=>50.51226520
                        ],
                        [
                            "num"=>292,
                            "province"=>17,
                            "name"=>" مهر",
                            "latitude"=>27.55599310,
                            "longitude"=>52.88472050
                        ],
                        [
                            "num"=>293,
                            "province"=>17,
                            "name"=>" نی ریز",
                            "latitude"=>29.19888900,
                            "longitude"=>54.32777800
                        ],
                        [
                            "num"=>294,
                            "province"=>18,
                            "name"=>" ابیک",
                            "latitude"=>36.04000000,
                            "longitude"=>50.53111100
                        ],
                        [
                            "num"=>295,
                            "province"=>18,
                            "name"=>" البرز",
                            "latitude"=>35.99604670,
                            "longitude"=>50.92892460
                        ],
                        [
                            "num"=>296,
                            "province"=>18,
                            "name"=>" بوئین زهرا",
                            "latitude"=>35.76694400,
                            "longitude"=>50.05777800
                        ],
                        [
                            "num"=>297,
                            "province"=>18,
                            "name"=>" تاکستان",
                            "latitude"=>36.06972200,
                            "longitude"=>49.69583300
                        ],
                        [
                            "num"=>298,
                            "province"=>18,
                            "name"=>" قزوین",
                            "latitude"=>36.08813170,
                            "longitude"=>49.85472660
                        ],
                        [
                            "num"=>299,
                            "province"=>19,
                            "name"=>" قم",
                            "latitude"=>34.63994430,
                            "longitude"=>50.87594190
                        ],
                        [
                            "num"=>300,
                            "province"=>20,
                            "name"=>" بانه",
                            "latitude"=>35.99859990,
                            "longitude"=>45.88234280
                        ],
                        [
                            "num"=>301,
                            "province"=>20,
                            "name"=>" بیجار",
                            "latitude"=>32.73527800,
                            "longitude"=>59.46666700
                        ],
                        [
                            "num"=>302,
                            "province"=>20,
                            "name"=>" دیواندره",
                            "latitude"=>35.91388900,
                            "longitude"=>47.02388900
                        ],
                        [
                            "num"=>303,
                            "province"=>20,
                            "name"=>" سرواباد",
                            "latitude"=>35.31250000,
                            "longitude"=>46.36694400
                        ],
                        [
                            "num"=>304,
                            "province"=>20,
                            "name"=>" سقز",
                            "latitude"=>36.24638900,
                            "longitude"=>46.26638900
                        ],
                        [
                            "num"=>305,
                            "province"=>20,
                            "name"=>" سنندج",
                            "latitude"=>35.32187480,
                            "longitude"=>46.98616470
                        ],
                        [
                            "num"=>306,
                            "province"=>20,
                            "name"=>" قروه",
                            "latitude"=>35.16789340,
                            "longitude"=>47.80382720
                        ],
                        [
                            "num"=>307,
                            "province"=>20,
                            "name"=>" کامیاران",
                            "latitude"=>34.79555600,
                            "longitude"=>46.93555600
                        ],
                        [
                            "num"=>308,
                            "province"=>20,
                            "name"=>" مریوان",
                            "latitude"=>35.52694400,
                            "longitude"=>46.17638900
                        ],
                        [
                            "num"=>309,
                            "province"=>21,
                            "name"=>" بافت",
                            "latitude"=>29.23305600,
                            "longitude"=>56.60222200
                        ],
                        [
                            "num"=>310,
                            "province"=>21,
                            "name"=>" بردسیر",
                            "latitude"=>29.92750000,
                            "longitude"=>56.57222200
                        ],
                        [
                            "num"=>311,
                            "province"=>21,
                            "name"=>" بم",
                            "latitude"=>29.10611100,
                            "longitude"=>58.35694400
                        ],
                        [
                            "num"=>312,
                            "province"=>21,
                            "name"=>" جیرفت",
                            "latitude"=>28.67511240,
                            "longitude"=>57.73715690
                        ],
                        [
                            "num"=>313,
                            "province"=>21,
                            "name"=>" راور",
                            "latitude"=>31.26555600,
                            "longitude"=>56.80555600
                        ],
                        [
                            "num"=>314,
                            "province"=>21,
                            "name"=>" رفسنجان",
                            "latitude"=>30.40666700,
                            "longitude"=>55.99388900
                        ],
                        [
                            "num"=>315,
                            "province"=>21,
                            "name"=>" رودبار جنوب",
                            "latitude"=>36.82412890,
                            "longitude"=>49.42372740
                        ],
                        [
                            "num"=>316,
                            "province"=>21,
                            "name"=>" زرند",
                            "latitude"=>30.81277800,
                            "longitude"=>56.56388900
                        ],
                        [
                            "num"=>317,
                            "province"=>21,
                            "name"=>" سیرجان",
                            "latitude"=>29.45866760,
                            "longitude"=>55.67140510
                        ],
                        [
                            "num"=>318,
                            "province"=>21,
                            "name"=>" شهر بابک",
                            "latitude"=>30.11638900,
                            "longitude"=>55.11861100
                        ],
                        [
                            "num"=>319,
                            "province"=>21,
                            "name"=>" عنبراباد",
                            "latitude"=>28.47833330,
                            "longitude"=>57.84138890
                        ],
                        [
                            "num"=>320,
                            "province"=>21,
                            "name"=>" قلعه گنج",
                            "latitude"=>27.52361100,
                            "longitude"=>57.88111100
                        ],
                        [
                            "num"=>321,
                            "province"=>21,
                            "name"=>" کرمان",
                            "latitude"=>29.48500890,
                            "longitude"=>57.64390480
                        ],
                        [
                            "num"=>322,
                            "province"=>21,
                            "name"=>" کوهبنان",
                            "latitude"=>31.41027800,
                            "longitude"=>56.28250000
                        ],
                        [
                            "num"=>323,
                            "province"=>21,
                            "name"=>" کهنوج",
                            "latitude"=>27.94676030,
                            "longitude"=>57.70625720
                        ],
                        [
                            "num"=>324,
                            "province"=>21,
                            "name"=>" منوجان",
                            "latitude"=>27.44756260,
                            "longitude"=>57.50516160
                        ],
                        [
                            "num"=>325,
                            "province"=>22,
                            "name"=>" اسلام اباد غرب",
                            "latitude"=>33.72938820,
                            "longitude"=>73.09314610
                        ],
                        [
                            "num"=>326,
                            "province"=>22,
                            "name"=>" پاوه",
                            "latitude"=>35.04333300,
                            "longitude"=>46.35638900
                        ],
                        [
                            "num"=>327,
                            "province"=>22,
                            "name"=>" ثلاث باباجانی",
                            "latitude"=>34.73583710,
                            "longitude"=>46.14939690
                        ],
                        [
                            "num"=>328,
                            "province"=>22,
                            "name"=>" جوانرود",
                            "latitude"=>34.80666700,
                            "longitude"=>46.48861100
                        ],
                        [
                            "num"=>329,
                            "province"=>22,
                            "name"=>" دالاهو",
                            "latitude"=>34.28416700,
                            "longitude"=>46.24222200
                        ],
                        [
                            "num"=>330,
                            "province"=>22,
                            "name"=>" روانسر",
                            "latitude"=>34.71208920,
                            "longitude"=>46.65129980
                        ],
                        [
                            "num"=>332,
                            "province"=>22,
                            "name"=>" سنقر",
                            "latitude"=>34.78361100,
                            "longitude"=>47.60027800
                        ],
                        [
                            "num"=>333,
                            "province"=>22,
                            "name"=>" صحنه",
                            "latitude"=>34.48138900,
                            "longitude"=>47.69083300
                        ],
                        [
                            "num"=>334,
                            "province"=>22,
                            "name"=>" قصر شیرین",
                            "latitude"=>34.51590310,
                            "longitude"=>45.57768590
                        ],
                        [
                            "num"=>335,
                            "province"=>22,
                            "name"=>" کرمانشاه",
                            "latitude"=>34.45762330,
                            "longitude"=>46.67053400
                        ],
                        [
                            "num"=>336,
                            "province"=>22,
                            "name"=>" کنگاور",
                            "latitude"=>34.50416700,
                            "longitude"=>47.96527800
                        ],
                        [
                            "num"=>337,
                            "province"=>22,
                            "name"=>" گیلان غرب",
                            "latitude"=>34.14222200,
                            "longitude"=>45.92027800
                        ],
                        [
                            "num"=>338,
                            "province"=>22,
                            "name"=>" هرسین",
                            "latitude"=>34.27191490,
                            "longitude"=>47.60461830
                        ],
                        [
                            "num"=>339,
                            "province"=>23,
                            "name"=>" بویر احمد",
                            "latitude"=>30.72458600,
                            "longitude"=>50.84563230
                        ],
                        [
                            "num"=>341,
                            "province"=>23,
                            "name"=>" دنا",
                            "latitude"=>30.95166660,
                            "longitude"=>51.43750000
                        ],
                        [
                            "num"=>343,
                            "province"=>23,
                            "name"=>" گچساران",
                            "latitude"=>30.35000000,
                            "longitude"=>50.80000000
                        ],
                        [
                            "num"=>344,
                            "province"=>24,
                            "name"=>" ازادشهر",
                            "latitude"=>37.08694400,
                            "longitude"=>55.17388900
                        ],
                        [
                            "num"=>345,
                            "province"=>24,
                            "name"=>" اق قلا",
                            "latitude"=>37.01388900,
                            "longitude"=>54.45500000
                        ],
                        [
                            "num"=>346,
                            "province"=>24,
                            "name"=>" بندر گز",
                            "latitude"=>36.77496500,
                            "longitude"=>53.94617490
                        ],
                        [
                            "num"=>347,
                            "province"=>24,
                            "name"=>" بندر ترکمن",
                            "latitude"=>36.90166700,
                            "longitude"=>54.07083300
                        ],
                        [
                            "num"=>348,
                            "province"=>24,
                            "name"=>" رامیان",
                            "latitude"=>37.01611100,
                            "longitude"=>55.14111100
                        ],
                        [
                            "num"=>349,
                            "province"=>24,
                            "name"=>" علی اباد",
                            "latitude"=>36.30822600,
                            "longitude"=>74.61954740
                        ],
                        [
                            "num"=>350,
                            "province"=>24,
                            "name"=>" کرد کوی",
                            "latitude"=>36.79414260,
                            "longitude"=>54.11027400
                        ],
                        [
                            "num"=>351,
                            "province"=>24,
                            "name"=>" کلاله",
                            "latitude"=>37.38083300,
                            "longitude"=>55.49166700
                        ],
                        [
                            "num"=>352,
                            "province"=>24,
                            "name"=>" گرگان",
                            "latitude"=>36.84564270,
                            "longitude"=>54.43933630
                        ],
                        [
                            "num"=>353,
                            "province"=>24,
                            "name"=>" گنبد کاووس",
                            "latitude"=>37.25000000,
                            "longitude"=>55.16722200
                        ],
                        [
                            "num"=>354,
                            "province"=>24,
                            "name"=>" مینو دشت",
                            "latitude"=>37.22888900,
                            "longitude"=>55.37472200
                        ],
                        [
                            "num"=>355,
                            "province"=>25,
                            "name"=>" استارا",
                            "latitude"=>38.42916700,
                            "longitude"=>48.87194400
                        ],
                        [
                            "num"=>356,
                            "province"=>25,
                            "name"=>" استانه اشرفیه",
                            "latitude"=>37.25980220,
                            "longitude"=>49.94366210
                        ],
                        [
                            "num"=>357,
                            "province"=>25,
                            "name"=>" املش",
                            "latitude"=>37.09163340,
                            "longitude"=>50.18693770
                        ],
                        [
                            "num"=>358,
                            "province"=>25,
                            "name"=>" بندر انزلی",
                            "latitude"=>37.47244670,
                            "longitude"=>49.45873120
                        ],
                        [
                            "num"=>359,
                            "province"=>25,
                            "name"=>" رشت",
                            "latitude"=>37.28083300,
                            "longitude"=>49.58305600
                        ],
                        [
                            "num"=>360,
                            "province"=>25,
                            "name"=>" رضوانشهر",
                            "latitude"=>37.55067500,
                            "longitude"=>49.14098010
                        ],
                        [
                            "num"=>361,
                            "province"=>25,
                            "name"=>" رودبار",
                            "latitude"=>36.82412890,
                            "longitude"=>49.42372740
                        ],
                        [
                            "num"=>362,
                            "province"=>25,
                            "name"=>" رودسر",
                            "latitude"=>37.13784150,
                            "longitude"=>50.28361990
                        ],
                        [
                            "num"=>363,
                            "province"=>25,
                            "name"=>" سیاهکل",
                            "latitude"=>37.15277800,
                            "longitude"=>49.87083300
                        ],
                        [
                            "num"=>364,
                            "province"=>25,
                            "name"=>" شفت",
                            "latitude"=>39.63063100,
                            "longitude"=>-78.92954200
                        ],
                        [
                            "num"=>365,
                            "province"=>25,
                            "name"=>" صومعه سرا",
                            "latitude"=>37.31166700,
                            "longitude"=>49.32194400
                        ],
                        [
                            "num"=>366,
                            "province"=>25,
                            "name"=>" طوالش",
                            "latitude"=>37.00000000,
                            "longitude"=>48.42222220
                        ],
                        [
                            "num"=>367,
                            "province"=>25,
                            "name"=>" فومن",
                            "latitude"=>37.22388900,
                            "longitude"=>49.31250000
                        ],
                        [
                            "num"=>368,
                            "province"=>25,
                            "name"=>" لاهیجان",
                            "latitude"=>37.20722200,
                            "longitude"=>50.00388900
                        ],
                        [
                            "num"=>369,
                            "province"=>25,
                            "name"=>" لنگرود",
                            "latitude"=>37.19694400,
                            "longitude"=>50.15361100
                        ],
                        [
                            "num"=>370,
                            "province"=>25,
                            "name"=>" ماسال",
                            "latitude"=>37.36211850,
                            "longitude"=>49.13147690
                        ],
                        [
                            "num"=>371,
                            "province"=>26,
                            "name"=>" ازنا",
                            "latitude"=>33.45583300,
                            "longitude"=>49.45555600
                        ],
                        [
                            "num"=>372,
                            "province"=>26,
                            "name"=>" الیگودرز",
                            "latitude"=>33.40055600,
                            "longitude"=>49.69500000
                        ],
                        [
                            "num"=>373,
                            "province"=>26,
                            "name"=>" بروجرد",
                            "latitude"=>33.89419930,
                            "longitude"=>48.76703300
                        ],
                        [
                            "num"=>374,
                            "province"=>26,
                            "name"=>" پلدختر",
                            "latitude"=>33.15361100,
                            "longitude"=>47.71361100
                        ],
                        [
                            "num"=>375,
                            "province"=>26,
                            "name"=>" خرم اباد",
                            "latitude"=>33.48777800,
                            "longitude"=>48.35583300
                        ],
                        [
                            "num"=>376,
                            "province"=>26,
                            "name"=>" دورود",
                            "latitude"=>33.49550280,
                            "longitude"=>49.06317430
                        ],
                        [
                            "num"=>377,
                            "province"=>26,
                            "name"=>" دلفان",
                            "latitude"=>33.50340140,
                            "longitude"=>48.35758360
                        ],
                        [
                            "num"=>378,
                            "province"=>26,
                            "name"=>" سلسله",
                            "latitude"=>32.04577600,
                            "longitude"=>34.75163900
                        ],
                        [
                            "num"=>379,
                            "province"=>26,
                            "name"=>" کوهدشت",
                            "latitude"=>33.53500000,
                            "longitude"=>47.60611100
                        ],
                        [
                            "num"=>380,
                            "province"=>26,
                            "name"=>" الشتر",
                            "latitude"=>33.86398880,
                            "longitude"=>48.26423870
                        ],
                        [
                            "num"=>381,
                            "province"=>26,
                            "name"=>" نوراباد",
                            "latitude"=>30.11416700,
                            "longitude"=>51.52166700
                        ],
                        [
                            "num"=>382,
                            "province"=>27,
                            "name"=>" امل",
                            "latitude"=>36.46972200,
                            "longitude"=>52.35083300
                        ],
                        [
                            "num"=>383,
                            "province"=>27,
                            "name"=>" بابل",
                            "latitude"=>32.46819100,
                            "longitude"=>44.55019350
                        ],
                        [
                            "num"=>384,
                            "province"=>27,
                            "name"=>" بابلسر",
                            "latitude"=>36.70250000,
                            "longitude"=>52.65750000
                        ],
                        [
                            "num"=>385,
                            "province"=>27,
                            "name"=>" بهشهر",
                            "latitude"=>36.69222200,
                            "longitude"=>53.55250000
                        ],
                        [
                            "num"=>386,
                            "province"=>27,
                            "name"=>" تنکابن",
                            "latitude"=>36.81638900,
                            "longitude"=>50.87388900
                        ],
                        [
                            "num"=>387,
                            "province"=>27,
                            "name"=>" جویبار",
                            "latitude"=>36.64111100,
                            "longitude"=>52.91250000
                        ],
                        [
                            "num"=>388,
                            "province"=>27,
                            "name"=>" چالوس",
                            "latitude"=>36.64591740,
                            "longitude"=>51.40697900
                        ],
                        [
                            "num"=>389,
                            "province"=>27,
                            "name"=>" رامسر",
                            "latitude"=>36.90305600,
                            "longitude"=>50.65833300
                        ],
                        [
                            "num"=>390,
                            "province"=>27,
                            "name"=>" ساری",
                            "latitude"=>36.56333300,
                            "longitude"=>53.06000000
                        ],
                        [
                            "num"=>391,
                            "province"=>27,
                            "name"=>" سوادکوه",
                            "latitude"=>36.30402550,
                            "longitude"=>52.88524030
                        ],
                        [
                            "num"=>392,
                            "province"=>27,
                            "name"=>" قائم شهر",
                            "latitude"=>36.46305600,
                            "longitude"=>52.86000000
                        ],
                        [
                            "num"=>393,
                            "province"=>27,
                            "name"=>" گلوگاه",
                            "latitude"=>36.72722200,
                            "longitude"=>53.80888900
                        ],
                        [
                            "num"=>394,
                            "province"=>27,
                            "name"=>" محمود اباد",
                            "latitude"=>36.63194400,
                            "longitude"=>52.26277800
                        ],
                        [
                            "num"=>395,
                            "province"=>27,
                            "name"=>" نکا",
                            "latitude"=>36.65083300,
                            "longitude"=>53.29916700
                        ],
                        [
                            "num"=>396,
                            "province"=>27,
                            "name"=>" نور",
                            "latitude"=>50.38512460,
                            "longitude"=>3.26424360
                        ],
                        [
                            "num"=>397,
                            "province"=>27,
                            "name"=>" نوشهر",
                            "latitude"=>36.64888900,
                            "longitude"=>51.49611100
                        ],
                        [
                            "num"=>398,
                            "province"=>27,
                            "name"=>" فریدونکنار",
                            "latitude"=>36.68638900,
                            "longitude"=>52.52250000
                        ],
                        [
                            "num"=>399,
                            "province"=>28,
                            "name"=>" اشتیان",
                            "latitude"=>34.52194400,
                            "longitude"=>50.00611100
                        ],
                        [
                            "num"=>400,
                            "province"=>28,
                            "name"=>" اراک",
                            "latitude"=>34.09166700,
                            "longitude"=>49.68916700
                        ],
                        [
                            "num"=>401,
                            "province"=>28,
                            "name"=>" تفرش",
                            "latitude"=>34.69194400,
                            "longitude"=>50.01305600
                        ],
                        [
                            "num"=>402,
                            "province"=>28,
                            "name"=>" خمین",
                            "latitude"=>33.64061480,
                            "longitude"=>50.07711250
                        ],
                        [
                            "num"=>403,
                            "province"=>28,
                            "name"=>" دلیجان",
                            "latitude"=>33.99055600,
                            "longitude"=>50.68388900
                        ],
                        [
                            "num"=>404,
                            "province"=>28,
                            "name"=>" زرندیه",
                            "latitude"=>35.30699620,
                            "longitude"=>50.49117920
                        ],
                        [
                            "num"=>405,
                            "province"=>28,
                            "name"=>" ساوه",
                            "latitude"=>35.02138900,
                            "longitude"=>50.35666700
                        ],
                        [
                            "num"=>406,
                            "province"=>28,
                            "name"=>" شازند",
                            "latitude"=>33.92750000,
                            "longitude"=>49.41166700
                        ],
                        [
                            "num"=>407,
                            "province"=>28,
                            "name"=>" کمیجان",
                            "latitude"=>34.71916700,
                            "longitude"=>49.32666700
                        ],
                        [
                            "num"=>408,
                            "province"=>28,
                            "name"=>" محلات",
                            "latitude"=>33.90857480,
                            "longitude"=>50.45526160
                        ],
                        [
                            "num"=>409,
                            "province"=>29,
                            "name"=>" بندرعباس",
                            "latitude"=>27.18322160,
                            "longitude"=>56.26664550
                        ],
                        [
                            "num"=>410,
                            "province"=>29,
                            "name"=>" میناب",
                            "latitude"=>27.14666700,
                            "longitude"=>57.08000000
                        ],
                        [
                            "num"=>411,
                            "province"=>29,
                            "name"=>" بندر لنگه",
                            "latitude"=>26.55805600,
                            "longitude"=>54.88055600
                        ],
                        [
                            "num"=>412,
                            "province"=>29,
                            "name"=>" رودان-دهبارز",
                            "latitude"=>27.44083300,
                            "longitude"=>57.19250000
                        ],
                        [
                            "num"=>413,
                            "province"=>29,
                            "name"=>" جاسک",
                            "latitude"=>25.64388900,
                            "longitude"=>57.77444400
                        ],
                        [
                            "num"=>414,
                            "province"=>29,
                            "name"=>" قشم",
                            "latitude"=>26.81186730,
                            "longitude"=>55.89132070
                        ],
                        [
                            "num"=>415,
                            "province"=>29,
                            "name"=>" حاجی اباد",
                            "latitude"=>28.30916700,
                            "longitude"=>55.90166700
                        ],
                        [
                            "num"=>416,
                            "province"=>29,
                            "name"=>" ابوموسی",
                            "latitude"=>25.87971060,
                            "longitude"=>55.03280170
                        ],
                        [
                            "num"=>417,
                            "province"=>29,
                            "name"=>" بستک",
                            "latitude"=>27.19916700,
                            "longitude"=>54.36666700
                        ],
                        [
                            "num"=>418,
                            "province"=>29,
                            "name"=>" گاوبندی",
                            "latitude"=>27.20833300,
                            "longitude"=>53.03611100
                        ],
                        [
                            "num"=>419,
                            "province"=>29,
                            "name"=>" خمیر",
                            "latitude"=>26.95222200,
                            "longitude"=>55.58500000
                        ],
                        [
                            "num"=>420,
                            "province"=>30,
                            "name"=>" اسداباد",
                            "latitude"=>34.78250000,
                            "longitude"=>48.11861100
                        ],
                        [
                            "num"=>421,
                            "province"=>30,
                            "name"=>" بهار",
                            "latitude"=>34.90832520,
                            "longitude"=>48.43927290
                        ],
                        [
                            "num"=>422,
                            "province"=>30,
                            "name"=>" تویسرکان",
                            "latitude"=>34.54805600,
                            "longitude"=>48.44694400
                        ],
                        [
                            "num"=>423,
                            "province"=>30,
                            "name"=>" رزن",
                            "latitude"=>35.38666700,
                            "longitude"=>49.03388900
                        ],
                        [
                            "num"=>424,
                            "province"=>30,
                            "name"=>" کبودر اهنگ",
                            "latitude"=>35.20833300,
                            "longitude"=>48.72388900
                        ],
                        [
                            "num"=>425,
                            "province"=>30,
                            "name"=>" ملایر",
                            "latitude"=>34.29694400,
                            "longitude"=>48.82361100
                        ],
                        [
                            "num"=>426,
                            "province"=>30,
                            "name"=>" نهاوند",
                            "latitude"=>34.18861100,
                            "longitude"=>48.37694400
                        ],
                        [
                            "num"=>427,
                            "province"=>30,
                            "name"=>" همدان",
                            "latitude"=>34.76079990,
                            "longitude"=>48.39881860
                        ],
                        [
                            "num"=>428,
                            "province"=>31,
                            "name"=>" ابرکوه",
                            "latitude"=>31.13040360,
                            "longitude"=>53.25037360
                        ],
                        [
                            "num"=>429,
                            "province"=>31,
                            "name"=>" اردکان",
                            "latitude"=>32.31000000,
                            "longitude"=>54.01750000
                        ],
                        [
                            "num"=>430,
                            "province"=>31,
                            "name"=>" بافق",
                            "latitude"=>31.61277800,
                            "longitude"=>55.41055600
                        ],
                        [
                            "num"=>431,
                            "province"=>31,
                            "name"=>" تفت",
                            "latitude"=>27.97890740,
                            "longitude"=>-97.39860410
                        ],
                        [
                            "num"=>432,
                            "province"=>31,
                            "name"=>" خاتم",
                            "latitude"=>37.27091520,
                            "longitude"=>49.59691460
                        ],
                        [
                            "num"=>433,
                            "province"=>31,
                            "name"=>" صدوق",
                            "latitude"=>32.02421620,
                            "longitude"=>53.47703590
                        ],
                        [
                            "num"=>434,
                            "province"=>31,
                            "name"=>" طبس",
                            "latitude"=>33.59583300,
                            "longitude"=>56.92444400
                        ],
                        [
                            "num"=>435,
                            "province"=>31,
                            "name"=>" مهریز",
                            "latitude"=>31.59166700,
                            "longitude"=>54.43166700
                        ],
                        [
                            "num"=>436,
                            "province"=>31,
                            "name"=>" میبد",
                            "latitude"=>32.24872260,
                            "longitude"=>54.00793410
                        ],
                        [
                            "num"=>437,
                            "province"=>31,
                            "name"=>" یزد",
                            "latitude"=>32.10063870,
                            "longitude"=>54.43421380
                        ]
            ]);
        } else {
            echo "\e[31mTable is not empty, therefore NOT ";
        }
    }
}
