<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
        'name', 'lastname','phone','user_id'
    ];
    public function users(){
    	return $this->belongsTo("App\User");
    }

    public function appoiments(){
    	return $this->hasMany("App\Appointment",'patient_id');
    }
}
