<?php

namespace App;
use App\User;
use App\Patient;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    public function users(){
    	return $this->belongsTo("App\User");
    }

    public function patients(){
    	return $this->hasMany("App\Patient",'id','patient_id');
    }

    protected $fillable = [
    	'user_id','patient_id','created_at','visitnum','updated_at','pick'
    ];

    public $dates = ['updated_at'];

    public function setUpdated_atAttribute($value)
    {
        $this->attributes['updated_at'] = Carbon::createFromFormat('d/m/Y', $value);
    }


}
