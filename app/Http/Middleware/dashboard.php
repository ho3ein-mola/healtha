<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class dashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect()->route('signinpage');
        }
        return $next($request);
    }
}
