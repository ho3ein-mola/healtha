<?php

namespace App\Http\Controllers;

use App\utility\sms;
use App\utility\sms_credits;
use Illuminate\Http\Request;
use App\Patient;
use Session;
use Auth;
use App\Appointment;
use Carbon\Carbon;
use \Smsir as Smsir;

class PatientController extends Controller
{
    public function deletepatient($id)
    {
        if (isset($id)){
            $patient = Patient::find($id);
            if ($patient){
                $patient->delete();
                Session::flash('mesg', 'بیمار با موفقیت حدف شد');
                return redirect('searchpatientpage');
            }else{
                Session::flash('mesg', 'بیمار یافت نشد');
                return redirect('searchpatientpage');
            }
        }else{
            Session::flash('mesg', 'ادرس اشتباه است');
            return redirect('searchpatientpage');
        }
    }
    public function searchpatientpage(Request $request)
    {
        if (isset($request->phone)) {
            $psearch = Patient::where('user_id', Auth::user()->id)->where('phone', $request->phone)->get();
        } else if (isset($request->name) && isset($request->lastname)) {
            $psearch = Patient::where('user_id', Auth::user()->id)->where('name', 'like', '%' . $request->name . '%')->where('lastname', 'like', '%' . $request->lastname . '%')->get();
        } else {
            $psearch = null;
        }
        $patient = Patient::where('user_id', Auth::user()->id)->paginate(5);
        return view('dashboard.user-dashboard.searchpatient')->with(compact('patient'))->with(compact('psearch'));
    }
    public function addpatientpage()
    {
        if (Auth::user()->active == 0){
            Session::flash('mesg', 'ابتدا شماره ی خود را فعال نمایید');
            return redirect('dashboard');
        }else{
            return view('dashboard.user-dashboard.addpatient');
        }

    }

    public function addappoimentpage()
    {
        if (Auth::user()->active == 0){
            Session::flash('mesg', 'ابتدا شماره ی خود را فعال نمایید');
            return redirect('dashboard');
        }else{
            return view('dashboard.user-dashboard.addappoiment');
        }
    }

    public function appoimentstore(Request $request)
    {
        $validatedData = $request->validate([
            'phone' => 'required|numeric|regex:/^[0][9][0-9][0-9]{8,8}$/'
        ]);
        $phone = $request->phone;
        $date = $request->datepicker;
        $user_id = Auth::user()->id;
        $normilizedate = (int)preg_replace('/[^0-9]/', '', $date);
        $newdate = Carbon::parse(gmdate("Y-m-d", $normilizedate));
        $persiandate = verta($normilizedate)->format('j-n-Y');
        $time = verta($normilizedate)->format('H:i');
        $appoiments = Appointment::where('user_id', Auth::user()->id)
            ->whereRaw('date(created_at) = ?', $newdate)
            ->orderBy('created_at', 'asc')
            ->with('patients')->count();
        $visitnum = $appoiments + 1;
        $patient = Patient::where('phone', $phone)->first();
        if ($patient) {
            $patient_id = $patient->id;
            $user = Appointment::create([
                'user_id' => $user_id,
                'visitnum' => $visitnum,
                'patient_id' => $patient_id,
                'created_at' => $normilizedate,
                'updated_at' => $normilizedate
            ]);
            if ($user->save()) {
                Session::flash('mesg', 'نوبت شما با موفقیت ثبت شد.');
                $sendtemp = $patient->name . ' ' . 'عزیز' . ' ' . 'یک نوبت برای شما در' . ' ' . Auth::user()->name . ' ' . 'در تاریخ' . ' ' . $persiandate . ' ' . 'در ساعت ' . ' ' . $time . ' ' . 'شماره ی نوبت شما ' . ' ' . $visitnum . ' ' . 'می باشد.';
                $sms_balance = sms::sms_credit_amount(Auth::user()->id);
                if ($sms_balance > 0) {
                    $reduce = sms::sms_credit_reducer(Auth::user()->id);
                    if ($reduce) {
                        Smsir::ultraFastSend(['date' => $persiandate, 'time' => $time , 'num' => $visitnum, 'name' => Auth::user()->name], 1359, $phone);
                    } else {
                        Session::flash('mesg', 'اشکال در برداشت مبلغ پیامک لطفا با پشتیبانی تماس بگیرید');
                        return redirect('dashboard');
                    }
                } else {
                    Session::flash('mesg', 'اعتبار پیامک شما به اتمام رسیده است. لطفا ابتدا حساب پیامک خود را شارژ نمایید');
                    return redirect('dashboard');
                }
                return redirect('dashboard');
            }
        } else {
            Session::flash('mesg', 'این کاربر وجود ندارد');
            return redirect('dashboard');
        }


    }

    public function patienpreview(Request $request)
    {
        $phone = $request->phone;
        $patient = Patient::where('user_id', Auth::user()->id)->where('phone', $phone)->get();
        if (count($patient) > 0) {
            return response($patient, 200);
        } else {
            return response("کاربر یافت نشد", 404);
        }
    }

    public function addpatient(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'phone' => 'required||unique:patients|numeric|regex:/^[0][9][0-9][0-9]{8,8}$/'
        ]);
        $name = $request->name;
        $lastname = $request->lastname;
        $phone = $request->phone;
        $user = Patient::create([
            'name' => $name,
            'user_id' => Auth::user()->id,
            'lastname' => $lastname,
            'phone' => $phone
        ]);
        if ($user->save()) {
            Session::flash('mesg', 'بیمار با موفقیت به لیست شما اضافه شد.');
            return redirect('dashboard');
        }
    }
}