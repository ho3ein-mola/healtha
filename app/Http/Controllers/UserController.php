<?php

namespace App\Http\Controllers;
use App\Phd;
use App\Publ;
use App\sms_credit;
use App\utility\sms;
use App\utility\sms_credits;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Session;
use Auth;
use Hash;
use \Verta as Verta;
use App\Patient;
use App\Appointment;
use Carbon\Carbon;
use \Smsir as Smsir;
use Charts;

class UserController extends Controller
{
    public function reminder($phone,$date,$time,$name,$num)
    {
        $sms_balance = sms::sms_credit_amount(Auth::user()->id);
        if ($sms_balance > 0) {
            $reduce = sms::sms_credit_reducer(Auth::user()->id);
            if ($reduce) {
                Session::flash('mesg', 'پیام یاداوری با موفقیت ارسال شد');
                Smsir::ultraFastSend(['date' => $date, 'time' => $time , 'num' => $num, 'name' => $name], 1359, $phone);
                return redirect('dashboard');
            } else {
                Session::flash('mesg', 'اشکال در برداشت مبلغ پیامک لطفا با پشتیبانی تماس بگیرید');
                return redirect('dashboard');
            }
        } else {
            Session::flash('mesg', 'اعتبار پیامک شما به اتمام رسیده است. لطفا ابتدا حساب پیامک خود را شارژ نمایید');
            return redirect('dashboard');
        }
    }
    public function resendcode()
    {
        Session::flash('mesg', ' کد فعال سازی مجددا برای شما ارسال شد.');
        Smsir::sendVerification(Auth::user()->verification,Auth::user()->phone);
        return redirect()->back();
    }
    public function deleteappiment($id)
    {
        $app = Appointment::where('id',$id)->where('user_id',Auth::user()->id);
        if ($app){
            $app->delete();
            Session::flash('mesg', 'نوبت با موفقیت حذف شد');
            return redirect()->back();
        }else{
            Session::flash('mesg', 'حدف نوبت با مشکل مواجه شد');
            return redirect()->back();
        }
        return redirect()->back();
    }
    public function searchappoimentpage(Request $request)
    {
        if ($request->datepicker) {
            $date = $request->datepicker;
            $normilizedate = (int)preg_replace('/[^0-9]/', '', $date);
        } else {
            $normilizedate = Verta::today();
        }
        $newdate = Verta::createTimestamp($normilizedate)->DateTime()->format('Y-m-d');
        $appoiments = Appointment::where('user_id', Auth::user()->id)
            ->whereRaw('date(created_at) = ?', $newdate)
            ->orderBy('created_at', 'asc')
            ->with('patients')->get();
        return view('dashboard.user-dashboard.searchappoiment')->with(compact('appoiments'));
    }
    public function profilestore(Request $request)
    {
       $validatedData = $request->validate([
            'profile' => 'required |mimes:jpeg,bmp,png| max:500',
        ]);
       $put =  $request->file('profile')->storeAs(
           'public', 'ucv/'.Auth::user()->id.".jpeg"
       );
       $user = User::find(Auth::user()->id);
       $user->profile = $put;
       if ($user->save()){
           return response($put,200);
       }
    }
    public function signup(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required||min:6',
            'phone' => 'required|unique:users|numeric|regex:/^[0][9][0-9][0-9]{8,8}$/'
        ]);
        $name = $request->name;
        $email = $request->email;
        $password = Hash::make($request->password);
        $phone = $request->phone;
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'phone' => $phone,
            'verification' => mt_rand(100000, 999999)
        ]);
        if ($user->save()) {
            $sms_balance = sms_credit::create([
                'user_id' => $user->id,
                'total_credit' => 101,
                'credit' => 101
            ]);
            $sms_balance->save();
            Auth::loginUsingId($user->id, true);
            Smsir::sendVerification(Auth::user()->verification,$phone);
            Session::flash('mesg', 'لطفا اطلاعات زیر را وارد کنید . این اطلاعات در اختیار بیمار قرار میگیرد');
            return redirect('publicsetting');
        }
    }

    public function signin(Request $request)
    {
        $validatedData = $request->validate([
            'phone' => 'required|numeric|regex:/^[0][9][1-9][0-9]{8,8}$/',
            'password' => 'required|min:6'
        ]);
        $phone = $request->phone;
        $password = $request->password;
        if (Auth::attempt(['phone' => $phone, 'password' => $password])) {
            Session::flash('mesg', 'شما با موفقیت وارد شدید');
            return redirect()->intended('dashboard');
        } else {
            Session::flash('mesg', 'شماره موبایل یا رمز عبور اشتباه است.');
            return redirect()->back();
        }
    }

    public function dashboard(Request $request)
    {
        if ($request->datepicker) {
            $date = $request->datepicker;
            $normilizedate = (int)preg_replace('/[^0-9]/', '', $date);
        } else {
            $normilizedate = Verta::today();
        }
        $sms_balance = sms::sms_credit_amount(Auth::user()->id);
        $sms_total = sms::sms_credit_total(Auth::user()->id);
        $percentage = round(($sms_balance * 100) / $sms_total, PHP_ROUND_HALF_UP);
        $newdate = Verta::createTimestamp($normilizedate)->DateTime()->format('Y-m-d');
        $appoiments = Appointment::where('user_id', Auth::user()->id)
            ->whereRaw('date(created_at) = ?', $newdate)
            ->orderBy('created_at', 'asc')
            ->with('patients')->get();
        $today_patient_total = count($appoiments);
        $lim = Publ::where('user_id', Auth::user()->id)->pluck('limit');
        return view('dashboard.user-dashboard.udashboard')->with(compact('appoiments'))
            ->with(compact('sms_balance'))
            ->with(compact('sms_total'))
            ->with(compact('percentage'))
            ->with(compact('lim'))
            ->with(compact('today_patient_total'));
    }

    public function activeuser(Request $request)
    {
        $validatedData = $request->validate([
            'code' => 'required|min:6'
        ]);
        $user = User::find(Auth::user()->id);
        if ($request->code == Auth::user()->verification) {
            $user->update([
                'active' => 1,
                'verification' => mt_rand(100000, 999999)
            ]);
            Session::flash('mesg', 'کد فعال سازی شما با موفقیت تایید شد');
            return redirect()->back();
        } else {
            Session::flash('mesg', 'کد فعال سازی شما اشتباه است یا منقضی شده است .');
            return redirect()->back();
        }

    }

    public function changepassword(Request $request)
    {
        $validatedData = $request->validate([
            'lastpass' => 'required|min:6',
            'newpass' => 'required|min:6',
            'secendnewpass' => 'required|min:6'
        ]);
        $newpass = $request->newpass;
        $secendpass = $request->secendnewpass;
        if ($newpass == $secendpass){
            $lastpass = $request->lastpass;
            $user= User::find(Auth::user()->id);
            $userpass = $user->password;
            $check =Hash::check($lastpass, $userpass);
            if ($check){
                $user->password = Hash::make($request->secendnewpass);
                $user->save();
                Session::flash('mesg', 'رمز عبور شما با موفقیت تغییر کرد');
                return redirect()->back();
            }else{
                Session::flash('mesg', 'رمز عبور قدیمی اشتباه است');
                return redirect()->back();
            }
        }else{
            Session::flash('mesg', 'رمز عبور قدیمی اشتباه است');
            return redirect()->back();
        }

    }
    public function changenumber(Request $request)
    {
        $validatedData = $request->validate([
            'phone' => 'required|numeric|regex:/^[0][9][1-9][0-9]{8,8}$/'
        ]);
        $phone = $request->phone;
        $validuser = User::where('phone', $phone)->count();
        if ($validuser > 0) {
            Session::flash('mesg', 'این شماره قبلا استفاده شده است.');
            return redirect()->back();
        } else {
            $user = User::find(Auth::user()->id);
            $user->active = 0;
            $user->phone = $phone;
            $user->verification = mt_rand(100000, 999999);
            if ($user->save()) {
                Session::flash('mesg', 'شماره با موفقیت تغییر کرد . و کد فعال سازی مجددا برای شما ارسال شد.');
                Smsir::sendVerification($user->verification,$phone);
                return redirect()->back();
            } else {
                Session::flash('mesg', 'خطا در ذخیره ی اطلاعات . لطفا بعدا امتحان کنید');
                return redirect()->back();
            }
        }

    }
    public function signout()
    {
        Auth::logout();
        return redirect()->route('landing');
    }
    public function settingpage()
    {
        return view('dashboard.user-dashboard.setting');
    }

    public function test()
    {
        $public = Publ::where('user_id', Auth::user()->id)->first();
        $date = Publ::where('user_id', Auth::user()->id)->first()->close;
        $v = Appointment::where('user_id', Auth::user()->id)->where('pick', '>',$date )->orWhere('visitnum', ">", $public->limit)->orderBy('visitnum', 'DESC')->pluck('created_at');
        return $v;
    }


}
