<?php

namespace App\Http\Controllers;

use App\Publ;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use App\User;
use App\Patient;
use App\Appointment;
use Session;
use Auth;
use Carbon\Carbon;
use \Smsir as Smsir;

class PublicController extends Controller
{
    public function publicpage($id)
    {
        $user = User::find($id);
        $public = Publ::where('user_id', $id)->first();
        if ($public) {
            $date = Publ::where('user_id', Auth::user()->id)->first()->close;
            $v = Appointment::where('user_id', Auth::user()->id)->where('pick', '>', $date)->orWhere('visitnum', ">", $public->limit)->orderBy('visitnum', 'DESC')->pluck('created_at');
        }
        $v;
        if (isset($v)) {
            $disable = [];
            for ($i = 0; $i < count($v); $i++) {
                $disable[$i] = verta($v[$i])->format('Y-m-d');
            }
        } else {
            $disable = [];
        }

        return view('public.publicreserve')->with(compact('user'))->with(compact('public'))->with(compact('disable'));
    }

    public function publicstorelogpage()
    {
        return view('public.publicstorelogpage');
    }

    public function publicsetting()
    {
        $public = Publ::where('user_id', Auth::user()->id)->first();
        return view('dashboard.user-dashboard.publicsetting')->with(compact('public'));
    }

    public function publicsettingstore(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'telephone' => 'required|numeric',
            'provinceout' => 'required',
            'cityout' => 'required',
            'offday' => 'required',
            'until' => 'required|numeric',
            'limmit' => 'required|numeric',
            'info' => 'required',
            'lon' => 'required',
            'lat' => 'required',
            'open' => 'required',
            'close' => 'required',
        ]);
        if (isset($request->specialist)) {
            $specialist = $request->specialist;
        } else {
            $specialist = null;
        }
        if (isset($request->sub_specialist)) {
            $sub_specialist = $request->sub_specialist;
        } else {
            $sub_specialist = null;
        }
        if ($request->active == 'on') {
            $active = 1;
        } else {
            $active = 0;
        }
        $prefix = $request->prefix;
        $name = $request->name;
        $lastname = $request->lastname;
        $telephone = $request->telephone;
        $province = $request->provinceout;
        $city = $request->cityout;
        $info = $request->info;
        $offday = $request->offday;
        $until = $request->until;
        $limmit = $request->limmit;
        $open = $request->open;
        $close = $request->close;
        $lat = $request->lat;
        $lon = $request->lon;
        $public = Publ::updateOrCreate(['user_id' => Auth::user()->id], [
            'user_id' => Auth::user()->id,
            'active' => $active,
            'prefix_id' => $prefix,
            'specialist_id' => $specialist,
            'sub_specialist_id' => $sub_specialist,
            'name' => $name,
            'lastname' => $lastname,
            'telephone' => $telephone,
            'provinces_id' => $province,
            'cities_id' => $city,
            'info' => $info,
            'offday' => $offday,
            'until' => $until,
            'limit' => $limmit,
            'open' => $open,
            'close' => $close,
            'lat' => $lat,
            'lon' => $lon
        ]);
        if ($public->save()) {
            Session::flash('mesg', 'تنظیمات ثبت شد');
            return redirect()->route('dashboard');
        }

    }

    public function publicstore(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'phone' => 'required|numeric|regex:/^[0][9][0-9][0-9]{8,8}$/',
            'datepicker' => 'required'
        ]);
        $name = $request->name;
        $lastname = $request->lastname;
        $phone = $request->phone;
        $date = $request->datepicker;
        $normalizeddate = (int)preg_replace('/[^0-9]/', '', $date);
        $newdate = Verta::createTimestamp($normalizeddate)->DateTime()->format('Y-m-d');
        $appoiments = Appointment::where('user_id', $id)
            ->whereRaw('date(created_at) = ?', $newdate)
            ->orderBy('created_at', 'asc')
            ->with('patients')->count();
        $visitnum = $appoiments + 1;
        $pub = Publ::where('user_id', $id)->first();
        if (isset($pub)) {
            if ($appoiments <= $pub->limit) {
                $do = Patient::firstOrNew(['phone' => $phone], [
                    'user_id' => $id,
                    'name' => $name,
                    'lastname' => $lastname,
                    'phone' => $phone
                ]);
                if ($do->save()) {
                    $date = Publ::where('user_id', Auth::user()->id)->first();
                    $open = Carbon::parse($date->open);
                    $open_hour = Carbon::parse($date->open)->hour;
                    $open_minute = Carbon::parse($date->open)->minute;
                    $close = Carbon::parse($date->close);
                    $close_hour = Carbon::parse($date->close)->hour;
                    $close_minute = Carbon::parse($date->close)->minute;
                    $appoimen = Appointment::where('user_id', Auth::user()->id)->where('created_at', '>=', Carbon::createFromTimestamp($normalizeddate)->format('Y-m-d'))->orderBy('updated_at', 'DESC')->count();
                    if ($appoimen > 0) {
                        $save_time = Carbon::parse(Appointment::where('user_id', Auth::user()->id)->where('created_at', '>=', Carbon::createFromTimestamp($normalizeddate)->format('Y-m-d'))->orderBy('updated_at', 'DESC')->first()->updated_at);
                    } else {
                        $save_time = $open;
                    }
                    $tester = Carbon::parse($save_time->format('H:i:s'))->between($open, $close);
                    if ($tester) {
                        $user = Appointment::create([
                            'user_id' => $id,
                            'visitnum' => $visitnum,
                            'patient_id' => $do->id,
                            'created_at' => $normalizeddate,
                            'pick' => ($appoimen > 0) ? $settime = verta(Appointment::where('user_id', Auth::user()->id)->where('created_at', '>=', Carbon::createFromTimestamp($normalizeddate)->format('Y-m-d'))->orderBy('updated_at', 'DESC')->first()->updated_at)->addMinutes(10)->Datetime()->format('H:i:s') : $settime = Carbon::parse($date->open)->format('H:i:s'),
                            'updated_at' => ($appoimen > 0) ? $timeer = verta(Appointment::where('user_id', Auth::user()->id)->where('created_at', '>=', Carbon::createFromTimestamp($normalizeddate)->format('Y-m-d'))->orderBy('updated_at', 'DESC')->first()->updated_at)->addMinutes(10)->Datetime() : $timeer = Carbon::parse($date->open)->timestamp
                        ]);
                        if ($user->save()) {
                            $mes = "تاریخ ثبت شده " . ' ' . \verta($normalizeddate)->format('Y-m-d') . "ساعت مراجعه" . ' ' .  \verta($timeer)->format('h:i:s') . "شماره نوبت شما" . ' ' . $visitnum . ' ' . ' می باشد';
                            Session::flash('mesg', $mes);
                            Smsir::ultraFastSend(['date' => \verta($normalizeddate)->format('Y-m-d'), 'time' => \verta($timeer)->format('h:i:s') , 'num' => $visitnum, 'name' => $lastname], 1359, $phone);
                            return redirect()->route('publicstorelogpage');
                        }
                    } else {
                        Session::flash('mesg', "ظرفیت در ساعات مجار تکمیل شده است");
                        return redirect()->route('publicpage', ['id' => $id]);
                    }
                }
            } else {
                Session::flash('mesg', "ظرفیت در این روز تکمیل است");
                return redirect()->route('publicpage', ['id' => $id]);
            }
        }


    }
}
