<?php

namespace App\Http\Controllers;
use App\Payment;
use App\sms_credit;
use App\Wallet;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Support\Facades\Auth;
use Zarinpal\Laravel\Facade\Zarinpal;
use Session;
use Illuminate\Http\Request;
use \Smsir as Smsir;
use App\utility\sms;

class PayController extends Controller
{
    public function smsbuy(Request $request)
    {
        $validatedData = $request->validate([
            'sms' => 'required|numeric'
        ]);
        $sms_calc = (int) (18.5 *  $request->sms);
        $wallet = Wallet::where('user_id',Auth::user()->id)->first();
        if ($wallet){
          $wall = $wallet->amount;
        }else{
            $wall = 0;
        }
        if ($sms_calc <= $wall){
                $wallet->update([
                    'amount' => $wallet->amount - $sms_calc
                ]);
                $payment = Payment::create([
                    "user_id" => Auth::user()->id,
                    "amount" => $sms_calc,
                    "status" => false,
                    'created_at' => Verta::now()->timestamp,
                    'updated_at' => Verta::now()->timestamp
                ]);
                if ($payment->save()){
                    $id = Auth::user()->id;
                    $sms_amount = sms::sms_credit_amount($id);
                    $new_balance =  $sms_amount + $request->sms ;
                    sms_credit::where('user_id',$id)->update([
                        "total_credit" => $new_balance,
                        "credit" => $new_balance
                    ]);
                    Session::flash('mesg', 'خرید با موفقیت انجام شد');
                    return redirect('walletpage');
                }else{
                    Session::flash('mesg', 'تراکنش با موفقیت انجام شد اما اطلاعات پرداخت شما به روز نشد با پشتیبانی تماس بگیرید');
                    return redirect('walletpage');
                }
        }else{
            Session::flash('mesg', 'موجودی کافی نیست');
            return redirect('walletpage');
        }
    }
    public function paymentpage()
    {
        $pay = Payment::where('user_id',Auth::user()->id)->paginate(20);
        return view('dashboard.user-dashboard.payment')->with(compact('pay'));
    }
    public function walletpage()
    {
        $sms_balance = sms::sms_credit_amount(Auth::user()->id);
        $wallet = Wallet::where('user_id',Auth::user()->id)->first();
        return view('dashboard.user-dashboard.wallet')->with(compact('sms_balance'))->with(compact('wallet'));
    }
    public function getway(Request $request)
    {
        $validatedData = $request->validate([
            'amount' => 'required|numeric'
        ]);
        $amount = $request->amount;
        $results = Zarinpal::request(
        route('getwaycallback',['amount' => $amount]),          //required
            "$amount",                                  //required
            'افزایش اعتبار کیف پول',                             //required
            Auth::user()->email,                      //optional
            Auth::user()->phone,                         //optional
            json_encode([                          //optional
                "Wages" => [
                    "zp.1.1" => [
                        "Amount" => 120,
                        "Description" => "part 1"
                    ],
                    "zp.2.5" => [
                        "Amount" => 60,
                        "Description" => "part 2"
                    ]
                ]
            ])
        );
        Zarinpal::redirect();
    }

    public function getwaycallback(Request $request,$amount)
    {
        $status =  $request->Status;
        if ($status == "OK"){
            $get =  Zarinpal::verify('OK',$amount,$request->Authority);
            if ($get){
                if ($get['Status'] == "success" || $get['Status'] == "verified_before"){
                    $payment = Payment::create([
                       "user_id" => Auth::user()->id,
                        "amount" => $amount,
                        "status" => true,
                        'created_at' => Verta::now()->timestamp,
                        'updated_at' => Verta::now()->timestamp
                    ]);
                    $wallet_credit = Wallet::where('user_id',Auth::user()->id)->first();
                    if ($wallet_credit){
                        $wallet_amount = $wallet_credit->amount;
                    }else{
                        $wallet_amount  = 0;
                    }
                    $wallet = wallet::updateOrCreate(['user_id'=> Auth::user()->id],[
                        "user_id" => Auth::user()->id,
                        "amount" => $wallet_amount + $amount
                    ]);
                    if ( $payment->save() && $wallet->save() ){
                        Session::flash('mesg', 'تراکنش با موفقیت انجام شد');
                        return redirect('walletpage');
                    }else{
                        Session::flash('mesg', 'تراکنش با موفقیت انجام شد اما اطلاعات پرداخت شما به روز نشد با پشتیبانی تماس بگیرید');
                        return redirect('walletpage');
                    }
                }else{
                    Session::flash('mesg', 'پرداخت نا موفق');
                    return redirect('walletpage');
                }
            }else{
                Session::flash('mesg', 'پرداخت نا موفق');
                return redirect('walletpage');
            }
        }else{
            Session::flash('mesg', 'پرداخت نا موفق');
            return redirect('walletpage');
        }
    }
}
