<?php


namespace App\utility;


use App\sms_credit;

class sms
{
    public static function sms_credit($user_id){
        $sms = new sms_credit;
        $find = $sms->where('user_id',$user_id)->first();
        return $find;
    }

    public static function sms_credit_amount($user_id)
    {
        return self::sms_credit($user_id)->credit;
    }
    public static function sms_credit_total($user_id)
    {
        return self::sms_credit($user_id)->total_credit;
    }
    public static function sms_credit_reducer($user_id){
        $reduser = self::sms_credit($user_id);
        $amount = self::sms_credit_amount($user_id);
        $update_credit = $reduser->update([
            'credit' => $amount - 1
        ]);
        if ($update_credit){
            return true;
        }
    }
}