<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sms_credit extends Model
{
    protected $fillable = [
        'user_id','credit','total_credit'
    ];
}
