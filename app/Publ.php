<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publ extends Model
{
    protected $casts = [
        'offday' => 'array',
    ];
    protected  $fillable = [
        'user_id','active','name','lastname','telephone','provinces_id','prefix_id','specialist_id','sub_specialist_id','cities_id','info','offday','until','limit','open','close','lat','lon'
    ];
    protected $table = 'publics';
}
