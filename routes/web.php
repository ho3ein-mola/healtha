<?php

use App\Http\Middleware\dashboard;
use App\Http\Middleware\homepage;


//general route

Route::get('/', function () {
    return view('landing');
})->name('landing')->middleware(homepage::class);;

Route::get('/signupup', function () {
    return view('welcome');
})->name('homepage')->middleware(homepage::class);

Route::get('/signinpage', function () {
    return view('auth.signinpage');
})->name('signinpage');

Route::get('/search', function () {
    return view('search');
})->name('search');

//User's Controller
Route::get('/dashboard', 'UserController@dashboard')->name('dashboard')->middleware(dashboard::class);
Route::post('/activeuser','UserController@activeuser')->name('activeuser')->middleware(dashboard::class);
Route::post('/resendcode','UserController@resendcode')->name('resendcode')->middleware(dashboard::class);;
Route::get('/reminder/{phone}/{date}/{time}/{name}/{num}','UserController@reminder')->name('reminder')->middleware(dashboard::class);;
Route::post('/sighup','UserController@signup')->name('sighup');
Route::post('/signin','UserController@signin')->name('signin');
Route::get('/signout','UserController@signout')->name('signout');
Route::post('/changenumber','UserController@changenumber')->name('changenumber')->middleware(dashboard::class);
Route::post('/changepassword','UserController@changepassword')->name('changepassword')->middleware(dashboard::class);
Route::get('/settingpage','UserController@settingpage')->name('settingpage')->middleware(dashboard::class);
Route::post('/profilestore','UserController@profilestore')->name('profilestore')->middleware(dashboard::class);
Route::get('/searchappoimentpage','UserController@searchappoimentpage')->name('searchappoimentpage')->middleware(dashboard::class);
Route::get('/searchpatientpage','PatientController@searchpatientpage')->name('searchpatientpage')->middleware(dashboard::class);
Route::get('/deletepatient/{id}','PatientController@deletepatient')->name('deletepatient')->middleware(dashboard::class);
Route::post('/profilestore','UserController@profilestore')->name('profilestore')->middleware(dashboard::class);
Route::get('/deleteappiment/{id}','UserController@deleteappiment')->name('deleteappiment')->middleware(dashboard::class);


//Patient's Controller
Route::get('/addpatientpage','PatientController@addpatientpage')->name('addpatientpage')->middleware(dashboard::class);;
Route::post('/addpatient','PatientController@addpatient')->name('addpatient')->middleware(dashboard::class);;
Route::get('/addappoimentpage','PatientController@addappoimentpage')->name('addappoimentpage')->middleware(dashboard::class);;
Route::get('/patienpreview','PatientController@patienpreview')->name('patienpreview')->middleware(dashboard::class);;
Route::post('/appoimentstore','PatientController@appoimentstore')->name('appoimentstore')->middleware(dashboard::class);;

//Test Route
Route::get('/test','UserController@test')->name('test');

//Public Reservation
Route::get('publicsetting','PublicController@publicsetting')->name('publicsetting')->middleware(dashboard::class);;
Route::post('publicsettingstore','PublicController@publicsettingstore')->name('publicsettingstore')->middleware(dashboard::class);;
Route::get('ucv/{id}','PublicController@publicpage')->name('publicpage');
Route::post('ucv/publicstore/{id}','PublicController@publicstore')->name('publicstore');
Route::get('log/publicstorelogpage','PublicController@publicstorelogpage')->name('publicstorelogpage');

//payment
Route::get('/walletpage','PayController@walletpage')->name('walletpage')->middleware(dashboard::class);
Route::get('/paymentpage','PayController@paymentpage')->name('paymentpage')->middleware(dashboard::class);
Route::get('/getway','PayController@getway')->name('getway')->middleware(dashboard::class);;
Route::get('getwaycallback/{amount}','PayController@getwaycallback')->name('getwaycallback')->middleware(dashboard::class);;
Route::get('/smsbuy','PayController@smsbuy')->name('smsbuy')->middleware(dashboard::class);
