<!DOCTYPE html>
<html lang="fa">
<head>
@yield('head')
<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--material-css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/')}}css/materialize.css">
    <link rel="stylesheet" type="text/css"
          href="{{ URL::asset('/')}}js/persian-datepicker/dist/css/persian-datepicker.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#1a237e">
    <!--app-css-->
    <link rel="stylesheet" href="{{ URL::asset('/')}}css/app.css">
    <title>فینداک | سامانه رزرواسیون پزشکی</title>
    <meta name="google-site-verification" content="CWKzgVJQ3bX8QWIbuPggrmFHYpuHjeYcE5xk2CsohaM" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="فینداک محلی برای پیدا کردم ادرس مطب پزشک عمومی تلفن مطب پزشک عممومی و ادرس مطب پزشک متخصص و تلفن مطب پزشک متخصص وادرس مطب دندانپزشک و تلفن مطب دنداپزشک و ادرس مطب روانپزشک و تلفن مطب روانپزشک و درخواست نوبت تلفنی پزشک و درخواست نوبت اینترنتی پزشک">
    @yield('head')
</head>
<body>
@yield('body')
<script type="text/javascript" src="{{ URL::asset('/')}}js/jquery.js"></script>
<script type="text/javascript" src="{{ URL::asset('/')}}js/app.js"></script>
<script src="{{ URL::asset('/')}}js/persian-date/dist/persian-date.min.js" type="text/javascript"></script>
<script src="{{ URL::asset('/')}}js/persian-datepicker/dist/js/persian-datepicker.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav({
            menuWidth: 300,
            closeOnClick: true,
            edge: 'right', // <--- CHECK THIS OUT
        });
        $("#message").slideDown(500, function () {
            setTimeout(function () {
                $("#message").slideUp(500);
            }, 9000);
        });
    });
</script>
@yield('script')
</body>
</html>