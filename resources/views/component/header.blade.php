<nav>
    <div class="nav-wrapper indigo darken-3">
        <ul class="center">
            <li class="hide-on-med-and-down">
                    <a href="{{ route('landing') }}" class="brand-logo center white-text">
                        <span>فینداک</span>
                    </a>
            </li>
            <li>
                <a href="{{ route('landing') }}" class="brand-logo center">
                    <img src="{{ URL::asset('/')}}image/fdoc-logo.png" alt="فینداک ای ار" class="brand-logo nav-log">
                </a>
            </li>
        </ul>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i
                    class="material-icons white-text">menu</i></a>
        <ul class="right hide-on-med-and-down">
            @if(Auth::user())
                <li><a class="white-text indigo darken-2 " href=" {{ route('paymentpage') }}">سابقه پرداخت</a></li>
                <li><a class="white-text indigo darken-2 " href=" {{ route('walletpage') }}">کیف پول</a></li>
                <li><a class="white-text indigo darken-1 " href=" {{ route('dashboard') }}">داشبورد</a></li>
                <li><a class="white-text red accent-3" href="{{ route('signout') }}">خروج از سیستم</a></li>
            @else
                <li><a class="white-text" href="{{ route('homepage') }}">ثبت نام</a></li>
                <li><a class="white-text" href="{{ route('signinpage') }}">ورود به سیستم</a></li>
            @endif
        </ul>

        <ul class="left hide-on-med-and-down">
            <li><a class="white-text" href="">وبلاگ</a></li>
            <li><a class="white-text" href="#">پشتیبانی</a></li>
        </ul>

        <ul class="side-nav " id="mobile-demo">
            @if(Auth::user())
                <li><a class="white-text indigo darken-2 right-align " href=" {{ route('dashboard') }}">داشبورد</a></li>
                <li><a class="white-text indigo darken-2 right-align " href=" {{ route('walletpage') }}">کیف پول</a></li>
                <li><a class="white-text indigo darken-2 right-align" href=" {{ route('paymentpage') }}">سابقه ی پرداخت</a></li>
                <li><a class="white-text indigo  right-align" href=" {{ route('paymentpage') }}">لینک عمومی</a></li>
                <li>
                    <a href="{{ route('addpatientpage') }}" class="collection-item right-align black-text">اضافه کردن
                        بیمار</a>
                </li>
                <li>
                    <a href="{{ route('searchpatientpage') }}" class="collection-item right-align black-text ">جست و جوی بیمار</a>
                </li>
                <li>
                    <a href="{{ route('addappoimentpage') }}" class="collection-item right-align black-text">ثبت
                        نوبت</a>
                </li>
                <li>
                    <a href="{{ route('searchappoimentpage') }}" class="collection-item right-align black-text ">جست و جوی نوبت</a>
                </li>
                <li>
                    <a href="{{ route('publicsetting') }}" class="collection-item right-align black-text">تنظیمات
                        لینک عمومی</a>
                </li>
                <li>
                    <a href="{{ route('settingpage') }}" class="collection-item right-align	black-text">تنظیمات</a>
                </li>
                <li><a class="white-text red accent-3 right-align" href="{{ route('signout') }}">خروج از سیستم</a></li>
            @else
                <li><a class="black-text" href="{{ route('homepage') }}">ثبت نام</a></li>
                <li><a class="black-text" href="{{ route('signinpage') }}">ورود به سیستم</a></li>
            @endif
        </ul>
    </div>
</nav>
