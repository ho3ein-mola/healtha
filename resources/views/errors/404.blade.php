@extends('master.html')
@section('body')
    <div class="row">
        <div class="container">
            <br>
            <div class="card-panel green accent-3 black-text center">
                <h1>404 خطای</h1>
            </div>
            <div class="card-panel red accent-3 white-text center">
                <h3>این صفحه یافت نشد </h3>
            </div>
        </div>
    </div>
@endsection