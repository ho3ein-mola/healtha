@extends('master.html')
@section('body')
    <br>
    @if (Session::has('mesg'))
        <div class="container">
            <div class="row">
                <div class="col l12 s12 m5">
                    <div class="card-panel blue">
      <span class="white-text">
        {{ Session::get('mesg')}}
    </span>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col l6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <div class="card red">
                            <div class="card-content white-text rtl">
                                <span class="card-title">خطا</span>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                <div class="card indigo accent-3">
                    <div class="card-content white-text rtl">
                        <span class="card-title">نکته برای ثبت نام</span>

                        <p>
                            لطفا نام خود را به صورت فارسی وارد نمایید
                        </p>
                        <p>
                            لطفا شماره موبایل را با پیش شما ره 0 وارد کنید برای مثال 09160000000
                        </p>
                        <p>
                            لطفا شماره موبایل را با پیش شما ره 0 وارد کنید برای مثال 0936000000
                        </p>
                        <p>
                            لطفا نام خود را بدون پیشوند دکتر و یا متخصص یا دندانپزشک وارد نمایید این پیشوند ها بعدا قابل
                            تنظیم هستند
                        </p>
                        <p>
                            رمز عبور شما باید از 6 رقم بیشتر باشد .
                        </p>
                        <p>
                            لطفا از رمز های اسان مانند 123456 استفاده نکنید
                        </p>
                        <p>
                            لطفا از ایمیل معتبر مانند گوگل و یاهو استفاده نمایید
                        </p>
                    </div>
                    <div class="card-action rtl">
                        <a class="white-text" href="#">اطلاعات بیشتر</a>
                    </div>
                </div>
                <div class="card red accent-3">
                    <div class="card-content white-text rtl">
                        <span class="card-title">مهم</span>
                        <p>
                            هیچ کدام از این اطلاعات در اختیار بیمار قرار نمیگیرد . و اطلاعات اشراکی شما با بیمار باید به
                            صورت جدا وارد شود.
                        </p>
                        <p>
                            برای مثال شماره ی موبایل شما برای بیمار نمایش داده نمی شود و باید شماره محل کار و یا مطب را
                            به صورت جداگانه بعد از ثبت نام وارد کنید
                        </p>
                    </div>
                    <div class="card-action right-align">
                        <a class="white-text" href="#">اطلاعات بیشتردرباره ی حریم خصوصی</a>
                    </div>
                </div>
            </div>

            <div class="col l6">
                <form class="col l12 white card" method="POST" action="{{ route('sighup') }}">
                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix">face</i>
                            <input name="name" id="icon_prefix" type="text" class="validate rtl"
                                   value="{{ old('name') }}">
                            <label for="icon_prefix"> نام خانوادگی به فارسی برای ارسال در پیامک</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix">markunread</i>
                            <input name="email" id="icon_prefix" type="text" class="validate"
                                   value="{{ old('email') }}">
                            <label for="icon_prefix">ایمیل</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix">enhanced_encryption</i>
                            <input name="password" id="icon_prefix" type="password" class="validate"
                                   value="{{ old('password') }}">
                            <label for="icon_prefix">رمز عبور</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m12l12">
                            <i class="material-icons prefix">phone</i>
                            <input name="phone" id="icon_prefix" type="text" class="validate center-align phonesize"
                                   value="{{ old('phone') }}">
                            <label for="icon_prefix">شماره موبایل</label>
                        </div>
                    </div>
                    <div class="row center">
                        <button class="btn-flat white-text green" type="submit">ثبت نام</button>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection