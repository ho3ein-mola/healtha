@extends('master.html')
@section('head')
    <meta name="robots" content="noindex, nofollow">
    <style>
        @media print{
            body{ background-color:#FFFFFF; background-image:none; color:#000000 }
            .hide-on-med-and-down{ display:none;}
            #printformpicker{ display:none;}
            #print{ display:none;}
            #act{display:none;}
        }
    </style>
@endsection
@section('body')
    <div class="row">
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb grey-text ">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb  grey-text">ثبت بیمار</a>
                    <a href="{{ route('searchpatientpage') }}" class="breadcrumb  white-text bold">جست و جوی بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text ">ثبت نوبت</a>
                    <a href="{{ route('searchappoimentpage') }}" class="breadcrumb grey-text ">جست و جوی نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb grey-text ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="container">
            <div class="col s12 m12 l12">
                @if (Session::has('mesg'))
                    <div class="container" id="message">
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card-panel green accent-4">
                                    <h5 class="white-text center">
                                        {{ Session::get('mesg')}}
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col s12 m12 l4">
                        <h5 class="center-align grey-text">جست و جو بر اساس شماره</h5>
                        <form class="col s12 m12 l12 white card" method="get" action="{{ route('searchpatientpage') }}">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">phone</i>
                                <input name="phone" id="icon_prefix" type="text" class="validate">
                                <label for="icon_prefix">شماره موبایل</label>
                            </div>
                            <div class="input-field col s12 m12 l12 center">
                                <input id="icon_prefix" type="submit" value="جست و جو" class="btn-flat white-text indigo">
                                <div class="row"></div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                        <h5 class="center-align grey-text">جست و جو بر اساس نام و نام خانوادگی</h5>
                        <form class="col s12 m12 l12 white card" method="get" action="{{ route('searchpatientpage') }}">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">person</i>
                                <input name="name" id="icon_prefix" type="text" class="validate rtl">
                                <label for="icon_prefix">نام</label>
                            </div>
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">people</i>
                                <input name="lastname" id="icon_prefix" type="text" class="validate rtl">
                                <label for="icon_prefix">نام خانوادگی</label>
                            </div>
                            <div class="input-field col s12 m12 l12 center">
                                <input id="icon_prefix" type="submit" value="جست و جو" class="btn-flat white-text green">
                                <div class="row"></div>
                            </div>
                        </form>
                    </div>
                    <div class="col m12 s12 l8">
                        @if(count($patient) > 0)
                            <h5 class="center-align grey-text rtl">۵ بیمار اخیر شما</h5>
                            <div class="col s12 m12 l12 card ">
                                <table class="centered responsive-table highlight  rtl">
                                    <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>موبایل</th>
                                        <th id="act">عملیات</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 0 ?>
                                    @foreach($patient as $pat )
                                        <tbody>
                                        <tr>
                                            <td>{{$pat->id}}</td>
                                            <td> {{ $pat->name }} </td>
                                            <td> {{ $pat->lastname }} </td>
                                            <td> {{ $pat->phone }} </td>
                                            <td id="act">
                                                <a  href="{{route("deleteappiment",['id'=> $pat->id])   }}" class="btn-flat white-text red accent-3">حذف</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <div class="col s12 m12 l12 center">
                                <h4 class="grey-text">هیچ بیماری وجود ندارد</h4>
                            </div>
                        @endif
                    </div>
                    <div class="col m12 s12 l8">
                        @if(isset($psearch))
                            <h5 class="center-align grey-text rtl">نتیجه ی جست و جو</h5>
                            <div class="col s12 m12 l12 card ">
                                <table class="centered responsive-table highlight  rtl">
                                    <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>موبایل</th>
                                        <th id="act">عملیات</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 0 ?>
                                    @foreach($psearch as $pse )
                                        <tbody>
                                        <tr>
                                            <td>{{$pse->id}}</td>
                                            <td> {{ $pse->name }} </td>
                                            <td> {{ $pse->lastname }} </td>
                                            <td> {{ $pse->phone }} </td>
                                            <td id="act">
                                                <a  href="{{route("deletepatient",['id'=> $pse->id])   }}" class="btn-flat white-text red accent-3">حذف</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <div class="col s12 m12 l12 center">
                                <h5 class="grey-text">بیماری جست و جو نشده است</h5>
                            </div>
                        @endif
                    </div>

                </div>

                <div class="col s12 m12 l5">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <div class="card red">
                                <div class="card-content white-text rtl">
                                    <span class="card-title">خطا</span>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>



    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4 class="red-text center">بیمار با این شماره یافت نشد</h4>
            <p class="right-align bold">لطفا ابتدا بیمار را ثبت کنید</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <h4 class="green-text center">بیمار با این شماره یافت شد</h4>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
        </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#print').click(function () {
                window.print();
            });
            $('.modal').modal();
            $('.inline-example').persianDatepicker({
                inline: true,
                altField: '#inlineExampleAlt',
                altFormat: 'LLLL:X',
                autoClose: true,
                viewMode: 'day',
                toolbox: {
                    calendarSwitch: {
                        enabled: true
                    }
                },
                navigator: {
                    scroll: {
                        enabled: true
                    }
                },
                maxDate: new persianDate().add('month', 3).valueOf(),
                minDate: new persianDate().subtract('month', 0).valueOf(),
                timePicker: {
                    enabled: true,
                    meridiem: {
                        enabled: true
                    }
                }
            });
        });
    </script>
@endsection