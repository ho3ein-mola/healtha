@extends('master.html')
@section('head')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/3.20.1/math.js"></script>
    <style>
        .placepicker-map {
            min-height: 250px;
        }
    </style>
@endsection
@section('body')
    <div class="row">
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb grey-text  ">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb  grey-text ">ثبت بیمار</a>
                    <a href="{{ route('searchpatientpage') }}" class="breadcrumb  grey-text ">جست و جوی بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text  ">ثبت نوبت</a>
                    <a href="{{ route('searchappoimentpage') }}" class="breadcrumb grey-text  ">جست و جوی نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb white-text bold ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="container">
            <div class="col l12">
                @if (Session::has('mesg'))
                    <div class="container" id="message">
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card-panel green accent-4">
						<span class="white-text center">
							<h5>{{ Session::get('mesg')}}</h5>
						</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col l5">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <div class="card red">
                                <div class="card-content white-text rtl">
                                    <span class="card-title">خطا</span>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card  red accent-3">
                        <div class="card-content white-text">
                            <span class="card-title right">انتخابگر مکان</span>
                            <input name="mapstring" type="text" id="us2-address" class="rtl "/>
                            <div id="us2" style="width: 100%; height: 50vh;"></div>
                            <br>
                            <span for="" class="right">
                            عرض جغرافیایی
                        </span>
                            <input name="lat" type="text" id="us2-lat"/>
                            <span for="" class="right">
                            طول جغرافیایی
                        </span>
                            <input name="lon" type="text" id="us2-lon"/>
                        </div>
                    </div>
                </div>
                <div class="col l7">
                    <form class="col l12 white card" method="POST" action="{{route('publicsettingstore')}}">

                        <div class="row">
                            <div class="row"></div>
                            <div class="center">
                                <div class="switch">
                                    <label>
                                        روشن
                                        <input name="active" type="checkbox" checked>
                                        <span class="lever"></span>
                                        خاموش
                                    </label>
                                </div>
                                <label>با غیر فعال کردن لینک عمومی امکان رزرو توسط بیماران غیره فعال می شود</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div class="input-field col s12 m12 s12">
                                    <i class="material-icons prefix">room</i>
                                    <input id="province" name="province" type="text" id="autocomplete-input" class="autocomplete rtl"
                                           value="{{ old('province') }} ">
                                    <label for="autocomplete-input">استان</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12">
                                <div class="input-field col s12 m12 s12">
                                    <i class="material-icons prefix">room</i>
                                    <input disabled name="city" type="text" id="autocomplete-input2"
                                           class="autocomplete2 rtl"
                                           value="{{ old('city') }}">
                                    <label for="autocomplete-input">شهر</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">school</i>
                                <select name="prefix" id="prefix">
                                    <option value="00" selected disabled>عمومی - تخصص - فوق تخصص را مشخص کنید</option>
                                </select>
                                <label for="">پزشک - دنداپزشک - روانپزشک</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">school</i>
                                <select name="specialist" id="specialist" disabled>
                                    <option value="00" selected disabled>تخصص خود را انتخاب کنید</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">school</i>
                                <select name="sub_specialist" id="sub_specialist" disabled>
                                    <option value="00" selected disabled>فوق تخصص خود را انتخاب کنید</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">account_circle</i>
                                <input name="name" id="icon_prefix" type="text" class="validate rtl" value="@if ($public) {{ $public->name }} @else {{ old('name') }} @endif">
                                <label for="icon_prefix">نام</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">assignment_ind</i>
                                <input name="lastname" id="icon_prefix" type="text" class="validate rtl"
                                       value="@if ($public) {{ $public->lastname }} @else {{ old('lastname') }} @endif">
                                <label for="icon_prefix">نام خانوادگی</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">phone_in_talk</i>
                                <input name="telephone" id="icon_prefix" type="text" class="validate"
                                       value="@if ($public) {{ $public->telephone }} @else {{ old('telephone') }} @endif">
                                <label for="icon_prefix">تلفن محل کار</label>
                            </div>
                        </div>



                        <div class="row">
                            <input  name="provinceout" type="text"
                                    class=" rtl hide"
                                    value="{{ old('provinceout') }}">
                            <input  name="cityout" type="text"
                                    class=" rtl hide"
                                    value="{{ old('cityout') }}">
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 s12">
                                <i class="material-icons prefix">assignment</i>
                                <textarea name="info" id="textarea1"
                                          class="materialize-textarea rtl">@if ($public) {{ $public->info }} @else {{ old('info') }} @endif</textarea>
                                <label for="textarea1">توضیحات تابلوی اعلانات</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 s12">
                                <i class="material-icons prefix">event_busy</i>
                                <select multiple id="multiple">
                                    <option @if(old('offday') ==  0) {{ 'selected ' }} @endif value="" selected disabled>روزهای تعطیلی خود را انتخاب کنید</option>
                                    <option @if(old('offday') ==  1) {{ 'selected ' }} @endif value="1">شنبه</option>
                                    <option @if(old('offday') ==  2) {{ 'selected ' }} @endif value="2">یکشنبه</option>
                                    <option @if(old('offday') ==  3) {{ 'selected ' }} @endif value="3">دوشنبه</option>
                                    <option @if(old('offday') ==  4) {{ 'selected ' }} @endif value="4">سه شنبه</option>
                                    <option @if(old('offday') ==  5) {{ 'selected ' }} @endif value="5">چهار شنبه</option>
                                    <option @if(old('offday') ==  6) {{ 'selected ' }} @endif value="6">پنج شنبه</option>
                                    <option @if(old('offday') ==  7) {{ 'selected ' }} @endif value="7">جمعه</option>
                                </select>
                                <label>رزرو در این روز ها برای بیماران بسته میشود </label>
                            </div>
                        </div>
                        <div class="row">
                            <input type="text" name="offday" id="offday" class="hide">
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <div class="col l12 center">
                                    <span class="center-align">تا چند ماه اینده امکان رزرو وجود داشته باشد ؟</span>
                                </div>
                                <p class="range-field">
                                    <input name="until" type="range" min="1" max="4"  value="@if ($public) {{ $public->until }} @else {{ old('until') }} @endif">
                                </p>
                            </div>
                        </div>
                        <div class="row center">
                            <spane>محدودیت تعداد بیمارن در هر روز را مشخص کنید</spane>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">traffic</i>
                                <input name="limmit" id="icon_prefix" type="text" class="validate"
                                       value="@if ($public) {{ $public->limit }} @else {{ old('limmit') }} @endif">
                                <label for="icon_prefix">عدد به انگلیسی -مثلا 250</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row center">
                                <span class="" for="">ساعات مراجعه</span>
                            </div>
                            <div class="input-field col s12 m12 l6">
                                <i class="material-icons prefix">date_range</i>
                                <input name="open" id="inlineExampleAlt"
                                       class="timepicker" value="{{ old('open') }}"/>
                                <div class=" datepicker-demo datepicker">از ساعت</div>
                            </div>

                            <div class="input-field col s12 m12 l6">
                                <i class="material-icons prefix">date_range</i>
                                <input name="close" id="inlineExampleAlt2"
                                       class="timepicker" value="{{ old('close') }}"/>
                                <div class="datepicker">تا ساعت</div>
                            </div>
                        </div>
                        <div class="row">
                            <input name="lat" id="lat" class="hide">
                            <input name="lon" id="lon" class="hide">
                        </div>
                        <div class="row center">
                            <button class="btn flat green darken-3" type="submit">
                                ثبت تنظیمات
                            </button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript"
            src='http://maps.google.com/maps/api/js?language=fa&key=AIzaSyAkR9vhY1uZ0EZfJpB3YqFMyDsLvO2xO5o&sensor=false&libraries=places'></script>
    <script src="{{ URL::asset('/')}}js/locationpicker.jquery.js"></script>
    <script src="{{ URL::asset('/')}}js/publicsetting.js"></script>
@endsection