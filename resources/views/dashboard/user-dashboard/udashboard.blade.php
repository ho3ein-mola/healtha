@extends('master.html')
@section('head')
    {!! Charts::styles() !!}
    <meta name="robots" content="noindex, nofollow">
@endsection
@section('body')
    <div class="row">
    <div class="row">
        <div class="col s12 m12 hide-on-med-and-up">
            <div class="col s12 m12 l12 card waves-effect waves-light">
                <img class="responsive-img " src="@if(Auth::user()->profile ==1) {{  URL::asset('/')}}image/doctor.jpg   @else {{ asset('storage/ucv/') }}/{{ Auth::user()->id }}.jpeg?x={{Verta::now()->timestamp}}" @endif ">
            </div>
        </div>
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb white-text bold ">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb  grey-text">ثبت بیمار</a>
                    <a href="{{ route('searchpatientpage') }}" class="breadcrumb  grey-text ">جست و جوی بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text ">ثبت نوبت</a>
                    <a href="{{ route('searchappoimentpage') }}" class="breadcrumb grey-text ">جست و جوی نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb grey-text ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="col s12 m12 l8">
            @if (Session::has('mesg'))
                <div class="container" id="message">
                    <div class="row">
                        <div class="col l12 s12 m12">
                            <div class="card-panel green accent-4">
						<span class="white-text">
							<h5 class="center-align">{{ Session::get('mesg')}}</h5>
						</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="card">
                @if(Auth::user()->active == 0)
                    <div class="row">
                        <div class="col s12 m6 right-align">
                            <div class="card red accent-3">
                                <div class="card-content white-text">
                                    <span class="card-title ">تغییر شماره</span>
                                    <p>
                                        اگر به هر دلیل د هنگام ثبت نام , شماره خود را به اشتباه وارد کریده اید می توانید
                                        از طریق فیلد پایین اقدام به تصحیح شماره نمایید.
                                    </p>
                                </div>
                                <div class="card-action">
                                    <a class="white-text" href="#">اطلاعات بیشتر</a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 right-align">
                            <div class="card indigo accent-3">
                                <div class="card-content white-text">
                                    <span class="card-title">کد یکبار مصرف</span>
                                    <p>
                                        اگر شماره خود را درست وارد کرده باشید یک کد 6 رقمی برای شماره ی موبایل وارد شده
                                        ارسال می شود لطفا کد یکبار مصرف را در فیلد پایین وارد نمایید تا اشتراک شما فعال
                                        شود
                                    </p>
                                </div>
                                <div class="card-action">
                                    <a class="white-text" href="#">کد فعالسازی بعد 300 ثانیه برایم ارسال نشد, چه کار کنم
                                        ؟</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col l6">
                        <form class="col l12 white card" method="POST" action="{{ route('changenumber') }}">
                            <div class="row">
                                <div class="input-field col s12 m12 l12 center">
                                    <h4 class="center">{{ Auth::user()->phone}}</h4>
                                </div>
                                <div class="input-field col l12">
                                    <i class="material-icons prefix">phone</i>
                                    <input name="phone" id="icon_prefix" type="text" class="validate">
                                    <label for="icon_prefix">phone</label>
                                </div>
                            </div>
                            <div class="row center">
                                <button class="btn flat red" type="submit">تغییر شماره موبایل</button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                    <div class="col l6">
                        <form class="col l12 white card" method="POST" action="{{ route('activeuser') }}">

                            <div class="row">
                                <div class="input-field col s12 m12  l12">
                                    <h4 class="center countdown">60</h4>
                                </div>
                                <div class="input-field col l12">
                                    <i class="material-icons prefix">keyboard_hide</i>
                                    <input name="code" id="icon_prefix" type="text" class="validate">
                                    <label for="icon_prefix">Code</label>
                                </div>
                            </div>
                            <div class="row center">
                                <button class="btn flat indigo" type="submit">تایید کد یکبار مصرف</button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col l6 m12 s12 center">
                    <div class="card white">
                        <div class="card-content black-text">
                            <span class="card-title">درصد پیام کوتاه باقی مانده</span>
                            <h2>{{ $percentage}}%</h2>
                            <div class="progress grey">
                                <div class="determinate green accent-3" style="width: {{ $percentage . '%' }}"></div>
                            </div>
                        </div>
                        <div class="card-action">
                            <a class="black-text" href="{{ route('walletpage') }}">افزایش اعتبار پیام کوتاه</a>
                        </div>
                    </div>

                </div>
                <div class="col l6 s12 center">
                    <div class="card white">
                        <div class="card-content black-text">
                            <span class="card-title">تعداد پیام کوتاه باقی مانده</span>
                            <h2>{{ $sms_balance }}/{{$sms_total}}</h2>
                            <div class="progress grey">
                                <div class="determinate red accent-3" style="width: {{ $percentage . '%' }}"></div>
                            </div>
                        </div>
                        <div class="card-action">
                            <a class="black-text" href="{{ route('walletpage') }}">افزایش اعتبار پیام کوتاه</a>
                        </div>
                    </div>
                </div>
                <div class="col l6 s12 center">
                    <div class="card white">
                        <div class="card-content black-text">
                            <span class="card-title">تعدادنوبت های روز مشخص شده</span>
                            <h2>{{ $today_patient_total }}</h2>
                            @if(count($lim) > 0)
                                <div class="progress grey">
                                    <div class="determinate blue accent-3" style="width: @if($today_patient_total && $lim) {{ ($today_patient_total * 100) / $lim[0] }}%  @else 0% @endif"></div>
                                </div>
                            @else
                                <div class="determinate blue accent-3" style="width: 0%"></div>
                            @endif

                        </div>
                        <div class="card-action">
                            <a class="black-text" href="#">بخش امار</a>
                        </div>
                    </div>
                </div>
                <div class="col l6 s12 center">
                    <div class="card white">
                        <div class="card-content black-text">
                            <span class="card-title">محدودیت برای هر روز</span>
                            @if(count($lim) > 0)
                                @foreach($lim as $l)
                                <h2>{{ $l }}</h2>
                                @endforeach
                            @else
                                <h3>نا مشخص</h3>
                            @endif
                            <div class="progress grey">
                                @if(count($lim) > 0)
                                    <div class="determinate red accent-3" style="width: @if($lim) {{ ($lim[0]*100)/100}}%  @else 0% @endif"></div>
                                @else
                                    <div class="determinate red accent-3" style="width: 0%"></div>
                                @endif
                            </div>
                        </div>
                        <div class="card-action">
                            <a class="black-text" href="#">بخش امار</a>
                        </div>
                    </div>
                </div>

                <div class="col s12 m12 l4 ">
                    <form class="col s12 m12 l12 white card" method="GET" action="{{ route('dashboard') }}">
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">date_range</i>
                                <input name="datepicker" id="inlineExampleAlt" class="datepicker-demo datepicker"/>
                                <div class="inline-example"></div>
                            </div>
                        </div>
                        <div class="row center">
                            <button class="btn-flat white-text indigo darken-3" type="submit">جست و جوی نوبت</button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="col m12 s12 l8">
                    @if(count($appoiments) > 0)
                        <h5 class="center-align grey-text">{{verta($appoiments[0]->created_at)->format('Y-m-d')  }} : نوبت های ثبت شده در تاریخ </h5>
                        <div class="col s12 m12 l12 card ">
                            <table class="centered responsive-table highlight  rtl">
                                <thead>
                                <tr>
                                    <th>شماره</th>
                                    <th>نام</th>
                                    <th>نام خانوادگی</th>
                                    <th>ساعت مراجعه</th>
                                    <th>موبایل</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <?php $i = 0 ?>
                                @foreach($appoiments as $appoiment )
                                    @foreach($appoiment->patients as $patients )
                                        <tbody>
                                        <tr>
                                            <td>{{$appoiment->visitnum}}</td>
                                            <td> {{ $patients->name }} </td>
                                            <td> {{ $patients->lastname }} </td>
                                            <td> {{ verta($appoiment->updated_at)->format('H:i') }} </td>
                                            <td> {{ $patients->phone }} </td>
                                            <td>
                                                <a href="{{ route('reminder',['phone'=> $patients->phone ,'date' => verta($appoiments[0]->created_at)->format('Y-m-d') , 'time' => verta($appoiment->updated_at)->format('H:i')  ,'name'=> $patients->lastname ,'num' =>  $appoiment->visitnum ]) }}" class="btn-flat white-text green accent-3">یاداوری</a>
                                            </td>
                                            <td>
                                                <a href="{{ route('deleteappiment',['id' => $appoiment->id]) }}" class="btn-flat white-text red accent-3">حذف</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                @endforeach
                            </table>
                        </div>
                    @else
                        <div class="col s12 m12 l12 center">
                            <h4 class="grey-text">هیچ نوبتی در این تاریخ وجود ندارد</h4>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col s12 m12 l4 hide-on-med-and-down">
            <div class="row">
                <ul class="collection   right-align">
                    <li class="collection-item avatar ">
                        <div class="row left auto">
                            <img src="@if(Auth::user()->profile ==1) {{  URL::asset('/')}}image/doctor.jpg   @else {{ asset('storage/ucv/') }}/{{ Auth::user()->id }}.jpeg?x={{Verta::now()->timestamp}}" @endif" class="responsive-img blue-grey">
                        </div>
                        <div class="row center">
                            <span class="title ">:لینک عمومی</span>
                        </div>
                        <div class="row left left-auto ">
                            <div class="col s12 m12 l12">
                                <a href="{{ URL::asset('/')}}{{ 'ucv/'.Auth::user()->id }}" class="btn-flat black-text white fix-button bordera ">{{ URL::asset('/')}}{{ 'ucv/'.Auth::user()->id }}</a>
                            </div>
                        </div>
                        <div class="row">
                            <label class="right">این لینک در اختیار بیمار قرار میگیرد.تا عملیات رزرو را انجام
                                دهد.لطفا قبل از اشتراک لینک عمومی اطلاعات مورد نیاز کاربران را در بخش تنظیمات لینک عمومی
                                وارد کنید.</label>
                        </div>
                    </li>
                    <li class="collection-item avatar ">
                        <i class="material-icons circle indigo accent-3">fingerprint</i>
                        <span class="title ">:شناسه</span>
                        <p>IRU-{{ Auth::user()->id }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle black">supervisor_account</i>
                        <span class="title ">:نام</span>
                        <p>{{ Auth::user()->name }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle green accent-3">email</i>
                        <span class="title ">:ایمیل</span>
                        <p>{{ Auth::user()->email }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle red accent-3">local_phone</i>
                        <span class="title ">:موبایل</span>
                        <p>{{ Auth::user()->phone }}
                        </p>
                        <label class="center-align">این شماره فقط برای شما قابل رویت است و بیمار به ان دسترسی ندارد.برای
                            اضافه کردن شماره محل کار به بخش تنظیمات لینک عمومی مراجعه کنید</label>
                    </li>
                </ul>
            </div>
            <div class="col l12">
                <div class="card">
                    <div class="collection ">
                        <a href="{{ route('dashboard') }}" class="collection-item  right-align indigo active white-text">داشبورد</a>
                        <a href="{{ route('addpatientpage') }}" class="collection-item right-align	black-text">اضافه کردن
                            بیمار</a>
                        <a href="{{ route('addappoimentpage') }}" class="collection-item right-align	black-text">ثبت
                            نوبت</a>
                        <a href="{{ route('publicsetting') }}" class="collection-item right-align	black-text">تنظیمات
                            لینک عمومی</a>
                        <a href="{{ route('settingpage') }}" class="collection-item right-align	black-text">تنظیمات</a>
                    </div>
                </div>
            </div>


        </div>
        </div>
    </div>
@endsection
<!-- Modal Structure -->
<div id="modal2" class="modal">
    <div class="modal-content blue">
        <h4 class="white-text center rtl">کد فعال سازی را دریافت نکردید ؟</h4>
        <form action="{{ route('resendcode') }}" method="POST" class="center">
            <p class="bold center">اول شماره خود را مجددا چک کنید</p>
            <h5 class="white-text">{{ Auth::user()->phone }}</h5>
            <p class="bold center">شماره صحیح است ؟ دوباره کد ارسال کنید</p>
            <input class="btn-flat blackk-text amber center " type="submit" value="ارسال مجدد کد فعال سازی">
            {{ csrf_field() }}
        </form>
    </div>
    <div class="modal-footer indigo">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat black-text amber">متوجه شدم</a>
    </div>
</div>
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            var modlastate = 0;
            $('.modal-close').click(function () {
                $('#modal1').modal('close');
                modlastate = 1;
            });
            // Function to update counters on all elements with class counter
            var doUpdate = function () {
                $('.countdown').each(function () {
                    var count = parseInt($(this).html());
                    if (count !== 0) {
                        $(this).html(count - 1);
                    } else {
                        if (modlastate == 0) {
                            $('#modal2').modal('open');
                        }
                    }
                });
            };
            // Schedule the update to happen once every second
            setInterval(doUpdate, 1000);
            $('.modal').modal();
            $('.inline-example').persianDatepicker({
                inline: true,
                altField: '#inlineExampleAlt',
                altFormat: 'LLLL:X',
                autoClose: true,
                viewMode: 'day',
                toolbox: {
                    calendarSwitch: {
                        enabled: true
                    }
                },
                navigator: {
                    scroll: {
                        enabled: true
                    }
                },
                timePicker: {
                    enabled: false,
                    meridiem: {
                        enabled: true
                    }
                }

            });
        });
    </script>
@endsection