@extends('master.html')
@section('head')
	<meta name="robots" content="noindex, nofollow">
@endsection
@section('body')
	<div class="row">
		<nav class="indigo darken-4 center-align hide-on-med-and-down">
			<div class="nav-wrapper">
				<div class="col s12">
					<a href="" class="breadcrumb white-text bold right">منوی سریع</a>
					<a href="{{ route('dashboard') }}" class="breadcrumb grey-text  ">داشبورد</a>
					<a href="{{ route('addpatientpage') }}" class="breadcrumb  grey-text ">ثبت بیمار</a>
					<a href="{{ route('searchpatientpage') }}" class="breadcrumb  grey-text ">جست و جوی بیمار</a>
					<a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text  ">ثبت نوبت</a>
					<a href="{{ route('searchappoimentpage') }}" class="breadcrumb grey-text  ">جست و جوی نوبت</a>
					<a href="{{ route('publicsetting') }}" class="breadcrumb grey-text  ">تنظیمات رزرواسیون</a>
					<a href="{{ route('settingpage') }}" class="breadcrumb white-text bold">تنظیمات </a>
				</div>
			</div>
		</nav>
	</div>
	<div class="row">
		<div class="container">
			<div class="col l12">
				@if (Session::has('mesg'))
					<div class="container" id="message">
						<div class="row">
							<div class="col l12 s12 m12 center">
								<div  class="card-panel indigo accent-4">
						<span class="white-text">
							{{ Session::get('mesg')}}
						</span>
								</div>
							</div>
						</div>
					</div>
				@endif
				<div class="col l5">
					<div class="col l12 s12 m12 hide card-panel red darken-2 center-align white-text" id="error">
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<div class="card red">
								<div class="card-content white-text">
									<span class="card-title rtl">خطا</span>
									@foreach ($errors->all() as $error)
										<li class="right-align rtl">{{ $error }}</li>
									@endforeach
								</div>
							</div>
						</div>
					@endif
					<div class="col l12">
						<div class="card">
							<div class="card-image waves-effect waves-block waves-light" id="preimg">
								<img id="proimg" class="activator" src="@if(Auth::user()->profile ==1) {{  URL::asset('/')}}image/doctor.jpg   @else {{ asset('storage/ucv/') }}/{{ Auth::user()->id }}.jpeg?x={{ Verta::now()->timestamp }}" @endif " ">
							</div>
							<div class="card-content">
								<span class="card-title activator grey-text text-darken-4 rtl">تصویر پروفایل</span>
							</div>
						</div>
					</div>
					<div id="preloader" class="col s12 m12 l12 hide">
						<div class="progress">
							<div class="indeterminate"></div>
						</div>
					</div>
					<div class="col l12 white card">
						<form  id="data" action="#" enctype="multipart/form-data">
							<div class="file-field input-field">
								<div  class="btn indigo">
									<span>انتخاب تصویر</span>
									<input multiple="multiple"  id="profileimage"  name="profile" type="file">
								</div>
								<div class="file-path-wrapper">
									<input  class="file-path validate" type="text">
								</div>
							</div>
							<div class="row center">
								<input id="proclick" type="submit" class="btn-flat white-text green" value="ارسال تصویر">
							</div>
						</form>
					</div>

				</div>

				<div class="col l7">
					<div class="row">
						<h5 class="h4 right-align">تغییر شماره موبایل</h5>
					</div>
					<form class="col l12 white card " method="post" action="{{ route('changenumber') }}">
						<div class="row">
							<div class="input-field col l12 ">
								<i class="material-icons prefix">phone</i>
								<input name="phone" id="icon_prefix" type="text" class="validate center-align phonesize">
								<label class="right-align"  for="icon_prefix">شماره موبایل جدید</label>
							</div>
						</div>
						<div class="row center">
							<button class="btn flat green darken-3" type="submit"><i class="material-icons left">phone_forwarded</i>تغییر شماره</button>
						</div>
						{{ csrf_field() }}
					</form>
					<div class="row">
						<h5 class="h4 right-align">تایید شماره موبایل</h5>
					</div>
					<form class="col l12 white card " method="POST" action="{{ route('activeuser') }}">
						<div class="row">
							<div class="input-field col l12 ">
								<i class="material-icons prefix">keyboard_hide</i>
								<input name="code" id="icon_prefix" type="text" class="validate center-align phonesize">
								<label class="right-align"  for="icon_prefix">کد فعال سازی</label>
							</div>
						</div>
						<div class="row center">
							<button class="btn flat red darken-3" type="submit"><i class="material-icons left">fingerprint</i>تایید کد یکبار مصرف</button>
						</div>
						{{ csrf_field() }}
					</form>
					<div class="row">
						<h5 class="h4 right-align">تغییر رمز عبور</h5>
					</div>
					<form class="col l12 white card " method="POST" action="{{ route('changepassword') }}">
						<div class="row">
							<div class="input-field col l12">
								<i class="material-icons prefix">lock</i>
								<input name="lastpass" id="icon_prefix" type="password" class="validate center-align phonesize">
								<label for="icon_prefix">رمز عبور قدیمی</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col l12">
								<i class="material-icons prefix">lock_open</i>
								<input name="newpass" id="icon_prefix" type="password" class="validate center-align phonesize">
								<label for="icon_prefix">رمز عبور جدید</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col l12">
								<i class="material-icons prefix">lock_open</i>
								<input name="secendnewpass" id="icon_prefix" type="password" class="validate center-align phonesize">
								<label for="icon_prefix">تکرار رمز عبور جدید</label>
							</div>
						</div>


						<div class="row center">
							<button class="btn flat indigo darken-3" type="submit"><i class="material-icons left">add</i>ثبت تغییرات</button>
						</div>
						{{ csrf_field() }}
					</form>
				</div>

			</div>
		</div>
	</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
			$("#proclick").click(function () {
				$('#preloader').removeClass('hide');
            });
            $("form#data").submit(function(e) {
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                var CSRF_TOKEN = $("input[name=_token]").val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': CSRF_TOKEN
                    },
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
                    },
                    url: "{{ route('profilestore') }}",
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        var dt = Date.now();
                        console.log(data);
                        $('#proimg').prop('src',"{{ asset('storage/ucv/') }}/{{ Auth::user()->id }}.jpeg?x="+dt);
                        $('#preloader').addClass('hide');
                    },
                    error: function(data){
                        $('#preloader').addClass('hide');
                        var errors = data.responseJSON;
                        for (i=1;i<=errors.errors.profile.length;i++){
                            $("#error").html("<h5 class='rtl'>"+errors.errors.profile[0]+"</h5>");
                            $('#error').removeClass('hide');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });


        });
    </script>

@endsection