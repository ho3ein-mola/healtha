@extends('master.html')
@section('head')
    <meta name="robots" content="noindex, nofollow">
@endsection
@section('body')
    <div class="row">
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb grey-text  ">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb  grey-text ">ثبت بیمار</a>
                    <a href="{{ route('searchpatientpage') }}" class="breadcrumb  grey-text ">جست و جوی بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb white-text bold ">ثبت نوبت</a>
                    <a href="{{ route('searchappoimentpage') }}" class="breadcrumb grey-text ">جست و جوی نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb grey-text ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="container">
            <div class="col s12 m12 l12">
                @if (Session::has('mesg'))
                    <div class="container" id="message">
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card-panel green accent-4">
						<span class="white-text">
							{{ Session::get('mesg')}}
						</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col s12 m12 l5">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <div class="card red">
                                <div class="card-content white-text rtl">
                                    <span class="card-title">خطا</span>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card indigo accent-3">
                        <div class="card-content white-text rtl ">
                            <table class="right-align">
                                <thead>
                                <tr>
                                    <th>نام</th>
                                    <th>نام خانوداگی</th>
                                    <th>شماره ی موبایل</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="first-name">XXXXXXXXXXX</td>
                                    <td class="last-name">XXXXXXXXXXX</td>
                                    <td class="phone-number">09XXXXXXXXX</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-action rtl">
                            <a class="white-text " href="#">مدریت بیمار</a>
                        </div>
                    </div>
                </div>
                <div class="col l7">
                    <form class="col l12 white card" method="POST" action="{{ route('appoimentstore') }}">

                        <div class="row">
                            <div class="input-field col l12 ">
                                <i class="material-icons prefix">phone</i>
                                <input name="phone" id="icon_prefix" type="text" class="validate  phonesize center-align">
                                <label for="icon_prefix">شماره ی موبایل</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col l12">
                                <i class="material-icons prefix">date_range</i>
                                <input name="datepicker" id="inlineExampleAlt" class="datepicker-demo datepicker"/>
                                <div class="inline-example"></div>
                            </div>
                        </div>

                        <div class="row center">
                            <button class="btn flat green darken-3" type="submit">ثبت نوبت<i class="material-icons left">add</i></button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>

            </div>
        </div>
    </div>



    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4 class="red-text center">بیمار با این شماره یافت نشد</h4>
            <p class="right-align bold">لطفا ابتدا بیمار را ثبت کنید</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <h4 class="green-text center">بیمار با این شماره یافت شد</h4>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
        </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript">
        $("#icon_prefix").focusout(function () {
            var phone = $('#icon_prefix').val();
            var CSRF_TOKEN = $("input[name=_token]").val();
            $.ajax
            ({
                headers: {
                    'X-CSRF-TOKEN': CSRF_TOKEN
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
                },
                type: "get",
                url: "{{ route('patienpreview') }}",
                data: {
                    phone: phone,
                    _token: "CSRF_TOKEN"
                },
                error: function () {
                    $('#modal1').modal('open');
                },
                success: function (data) {
                    $('#modal2').modal('open');
                    $('.first-name').text(data[0]['name']);
                    $('.last-name').text(data[0]['lastname']);
                    $('.phone-number').text(data[0]['phone']);
                }
            });
        });
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('.modal').modal();
            $('.inline-example').persianDatepicker({
                inline: true,
                altField: '#inlineExampleAlt',
                altFormat: 'LLLL:X',
                autoClose: true,
                viewMode: 'month',
                toolbox: {
                    calendarSwitch: {
                        enabled: true
                    }
                },
                navigator: {
                    scroll: {
                        enabled: true
                    }
                },
                maxDate: new persianDate().add('month', 3).valueOf(),
                minDate: new persianDate().subtract('month', 0).valueOf(),
                timePicker: {
                    enabled: true,
                    meridiem: {
                        enabled: true
                    }
                }
            });
        });
    </script>
@endsection