@extends('master.html')
@section('head')
    <meta name="robots" content="noindex, nofollow">
    {!! Charts::styles() !!}
@endsection
@section('body')
    <div class="row">
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb white-text bold">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb grey-text">ثبت بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text">ثبت نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb grey-text ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="col l8">
            <div class="row">
                @if (Session::has('mesg'))
                    <div class="container" id="message">
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card-panel green accent-4">
						<span class="white-text">
							<h5 class="center-align">{{ Session::get('mesg')}}</h5>
						</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <div class="card red">
                            <div class="card-content white-text rtl">
                                <span class="card-title">خطا</span>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="card white rtl ">
                        <div class="card-content black-text center bold ">
                            <h5 class="card-title">اعتبار پیامک</h5>
                            @if($sms_balance)
                                <h3 class="center-align">{{ $sms_balance }} </h3>
                            @else
                                <h3 class="center-align">0 تومان</h3>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l6">
                    <div class="card white rtl ">
                        <div class="card-content black-text center">
                            <h5 class="card-title">اعتبار کیف پول</h5>
                            @if($wallet)
                                <h3 class="center-align">{{ $wallet->amount }} تومان</h3>
                            @else
                                <h3 class="center-align">0 تومان</h3>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h4 class="center-align grey-text">افزایش اعتبار کیف پول</h4>
                <div class="card">
                    <div class="card-content black-text ">
                            <div class="row">
                                <div class="col s12 m12 l6">
                                    <div class="col s12 m12 l6 center">
                                        <script src="https://www.zarinpal.com/webservice/TrustCode" type="text/javascript" ></script>
                                    </div>
                                    <div class="col s12 m12 l6 center">
                                        <img src="image/up.png" alt="" class="responsive-img">
                                    </div>
                                    <div class="col s12 m12 l12 center">
                                        <img src="image/allbank.png" alt="" class="responsive-img">
                                    </div>
                                </div>
                                <div class="col s12 m12 l6">
                                    <form action="{{ route('getway') }}" method="get">
                                        <div class="input-field col s12 m12 l12 center-align">
                                            <p class="rtl center phonesize"> پرداخت با کلیه کارت های عضو شتاب یا  USSD</p>
                                        </div>
                                        <div class="input-field col s12 m12 l12">
                                            <i class="material-icons prefix">payment</i>
                                            <input name="amount" id="icon_prefix" type="text" class="validate">
                                            <label for="icon_prefix">مبلغ</label>
                                        </div>
                                        <div class="input-field col s12 m12  l8 offset-l2 center" >
                                            <a class="btn-flat white-text indigo fix-button" disabled >مبلغ را به تومان وارد نمایید </a>
                                        </div>
                                        <div class="input-field col s12 m12 l8 offset-l2 center">
                                            <input type="submit" class="btn-flat green  white-text fix-button" value="پرداخت">
                                        </div>
                                        <div class="input-field col s12 m12 l8 offset-l2  center diver"></div>
                                        <div class="input-field col s12 m12 l8 offset-l2  center">
                                            <a href="{{ route('paymentpage') }}" class="btn-flat amber black-text fix-button">سابقه پرداخت</a>
                                        </div>
                                        <div class="input-field col s12 m12 l8 offset-l2  center diver"></div>
                                        <div class="input-field col s12 m12 l8 offset-l2  center">
                                            <h5>مشکلی به وجود امده ؟</h5>
                                        </div>
                                        <div class="input-field col s12 m12 l8 offset-l2  center">
                                            <a href="{{ route('paymentpage') }}" class="btn-flat blue white-text fix-button">تماس با پشتیبانی</a>
                                        </div>
                                        {{ csrf_field() }}
                                    </form>
                                    <div class="col l12 s12 m12">

                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h4 class="center-align grey-text">خرید بسته پیامک</h4>
                <div class="card white">
                    <div class="card-content black-text  ">
                        <div class="row">
                            <div class="container">
                                <h4 id="sms_num" class="center-align rtl bordera  green lighten-5"><span id="num">0</span><span>  پیامک</span></h4>
                                <h5 id="sms_num" class="center-align rtl bordera  yellow lighten-5"><span id="cost">0</span><span>  تومان</span></h5>
                                <form action="{{ route('smsbuy') }}" method="get" class="center">
                                    <div class="input-field col s12 m12 l12">
                                        <p class="range-field">
                                            <input name="sms"  type="range" step="50" id="sms_range" min="0" max="1000" />
                                        </p>
                                    </div>
                                    <div class="input-field col s12 m12 l12">
                                        <input type="submit"  value="خرید با موجودی کیف پول" class="btn-flat white-text indigo " />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m12 l4 hide-on-med-and-down">
            <div class="row">
                <ul class="collection   right-align">
                    <li class="collection-item avatar ">
                        <div class="row left auto">
                            <img src="@if(Auth::user()->profile ==1) {{  URL::asset('/')}}image/doctor.jpg   @else {{ asset('storage/ucv/') }}/{{ Auth::user()->id }}.jpeg?x={{Verta::now()->timestamp}}" @endif" class="responsive-img blue-grey">
                        </div>
                        <div class="row center">
                            <span class="title ">:لینک عمومی</span>
                        </div>
                        <div class="row left left-auto">
                            <a href="{{ URL::asset('/')}}{{ 'ucv/'.Auth::user()->id }}" class="btn-flat black-text white fix-button bordera ">{{ URL::asset('/')}}{{ 'ucv/'.Auth::user()->id }}</a>
                        </div>
                        <div class="row">
                            <label class="right">این لینک در اختیار بیمار قرار میگیرد.تا عملیات رزرو را انجام
                                دهد.لطفا قبل از اشتراک لینک عمومی اطلاعات مورد نیاز کاربران را در بخش تنظیمات لینک عمومی
                                وارد کنید.</label>
                        </div>
                    </li>
                    <li class="collection-item avatar ">
                        <i class="material-icons circle indigo accent-3">fingerprint</i>
                        <span class="title ">:شناسه</span>
                        <p>IRU-{{ Auth::user()->id }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle black">supervisor_account</i>
                        <span class="title ">:نام</span>
                        <p>{{ Auth::user()->name }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle green accent-3">email</i>
                        <span class="title ">:ایمیل</span>
                        <p>{{ Auth::user()->email }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle red accent-3">local_phone</i>
                        <span class="title ">:موبایل</span>
                        <p>{{ Auth::user()->phone }}
                        </p>
                        <label class="center-align">این شماره فقط برای شما قابل رویت است و بیمار به ان دسترسی ندارد.برای
                            اضافه کردن شماره محل کار به بخش تنظیمات لینک عمومی مراجعه کنید</label>
                    </li>
                </ul>
            </div>
            <div class="col l12">
                <div class="card">
                    <div class="collection ">
                        <a href="{{ route('dashboard') }}" class="collection-item  right-align indigo active white-text">داشبورد</a>
                        <a href="{{ route('addpatientpage') }}" class="collection-item right-align	black-text">اضافه کردن
                            بیمار</a>
                        <a href="{{ route('addappoimentpage') }}" class="collection-item right-align	black-text">ثبت
                            نوبت</a>
                        <a href="{{ route('publicsetting') }}" class="collection-item right-align	black-text">تنظیمات
                            لینک عمومی</a>
                        <a href="{{ route('settingpage') }}" class="collection-item right-align	black-text">تنظیمات</a>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection

<!-- Modal Structure -->
<div id="modal2" class="modal">
    <div class="modal-content">
        <h4 class="red-text center"> ?کدی دریافت نکردید</h4>
        <p class="bold center">اول شماره خود را مجددا چک کنید</p>
        <p class="bold center">از طریق لینک زیر اقدام کنید</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            function itpro(Number)
            {
                Number+= '';
                Number= Number.replace(',', ''); Number= Number.replace(',', ''); Number= Number.replace(',', '');
                Number= Number.replace(',', ''); Number= Number.replace(',', ''); Number= Number.replace(',', '');
                x = Number.split('.');
                y = x[0];
                z= x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(y))
                    y= y.replace(rgx, '$1' + ',' + '$2');
                return y+ z;
            };
            $('.range-field').change(function () {
                var num = $('#sms_range').val();
                var cost = 18.5 * num;
                $("#num").text(num);
                $("#cost").text(itpro(cost));
            });
        });
    </script>
@endsection