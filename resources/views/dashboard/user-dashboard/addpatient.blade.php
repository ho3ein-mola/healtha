@extends('master.html')
@section('head')
    <meta name="robots" content="noindex, nofollow">
@endsection
@section('body')
    <div class="row">
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb grey-text  ">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb  white-text">ثبت بیمار</a>
                    <a href="{{ route('searchpatientpage') }}" class="breadcrumb  grey-text ">جست و جوی بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text ">ثبت نوبت</a>
                    <a href="{{ route('searchappoimentpage') }}" class="breadcrumb grey-text ">جست و جوی نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb grey-text ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="container">
            <div class="col s12 m12 l12 center-align">
                @if (Session::has('mesg'))
                    <div class="container" id="message">
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card-panel green accent-4">
						<span class="white-text">
							{{ Session::get('mesg')}}
						</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col s12 m12 l5">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <div class="card red">
                                <div class="card-content white-text">
                                    <span class="card-title rtl">خطا</span>
                                    @foreach ($errors->all() as $error)
                                        <li class="right-align rtl">{{ $error }}</li>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card indigo accent-3">
                        <div class="card-content white-text right-align">
                            <span class="card-title">ثبت بیمار</span>
                            <p>
                                شماره ی موبایل باید منحصر به فرد باشد . همه ی نوبت ها بر اساس شماره موبایل ثبت می شود و همه ی پیام های کوتاه اطلاع رسانی هم از  طریق همین شماره انجام می شود
                            </p>
                        </div>
                        <div class="card-action rtl">
                            <a class="white-text" href="#">توضیحات بیشتر</a>
                        </div>
                    </div>

                    <div class="card green accent-3">
                        <div class="card-content black-text right-align">
                            <span class="card-title">توجه</span>
                            <p>
                                هر بیمار فقط یکبار ثبت میشود و برای دفعات بعدی فقط کافی است تا به صفحه ی ثبت نوبت رفته و شماره ی بیمار را وارد نمایید.
                            </p>
                        </div>
                        <div class="card-action rtl">
                            <a class="black-text" href="#">توضیحات بیشتر</a>
                        </div>
                    </div>

                </div>
                <div class="col s12 m12 l7">
                    <form class="col l12 white card " method="POST" action="{{ route('addpatient') }}">
                        <div class="row">
                            <div class="input-field col s12 m12 l12 ">
                                <i class="material-icons prefix">account_circle</i>
                                <input name="name" id="icon_prefix" type="text" class="validate right-align">
                                <label class="right-align"  for="icon_prefix">نام</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">assignment_ind</i>
                                <input name="lastname" id="icon_prefix" type="text" class="validate right-align">
                                <label for="icon_prefix">نام خانوادگی</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m12 l12">
                                <i class="material-icons prefix">phone</i>
                                <input name="phone" id="icon_prefix" type="text" class="validate center-align phonesize">
                                <label for="icon_prefix">شماره بیمار</label>
                            </div>
                        </div>


                        <div class="row center">
                            <button class="btn flat indigo darken-3" type="submit"><i class="material-icons left">add</i>ثبت بیمار</button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection