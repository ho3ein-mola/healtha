@extends('master.html')
@section('head')
    <meta name="robots" content="noindex, nofollow">
    {!! Charts::styles() !!}
@endsection
@section('body')
    <div class="row">
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb white-text bold">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb grey-text">ثبت بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text">ثبت نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb grey-text ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="col s12 m12 l8">
            <div class="row">
                @if (Session::has('mesg'))
                    <div class="container" id="message">
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card-panel green accent-4">
						<span class="white-text">
							<h5 class="center-align">{{ Session::get('mesg')}}</h5>
						</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <div class="card red">
                            <div class="card-content white-text rtl">
                                <span class="card-title">خطا</span>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <div class="row">
                <h4 class="center-align grey-text">سابقه ۲۰ تراکنش اخر</h4>
                <div class="col m12 s12 l12">
                    @if(count($pay) > 0)
                        <div class="col s12 m12 l12 card ">
                            <table class="centered responsive-table highlight  rtl">
                                <thead>
                                <tr>
                                    <th>شماره</th>
                                    <th>مبلغ</th>
                                    <th>تاریخ</th>
                                    <th>ساعت</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                               <?php $i = 0 ?>
                            @foreach($pay as $pa )
                                    <?php $i++ ?>
                                    <tbody>
                                        <tr class=" @if($pa->status == 1) light-green lighten-4 @else red lighten-4 @endif">
                                            <td>{{ $i }}</td>
                                            <td>{{$pa->amount . ' '}}  تومان</td>
                                            <td> {{ verta($pa->created_at)->format('Y-n-j')}} </td>
                                            <td> {{ verta($pa->created_at)->format('H:i')}} </td>
                                            <td> @if($pa->status == 1)  {{ "افزایش اعتبار" }} @else {{ "کاهش اعتبار" }} @endif </td>
                                        </tr>
                                        </tbody>
                                @endforeach
                            </table>
                        </div>
                    @else
                        <div class="col s12 m12 l12 center">
                            <h4 class="grey-text">هیچ تراکنشی وجود ندارد</h4>
                        </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="col s12 m12 l4 hide-on-med-and-down">
            <div class="row">
                <ul class="collection   right-align">
                    <li class="collection-item avatar ">
                        <div class="row left auto">
                            <img src="@if(Auth::user()->profile ==1) {{  URL::asset('/')}}image/doctor.jpg   @else {{ asset('storage/ucv/') }}/{{ Auth::user()->id }}.jpeg?x={{Verta::now()->timestamp}}" @endif" class="responsive-img blue-grey">
                        </div>
                        <div class="row center">
                            <span class="title ">:لینک عمومی</span>
                        </div>
                        <div class="row left left-auto">
                            <a href="{{ URL::asset('/')}}{{ 'ucv/'.Auth::user()->id }}" class="btn-flat black-text white fix-button bordera ">{{ URL::asset('/')}}{{ 'ucv/'.Auth::user()->id }}</a>
                        </div>
                        <div class="row">
                            <label class="right">این لینک در اختیار بیمار قرار میگیرد.تا عملیات رزرو را انجام
                                دهد.لطفا قبل از اشتراک لینک عمومی اطلاعات مورد نیاز کاربران را در بخش تنظیمات لینک عمومی
                                وارد کنید.</label>
                        </div>
                    </li>
                    <li class="collection-item avatar ">
                        <i class="material-icons circle indigo accent-3">fingerprint</i>
                        <span class="title ">:شناسه</span>
                        <p>IRU-{{ Auth::user()->id }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle black">supervisor_account</i>
                        <span class="title ">:نام</span>
                        <p>{{ Auth::user()->name }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle green accent-3">email</i>
                        <span class="title ">:ایمیل</span>
                        <p>{{ Auth::user()->email }}
                        </p>
                    </li>
                    <li class="collection-item avatar">
                        <i class="material-icons circle red accent-3">local_phone</i>
                        <span class="title ">:موبایل</span>
                        <p>{{ Auth::user()->phone }}
                        </p>
                        <label class="center-align">این شماره فقط برای شما قابل رویت است و بیمار به ان دسترسی ندارد.برای
                            اضافه کردن شماره محل کار به بخش تنظیمات لینک عمومی مراجعه کنید</label>
                    </li>
                </ul>
            </div>
            <div class="col l12">
                <div class="card">
                    <div class="collection ">
                        <a href="{{ route('dashboard') }}" class="collection-item  right-align indigo active white-text">داشبورد</a>
                        <a href="{{ route('addpatientpage') }}" class="collection-item right-align	black-text">اضافه کردن
                            بیمار</a>
                        <a href="{{ route('addappoimentpage') }}" class="collection-item right-align	black-text">ثبت
                            نوبت</a>
                        <a href="{{ route('publicsetting') }}" class="collection-item right-align	black-text">تنظیمات
                            لینک عمومی</a>
                        <a href="{{ route('settingpage') }}" class="collection-item right-align	black-text">تنظیمات</a>
                    </div>
                </div>
            </div>


        </div>

    </div>
@endsection

<!-- Modal Structure -->
<div id="modal2" class="modal">
    <div class="modal-content">
        <h4 class="red-text center"> ?کدی دریافت نکردید</h4>
        <p class="bold center">اول شماره خود را مجددا چک کنید</p>
        <p class="bold center">از طریق لینک زیر اقدام کنید</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            function itpro(Number)
            {
                Number+= '';
                Number= Number.replace(',', ''); Number= Number.replace(',', ''); Number= Number.replace(',', '');
                Number= Number.replace(',', ''); Number= Number.replace(',', ''); Number= Number.replace(',', '');
                x = Number.split('.');
                y = x[0];
                z= x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(y))
                    y= y.replace(rgx, '$1' + ',' + '$2');
                return y+ z;
            };
            $('.range-field').change(function () {
                var num = $('#sms_range').val();
                var cost = 18.5 * num;
                $("#num").text(num);
                $("#cost").text(itpro(cost));
            });
        });
    </script>
@endsection