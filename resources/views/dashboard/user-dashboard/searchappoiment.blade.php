@extends('master.html')
@section('head')
    <meta name="robots" content="noindex, nofollow">
    <style>
        @media print{
            body{ background-color:#FFFFFF; background-image:none; color:#000000 }
            .hide-on-med-and-down{ display:none;}
            #printformpicker{ display:none;}
            #print{ display:none;}
            #act{display:none;}
        }
    </style>
@endsection
@section('body')
    <div class="row">
        <nav class="indigo darken-4 center-align hide-on-med-and-down">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="" class="breadcrumb white-text bold right">منوی سریع</a>
                    <a href="{{ route('dashboard') }}" class="breadcrumb grey-text  ">داشبورد</a>
                    <a href="{{ route('addpatientpage') }}" class="breadcrumb  grey-text ">ثبت بیمار</a>
                    <a href="{{ route('searchpatientpage') }}" class="breadcrumb  grey-text ">جست و جوی بیمار</a>
                    <a href="{{ route('addappoimentpage') }}" class="breadcrumb grey-text  ">ثبت نوبت</a>
                    <a href="{{ route('searchappoimentpage') }}" class="breadcrumb white-text bold ">جست و جوی نوبت</a>
                    <a href="{{ route('publicsetting') }}" class="breadcrumb grey-text ">تنظیمات رزرواسیون</a>
                    <a href="{{ route('settingpage') }}" class="breadcrumb grey-text ">تنظیمات </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="container">
            <div class="col s12 m12 l12">
                @if (Session::has('mesg'))
                    <div class="container" id="message">
                        <div class="row">
                            <div class="col l12 s12 m12">
                                <div class="card-panel green accent-4">
                                    <h5 class="white-text center">
                                        {{ Session::get('mesg')}}
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col s12 m12 l4 ">
                        <a id="print" class="fix-button waves-effect waves-light btn-flat white-text deep-purple accent-3 center "><i class="material-icons right">print</i>پرینت تمام نوبت های امروز</a>
                        <form id="printformpicker" class="col s12 m12 l12 white card" method="GET" action="{{ route('searchappoimentpage') }}">
                            <div class="row">
                                <div class="input-field col s12 m12 l12">
                                    <i class="material-icons prefix">date_range</i>
                                    <input name="datepicker" id="inlineExampleAlt" class="datepicker-demo datepicker"/>
                                    <div class="inline-example"></div>
                                </div>
                            </div>
                            <div class="row center">
                                <button class="btn-flat white-text indigo darken-3" type="submit">جست و جوی نوبت</button>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>
                    <div class="col m12 s12 l8">
                        @if(count($appoiments) > 0)
                            <h5 class="center-align grey-text">{{verta($appoiments[0]->created_at)->format('Y-m-d')  }} : نوبت های ثبت شده در تاریخ </h5>
                            <div class="col s12 m12 l12 card ">
                                <table class="centered responsive-table highlight  rtl">
                                    <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>ساعت مراجعه</th>
                                        <th>موبایل</th>
                                        <th id="act">عملیات</th>
                                    </tr>
                                    </thead>
                                    <?php $i = 0 ?>
                                    @foreach($appoiments as $appoiment )
                                        @foreach($appoiment->patients as $patients )
                                            <tbody>
                                            <tr>
                                                <td>{{$appoiment->visitnum}}</td>
                                                <td> {{ $patients->name }} </td>
                                                <td> {{ $patients->lastname }} </td>
                                                <td> {{ verta($appoiment->updated_at)->format('H:i') }} </td>
                                                <td> {{ $patients->phone }} </td>
                                                <td id="act">
                                                    <a href="{{ route('reminder',['phone'=> $patients->phone ,'date' => verta($appoiments[0]->created_at)->format('Y-m-d') , 'time' => verta($appoiment->updated_at)->format('H:i')  ,'name'=> $patients->lastname ,'num' =>  $appoiment->visitnum ]) }}" class="btn-flat white-text green accent-3">یاداوری</a>
                                                </td>
                                                <td id="act">
                                                    <a  href="{{route("deleteappiment",['id'=> $appoiment->id])   }}" class="btn-flat white-text red accent-3">حذف</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        @endforeach
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <div class="col s12 m12 l12 center">
                                <h4 class="grey-text">هیچ نوبتی در این تاریخ وجود ندارد</h4>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col s12 m12 l5">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <div class="card red">
                                <div class="card-content white-text rtl">
                                    <span class="card-title">خطا</span>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>



    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4 class="red-text center">بیمار با این شماره یافت نشد</h4>
            <p class="right-align bold">لطفا ابتدا بیمار را ثبت کنید</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <h4 class="green-text center">بیمار با این شماره یافت شد</h4>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">متوجه شدم</a>
        </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript">
        $("#icon_prefix").focusout(function () {
            var phone = $('#icon_prefix').val();
            var CSRF_TOKEN = $("input[name=_token]").val();
            $.ajax
            ({
                headers: {
                    'X-CSRF-TOKEN': CSRF_TOKEN
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
                },
                type: "get",
                url: "{{ route('patienpreview') }}",
                data: {
                    phone: phone,
                    _token: "CSRF_TOKEN"
                },
                error: function () {
                    $('#modal1').modal('open');
                },
                success: function (data) {
                    $('#modal2').modal('open');
                    $('.first-name').text(data[0]['name']);
                    $('.last-name').text(data[0]['lastname']);
                    $('.phone-number').text(data[0]['phone']);
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#print').click(function () {
                window.print();
            });
            $('.modal').modal();
            $('.inline-example').persianDatepicker({
                inline: true,
                altField: '#inlineExampleAlt',
                altFormat: 'LLLL:X',
                autoClose: true,
                viewMode: 'day',
                toolbox: {
                    calendarSwitch: {
                        enabled: true
                    }
                },
                navigator: {
                    scroll: {
                        enabled: true
                    }
                },
                timePicker: {
                    enabled: true,
                    meridiem: {
                        enabled: true
                    }
                }
            });
        });
    </script>
@endsection