@extends('master.html')
@section('body')
    <br>
    @if (Session::has('mesg'))
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l8 offset-l2">
                    <div class="card-panel red">
                          <h5 class="white-text center-align">
                            {{ Session::get('mesg')}}
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l8 offset-l2">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <div class="card red">
                            <div class="card-content white-text rtl">
                                <span class="card-title">خطا</span>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <div class="col s12 m12 l8 offset-l2">
                <form class="col s12 m12 l12 white card" method="POST" action="{{ route('signin') }}">


                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix">phone</i>
                            <input name="phone" id="icon_prefix" type="text" class="validate phonesize center"
                                   value="{{ old('phone') }}">
                            <label for="icon_prefix">شماره موبایل</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m12 l12">
                            <i class="material-icons prefix">enhanced_encryption</i>
                            <input name="password" id="icon_prefix" type="password" class="validate">
                            <label for="icon_prefix">پسورد</label>
                        </div>
                    </div>


                    <div class="row center">
                        <button class="btn-flat white-text green" type="submit">ورود به سامانه</button>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection