@extends('master.html')
@section('head')
    <style>
        .datepicker-plot-area .datepicker-day-view .table-days td span.other-month {
            background-color: #311b92;
            color: white;
            border: none;
            text-shadow: none;
        }

        .datepicker-plot-area .datepicker-day-view .table-days td.disabled span, .datepicker-plot-area .datepicker-year-view .year-item-disable,
        .datepicker-plot-area .datepicker-month-view .month-item-disable {
            background-color: #d32f2f !important;
            color: white;
            border: none;
            text-shadow: none;
            cursor: default;
        }

        .datepicker-plot-area .datepicker-day-view .table-days td span, .datepicker-plot-area .datepicker-year-view .year-item,
        .datepicker-plot-area .datepicker-month-view .month-item {
            font: 14px;
            background-color: #388e3c;
            color: white;
            border: 0;
            text-shadow: none;
        }
    </style>
@endsection
@section('body')
    @if($user)
        @if($public)
            @if($public->active == 1)
                <div class="row">
                    <div class="col s12 m12 l3 rtl  hide-on-med-and-up">
                        <div class="card  Small horizontal">
                            <div class="card-image">
                                <img src="@if($user->profile ==1) {{  URL::asset('/')}}image/doctor.jpg   @else {{ asset('storage/ucv/') }}/{{ $user->id }}.jpeg?x={{Verta::now()->timestamp}}" @endif " class="responsive-img">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content ">
                                    <div class="row">
                                        <button class="btn-flat  black-text white fix-button " href="#"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m12 l3 hide-on-med-and-up ">
                        <div class="row">

                            <ul class="collection right-align">
                                <li class="collection-item avatar">
                                    <i class="material-icons circle indigo accent-3">fingerprint</i>
                                    <span class="title">User ID</span>
                                    @if($public)
                                        <p>UCV-{{ $public->user_id}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>
                                <li class="collection-item avatar">
                                    <i class="material-icons circle black">account_circle</i>
                                    <span class="title">نام</span>
                                    @if($public)
                                        <p>{{ $public->name}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>
                                <li class="collection-item avatar">
                                    <i class="material-icons circle black">supervisor_account</i>
                                    <span class="title">نام خانوادگی</span>
                                    @if($public)
                                        <p>{{ $public->lastname}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>

                                <li class="collection-item avatar">
                                    <i class="material-icons circle green ">local_phone</i>
                                    <span class="title">تلفن</span>
                                    @if($public)
                                        <p>{{ $public->telephone}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>
                                <li class="collection-item avatar">
                                    <i class="material-icons circle red  darken-3">access_time</i>
                                    <span class="title">از ساعت</span>
                                    @if($public)
                                        <p>{{ $public->open}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                        <br>
                                        <i class="material-icons circle red darken-3">history</i>
                                        <span class="title">تا ساعت</span>
                                        @if($public)
                                            <p>{{ $public->close}}
                                        @else
                                            <p>there is no user</p>
                                            @endif
                                            </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col l9">
                        @if (Session::has('mesg'))
                            <div class="container" id="message">
                                <div class="row">
                                    <div class="col l12 s12 m12">
                                        <div class="card-panel red accent-4">
                                            <h5 class="white-text center">
                                                {{ Session::get('mesg')}}
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col l5">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <div class="card red">
                                        <div class="card-content white-text">
                                            <span class="card-title">Errors</span>
                                            @foreach ($errors->all() as $error)
                                               <h5 class="center-align"><li>{{ $error }}</li></h5>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="card  indigo ">
                                <div class="card-content white-text">
                                    <span class="card-title right-align">ادرس دقیق پزشک</span>
                                    <input name="mapstring" type="text" id="us2-address" class="right-align "/>
                                    <div id="us2" style="width: 100%; height: 50vh;"></div>
                                    <br>
                                    <span for="" class="right">
                            عرض جغرافیایی
                        </span>
                                    <input name="lat" type="text" id="us2-lat"/>
                                    <span for="" class="right">
                            طول جغرافیایی
                        </span>
                                    <input name="lon" type="text" id="us2-lon"/>
                                </div>
                            </div>

                        </div>
                        <div class="col l7">

                                <div class="col s12 m12 l12 right-align">
                                    <div class="card  white">
                                        <div class="card-content blue-grey-text">
                                            <span class="card-title center-align">تابلوی اعلانات</span>
                                            @if($public)
                                                <p>{{ $public->info }}</p>
                                            @else
                                                <p></p>
                                            @endif
                                        </div>
                                        <div class="card-action">
                                            <a href="#">اینستاگرام</a>
                                            <a href="#">تلگرام</a>
                                        </div>
                                    </div>
                                </div>

                            <form class="col l12 white card" method="post"
                                  action="{{ route('publicstore',['id'=>$user->id]) }}">
                                <div class="row">
                                    <div class="input-field col s12 m12 l12 ">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input name="name" id="icon_prefix" type="text" class="validate right-align">
                                        <label class="rtl" for="icon_prefix">نام</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m12l12">
                                        <i class="material-icons prefix">assignment_ind</i>
                                        <input name="lastname" id="icon_prefix" type="text" class="validate right-align">
                                        <label class="rtl" for="icon_prefix">نام خانوادگی</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">phone</i>
                                        <input name="phone" id="icon_prefix" type="text" class="validate phonesize center-align">
                                        <label class="rtl" for="icon_prefix">موبایل</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m12 l12">
                                        <div class="input-field col l12 center ">
                                            <h5 class="rtl">لطفا از جدول پایین یک روز را انتخاب کنید</h5>
                                        </div>
                                        <div class="input-field col s12 m12 l12 center-align ">
                                            <i class="material-icons prefix">date_range</i>
                                            <input name="datepicker" id="inlineExampleAlt"
                                                   class="datepicker-demo datepicker  right-align  ">
                                        </div>

                                        <div class="input-field col s12 m12 l12 center-align ">
                                            <span>راهنما</span>
                                        </div>
                                        <div class="input-field col s12 m12 l12 center">
                                            <div class="btn-flat red darken-2 white-text">ظرفیت تکمیل</div>
                                            <div class="btn-flat green darken-2 white-text">ظرفیت باز</div>
                                            <div class="btn-flat deep-purple darken-4 white-text">ظرفیت باز در ماه اینده</div>
                                        </div>
                                        <div class="input-field col s12 m12l12 center">
                                            <div class="inline-example"></div>
                                        </div>
                                    </div>
                                    <br>
                                </div>

                                <div class="row center">
                                    <button class="btn flat indigo darken-3" type="submit"><i class="material-icons left">alarm_add</i>ثبت نوبت</button>
                                </div>
                                {{ csrf_field() }}
                            </form>

                        </div>

                    </div>
                    <div class="col s12 m12 l3 rtl  hide-on-med-and-down">
                        <div class="card  Small horizontal">
                            <div class="card-image">
                                <img src="@if($user->profile ==1) {{  URL::asset('/')}}image/doctor.jpg   @else {{ asset('storage/ucv/') }}/{{ $user->id }}.jpeg?x={{Verta::now()->timestamp}}" @endif " class="responsive-img">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content ">
                                    <div class="row">
                                        <button class="btn-flat  black-text white fix-button " href="#"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m12 l3  hide-on-med-and-down">
                        <div class="row">
                            <ul class="collection right-align">
                                <li class="collection-item avatar">
                                    <i class="material-icons circle indigo accent-3">fingerprint</i>
                                    <span class="title">User ID</span>
                                    @if($public)
                                        <p>UCV-{{ $public->user_id}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>
                                <li class="collection-item avatar">
                                    <i class="material-icons circle black">account_circle</i>
                                    <span class="title">نام</span>
                                    @if($public)
                                        <p>{{ $public->name}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>
                                <li class="collection-item avatar">
                                    <i class="material-icons circle black">supervisor_account</i>
                                    <span class="title">نام خانوادگی</span>
                                    @if($public)
                                        <p>{{ $public->lastname}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>

                                <li class="collection-item avatar">
                                    <i class="material-icons circle green ">local_phone</i>
                                    <span class="title">تلفن</span>
                                    @if($public)
                                        <p>{{ $public->telephone}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                </li>
                                <li class="collection-item avatar">
                                    <i class="material-icons circle red  darken-3">access_time</i>
                                    <span class="title">از ساعت</span>
                                    @if($public)
                                        <p>{{ $public->open}}
                                    @else
                                        <p>there is no user</p>
                                        @endif
                                        </p>
                                        <br>
                                        <i class="material-icons circle red darken-3">history</i>
                                        <span class="title">تا ساعت</span>
                                        @if($public)
                                            <p>{{ $public->close}}
                                        @else
                                            <p>there is no user</p>
                                            @endif
                                            </p>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            @else
                <div class="container right-align">
                    <div class="col l12">
                        <div class="card red accent-3">
                            <div class="card-content white-text">
                                <span class="card-title">خطا</span>
                                <h5 class="center-align">امکان رزرو توسط پزشک غیره فعال شده است</h5>
                            </div>
                            <div class="card-action">
                                <a class="white-text" href="#">اطلاعات بیشتر</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @else
            <div class="container right-align">
                <div class="col l12">
                    <div class="card green accent-3">
                        <div class="card-content white-text">
                            <span class="card-title">خطا</span>
                            <h5 class="center-align">امکان رزرو غیره فعال شده است</h5>
                        </div>
                        <div class="card-action">
                            <a class="white-text" href="#">اطلاعات بیشتر</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @else
        <div class="container right-align">
            <div class="col l12">
                <div class="card red accent-3">
                    <div class="card-content white-text">
                        <span class="card-title">خطا</span>
                        <h5 class="center-align">پزشک مورد نظر یافت نشد . لطفا ادرس صحیح را وارد کنید</h5>
                    </div>
                    <div class="card-action">
                        <a class="white-text" href="#">اطلاعات بیشتر</a>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4 class="red-text center">User Not Found</h4>
            <p>Please Enter Your User First</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <h4 class="green-text center">User Found</h4>
            <p>User Exist</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
        </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript"
            src='http://maps.google.com/maps/api/js?language=fa&key=AIzaSyAkR9vhY1uZ0EZfJpB3YqFMyDsLvO2xO5o&sensor=false&libraries=places'></script>
    <script src="{{ URL::asset('/')}}js/locationpicker.jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            /*//disable back  ucv page after add appoiment
            if (window.history && window.history.pushState) {
                window.history.pushState('forward', null, "{{ URL::asset('/')}}");
                $(window).on('popstate', function() {
                    window.location.reload(history.back());
                });
            }*/


            //map location picker setting
            $('#us2').locationpicker({
                enableAutocomplete: true,
                enableReverseGeocode: true,
                radius: 50,
                zoom: 16,
                location: {
                    @if ($public)
                    latitude: "{{ $public->lat }}",
                    @else
                    latitude: 35.74484034043084,
                    @endif
                            @if ($public)
                    longitude: "{{ $public->lon }}",
                    @else
                    longitude: 51.37531033917844,
                    @endif
                },
                inputBinding: {
                    latitudeInput: $('#us2-lat'),
                    longitudeInput: $('#us2-lon'),
                    radiusInput: $('#us2-radius'),
                    locationNameInput: $('#us2-address')
                },
                onlocationnotfound: function (locationName) {
                    alert("not found");
                },
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    var addressComponents = $(this).locationpicker('map').location.addressComponents;
                    updateControls(addressComponents); //Data
                }
            });

            function updateControls(addressComponents) {
                console.log(addressComponents);
            }

            $('span.other-month').addClass('red');
                    @if ($public)
            var lim = "{{ $public->offday }}";
                    @else
            var lim = [0];
                    @endif
            var a = lim.split(",");
            for (var i = 0; i < a.length; i++) {
                if (a[i] == 1) {
                    var shan = 1;
                }
                if (a[i] == 2) {
                    var yek = 2;
                }
                if (a[i] == 3) {
                    var doo = 3;
                }
                if (a[i] == 4) {
                    var see = 4;
                }
                if (a[i] == 5) {
                    var char = 5;
                }
                if (a[i] == 6) {
                    var panj = 6;
                }
                if (a[i] == 7) {
                    var jom = 7;
                }
            }
            ;
            window.shan = shan !== undefined ? shan : 0;
            window.yek = yek !== undefined ? yek : 0;
            window.doo = doo !== undefined ? doo : 0;
            window.see = see !== undefined ? see : 0;
            window.char = char !== undefined ? char : 0;
            window.panj = panj !== undefined ? panj : 0;
            window.jom = jom !== undefined ? jom : 0;
            $('.modal').modal();
            $('.inline-example').persianDatepicker({
                inline: true,
                persian: {
                    "locale": "fa",
                    "showHint": true,
                    "leapYearMode": "algorithmic"
                },
                altField: '#inlineExampleAlt',
                altFormat: 'LLLL:X',
                autoClose: true,
                viewMode: 'day',
                toolbox: {
                    calendarSwitch: {
                        enabled: true
                    }
                },
                onSelect: function (unix) {
                    /* $('tr').find("td[data-unix="+unix+"]").addClass('disabled');*/
                },
                checkDate: function (unix) {
                    $('input#inlineExampleAlt').val(' ');
                    var shanbe = new persianDate(unix).day() != window.shan;
                    var yekshanbe = new persianDate(unix).day() != window.yek;
                    var doshanbe = new persianDate(unix).day() != window.doo;
                    var seshanbe = new persianDate(unix).day() != window.see;
                    var choharshanbe = new persianDate(unix).day() != window.char;
                    var panjshanbe = new persianDate(unix).day() != window.panj;
                    var jome = new persianDate(unix).day() != window.jom;
                    var befor = {!! json_encode($disable) !!};
                    var after = [];
                    for (i = 0; i < befor.length; i++) {
                        after[i] = befor[i].split('-');
                    }
                    console.log(after);
                    var crator = "";
                    for (j = 0; j < after.length; j++) {
                        crator += "new persianDate(unix).isSameDay(new persianDate([Number(after[" + j + "][0]),Number(after[" + j + "][1]),Number(after[" + j + "][2]) ])) || " + ' ';
                    }
                    var factor = crator.substring(0, crator.length - 4);
                    console.log(factor);
                    if (eval(factor)) {
                        return false && shanbe && yekshanbe && doshanbe && seshanbe && choharshanbe && panjshanbe && jome;
                    } else {
                        return true && shanbe && yekshanbe && doshanbe && seshanbe && choharshanbe && panjshanbe && jome;
                    }
                },
                navigator: {
                    scroll: {
                        enabled: true
                    }
                },
                @if ($public)
                maxDate: new persianDate().add('month',{{ $public->until }}).valueOf(),
                @else
                maxDate: new persianDate().add('month', 3).valueOf(),
                @endif
                minDate: new persianDate().subtract('month', 0).valueOf(),
                timePicker: {
                    enabled: false,
                    meridiem: {
                        enabled: true
                    }
                }
            });
        });

    </script>
@endsection