@extends('master.html')
@section('head')
@endsection
@section('body')
<div class="row">
	<div class="col l12">
		@if (Session::has('mesg'))
		<div class="container">
			<div class="row">
				<div class="col l12 s12 m12">
					<div class="card-panel green accent-4">
						<span class="white-text center-align">
							<h5>نوبت شما با موفقیت ثبت شد</h5>
						</span>
					</div>
					<div class="card-panel indigo accent-4">
						<span class="white-text center rtl">
							<h5>{{ Session::get('mesg')}}</h5>
						</span>
						<span class="white-text center-align">
							<h5>یک نسخه از اطلاعات فوق برای شما پیامک می شود</h5>
							<p>لطفا در ساعت تعیین شده به مطب مراجعه کنید . و بدون هماهنگی در ساعت دیگری مراجعه نکنید</p>
							<p>نیازی به کنسل کردن نوبت نیست . عدم حضور در ساعت مشخص شده به منزله کنسل شدن می باشد</p>
						</span>
					</div>
					<div class="card-panel indigo accent-4 center">
						<a href="{{ route('landing') }}" class="btn-flat white blue-grey-text ">خروج</a>
					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>
@endsection



