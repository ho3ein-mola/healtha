@extends('master/searchhtml')
@section('head')
    <style>


        .btn-floating{
            width: 150px !important;
            height: 150px !important;
        }
        #search-container{
            background-image: linear-gradient(to right, #43e97b 0%, #38f9d7 100%);
            border-bottom: 1px solid grey;
        }
        .carousel{
            height: 200px !important;
        }
        .gap {
            margin: 0 !important;
        }
        html, body{
            background: #f5f5f5;
        }
        .carousel .indicators .indicator-item{
            background: black !important;
        }
        .carousel .indicators .indicator-item.active{
            background: #18bc9c !important;
        }
    </style>
@endsection
@section('body')
    <div class="row " id="search-container">
        <div class="container center">
            <div class="row"></div>
            <div class="col s12 m12 l12 ">
                <a class="btn btn-floating btn-large black  pulse  "><img width="154px" class="material-icons" id="mat" src="/image/fdoc-logo.png" class="responsive-img" ></a>
            </div>
            <div class="row"></div>
            <h5 class="">فینداک | پزشک یاب هوشمند</h5>
            </div>
            <div class="row"></div>
            <div class="col s12 m12 l12 ">
               <div class="row">
                   <form action="" class="col s12 m12 l12">
                       <div class="container ">
                           <select class="browser-default col l4">
                               <option value="" disabled selected>فوق تخصص پزشک</option>
                               <option value="1">Option 1</option>
                               <option value="2">Option 2</option>
                               <option value="3">Option 3</option>
                           </select>
                           <select class="browser-default col l4">
                               <option value="" disabled selected>تخصص پزشک</option>
                               <option value="1">Option 1</option>
                               <option value="2">Option 2</option>
                               <option value="3">Option 3</option>
                           </select>
                           <select class="browser-default col l4">
                               <option value="" disabled selected>نوع پزشک</option>
                               <option value="1">Option 1</option>
                               <option value="2">Option 2</option>
                               <option value="3">Option 3</option>
                           </select>
                           <select class="browser-default col l6">
                               <option value="" disabled selected>استان</option>
                               <option value="1">Option 1</option>
                               <option value="2">Option 2</option>
                               <option value="3">Option 3</option>
                           </select>
                           <select class="browser-default col l6">
                               <option value="" disabled selected>شهر</option>
                               <option value="1">Option 1</option>
                               <option value="2">Option 2</option>
                               <option value="3">Option 3</option>
                           </select>
                       </div>
                       <div class="row"></div>
                       <div class="col s12 m12 l12 center">
                           <input type="submit" class="btn-flat black-text white" value="جست و جو">
                       </div>
                   </form>
               </div>
            </div>
        </div>
    </div>
    <div class="row gap">
        <div class="carousel carousel-slider center" data-indicators="true">
            <div class="carousel-fixed-item center ">
                <a href="{{ route("landing") }}" class="btn-flat waves-effect white black-text darken-text-2 bordera">اطلاعات بیشتر</a>
                <a href="{{ route("homepage") }}" class="btn-flat waves-effect white black-text darken-text-2 bordera">ثبت نام پزشک</a>
            </div>
            <div class="carousel-item grey lighten-4  black-text" href="#two!">
                <h2>از طریق پیامک اخرین وضعیت نوبت خود را پیگیری کنید</h2>
            </div>
            <div class="carousel-item grey lighten-4   black-text" href="#one!">
                <h2>پزشک خود را در کمترین زمان بیابید</h2>
            </div>
            <div class="carousel-item grey lighten-4  black-text" href="#two!">
                <h2>در کمترین زمان نوبت خود را ثبت کنید</h2>
            </div>
            <div class="carousel-item grey lighten-4  black-text" href="#two!">
                <h2>بدون نیاز به ثبت نام</h2>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <script>
        $(document).ready(function(){
            $('.carousel.carousel-slider').carousel({
                fullWidth: true
            });
            autoplay();
            function autoplay() {
                $('.carousel').carousel('next');
                setTimeout(autoplay, 5500);
            }
        });
    </script>
@endsection