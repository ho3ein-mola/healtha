@extends('master/html')


@section('head')
    <meta name="robots" content="index, follow">
    <style>
        .datepicker-plot-area .datepicker-day-view .table-days td span.other-month {
            background-color: #311b92;
            color: white;
            border: none;
            text-shadow: none;
        }

        .datepicker-plot-area .datepicker-day-view .table-days td.disabled span, .datepicker-plot-area .datepicker-year-view .year-item-disable,
        .datepicker-plot-area .datepicker-month-view .month-item-disable {
            background-color: #d32f2f !important;
            color: white;
            border: none;
            text-shadow: none;
            cursor: default;
        }

        .datepicker-plot-area .datepicker-day-view .table-days td span, .datepicker-plot-area .datepicker-year-view .year-item,
        .datepicker-plot-area .datepicker-month-view .month-item {
            font: 14px;
            background-color: #388e3c;
            color: white;
            border: 0;
            text-shadow: none;
        }
        .vis{
            visibility: hidden;
        }
    </style>
@endsection

@section('body')
    <div class="carousel carousel-slider center" data-indicators="true">
        <div class="carousel-fixed-item center">
            @if(Auth::user())
                <a href="{{ route('dashboard') }}" class="btn waves-effect white grey-text darken-text-2">داشبورد</a>
            @else
                <a href="{{ route('signinpage') }}" class="btn waves-effect white grey-text darken-text-2">ورود</a>
                <a href="{{ route('homepage') }}" class="btn waves-effect white grey-text darken-text-2">ثبت نام</a>
            @endif
        </div>
        <div class="carousel-item blue white-text" href="#two!">
            <h1>برای بیمار خود نوبت ثبت کنید</h1>
            <h5 class="white-text"> نوبت ثبت کنید , نوبت ها را مدریت کنید  </h5>
        </div>
        <div class="carousel-item amber black-text " href="#one!">
            <h1>بیمار ثبت کنید</h1>
            <h5 class="black-text">بیماران خود را یکبار ثبت کنید و همیشه با چند کلیک مدریت کنید </h5>
        </div>
        <div class="carousel-item green white-text" href="#three!">
            <h1>لینک عمومی بسازید</h1>
            <h5 class="white-text">لینک عمومی نوبت گیری بسازید و در اختیار بیمار قرار دهید .</h5>
        </div>
        <div class="carousel-item red white-text" href="#four!">
            <h1>تنظیمات پیشرفته</h1>
            <h5 class="white-text">به وسیله تنظیمات پیشرفته لینک عمومی بیماران خود را محدود کنید</h5>
        </div>
    </div>

    <div class="row">
      <div class="col s12 m12 l12">
      </div>
  </div>



    <div class="row">
        <h1 class="center black-text">
            خدمات ما برای چه کسی مناسب است ؟
        </h1>
    </div>

    <div class="row">

        <div class="col s12 m12 l4">
            <div class="card blue lighten-5">
                <div class="card-content black-text rtl">
                    <span class="card-title">پزشکان</span>
                    <p>همه ی پزشکان عمومی و متخصص و فوق تخصص جهت منظم سازی کارها و مدریت بهتر زمان می توانند از این سیستم بهره ببرند </p>
                </div>
            </div>
        </div>

        <div class="col s12 m12 l4">
            <div class="card deep-purple lighten-5">
                <div class="card-content black-text rtl">
                    <span class="card-title">دندان پزشکان</span>
                    <p>همه ی دندان پزشکان عمومی و متخصص و فوق تخصص جهت منظم سازی کارها و مدریت بهتر زمان می توانند از این سیستم بهره ببرند </p>

                </div>
            </div>
        </div>

        <div class="col s12 m12 l4">
            <div class="card  lime lighten-4">
                <div class="card-content black-text rtl">
                    <span class="card-title">روانپزشکان</span>
                    <p>همه ی روانپزشکان عمومی و متخصص و فوق تخصص جهت منظم سازی کارها و مدریت بهتر زمان می توانند از این سیستم بهره ببرند </p>
                </div>
            </div>
        </div>

    </div>
  <div class="row">
      <h1 class="center black-text">
          تقویم نوبت دهی قدرتمند و برنامه پذیر
      </h1>
  </div>
  <div class="row">
         <div class="input-field col s12 m12 l6">
             <input name="datepicker" id="inlineExampleAlt"
                    class="datepicker-demo datepicker  right-align  hide">
             <div class="input-field col s12 m12 l12 center">
                 <div class="btn-flat red darken-2 white-text">ظرفیت تکمیل</div>
                 <div class="btn-flat green darken-2 white-text">ظرفیت باز</div>
                 <div class="btn-flat deep-purple darken-4 white-text">ظرفیت باز در ماه اینده</div>
             </div>
             <div class="input-field col s12 m12 l12 center">
                 <div class="inline-example"></div>
             </div>
         </div>
        <div class="col l6 rtl center">
            <br><br>
            <h5>بستن تقویم برای روز های گذشته</h5>
            <h5>بستن نوبت در روز های تایین شده</h5>
            <h5>تأیین ظرفیت برای هر روز</h5>
            <h5>بسته شدن روز در صورت تکمیل ظرفیت </h5>
            <h5>بستن تقویم برای چند ماه اینده به بعد</h5>
            <p class="black-text amber  phonesize">در تقویم رو به رو روز های دوشنبه و پنجشنبه بسته شده و فقط تا سه ماه اینده امکان رزرو وجود دارد</p>
        </div>
  </div>
  <div class="row">
      <h1 class="center black-text rtl">
          در محل کار کامپیوتر یا لپتاپ ندارید?
      </h1>
  </div>

  <div class="black">
      <div class="row">
          <div class="col s12 m12 l6 red accent-3 super-small center">
              <h5 class="center-align phonesize black-text">در خانه یا کافی نت نوبت های فردا را پرینت کنید</h5>
              <i class="large-icon material-icons black-text">print</i>
          </div>
          <div class="col s12 m12 l6 green accent-3 super-small center">
              <h5 class="center-align phonesize black-text">با موبایل و تبلت خود بیماران را مدریت کنید</h5>
              <i class="large-icon material-icons black-text">phone_iphone</i>
          </div>
      </div>
      <div class="row"></div>
      <div class="row">
          <h1 class="center white-text">
              اطلاع رسانی در کمترین زمان
          </h1>
      </div>
      <div class="row">
          <div class="col s12 m12 l6  deep-purple accent-3 super center">
              <h5 class="center-align phonesize white-text">بعد از ثبت  نوبت یک پیامک  ارسال می شود</h5>
              <i class="xlarge-icon material-icons white-text">chat</i>
          </div>
          <div class="col s12 m12 l6 green accent-3 super center">
              <h5 class="center-align phonesize black-text">هرگونه تغییر در نوبت را به بیماران اطلاع دهید</h5>
              <i class="xlarge-icon material-icons black-text">markunread</i>
          </div>
      </div>
      <div class="row">
          <h1 class="center white-text">
              همه جا در دسترس
          </h1>
      </div>
      <div class="row">
          <div class="col s12 m12 l3 green accent-3 super-small center">
              <h5 class="center-align phonesize black-text">موبایل</h5>
              <i class="large-icon material-icons black-text">phone_android</i>
          </div>
          <div class="col s12 m12 l3 red accent-3 super-small center">
              <h5 class="center-align phonesize black-text">تبلت</h5>
              <i class="large-icon material-icons black-text">tablet</i>
          </div>
          <div class="col s12 m12 l3 orange accent-3 super-small center">
              <h5 class="center-align phonesize black-text">لپتاپ</h5>
              <i class="large-icon material-icons black-text">laptop</i>
          </div>
          <div class="col s12 m12 l3 teal accent-3 super-small center">
              <h5 class="center-align phonesize black-text">کامپیوتر</h5>
              <i class="large-icon material-icons black-text">desktop_windows</i>
          </div>
      </div>

  </div>


@endsection


@section('script')
<script>
    $(document).ready(function(){
        $('.inline-example').persianDatepicker({
            inline: true,
            persian: {
                "locale": "fa",
                "showHint": true,
                "leapYearMode": "algorithmic"
            },
            altField: '#inlineExampleAlt',
            altFormat: 'LLLL:X',
            autoClose: true,
            viewMode: 'day',
            toolbox: {
                calendarSwitch: {
                    enabled: true
                }
            },
            onSelect: function (unix) {
                /* $('tr').find("td[data-unix="+unix+"]").addClass('disabled');*/
            },
            checkDate: function (unix) {
                return new persianDate(unix).day() != 3 &&  new persianDate(unix).day() != 6;
            },
            navigator: {
                scroll: {
                    enabled: true
                }
            },
            maxDate: new persianDate().add('month', 3).valueOf(),
            minDate: new persianDate().subtract('month', 0).valueOf(),
            timePicker: {
                enabled: false,
                meridiem: {
                    enabled: true
                }
            }
        });
        $('.carousel.carousel-slider').carousel({
            padding: 200,
            fullWidth: true
        });
        autoplay()
        function autoplay() {
            $('.carousel').carousel('next');
            setTimeout(autoplay, 5500);
        }
    });
</script>
@endsection