$(document).ready(function () {
    $('#province').val('');
    jQuery.get('../json/phds.json', function (obj) {
        $.each(obj, function(i, value) {
            $('select#prefix').material_select();
            $("select[name=prefix]").append($('<option>').text(value.name).attr('value', value.id));
            $('select#prefix').change(function() {
                var selstate = $( "#prefix option:selected" ).val();
                //specialist select get from json
                if (value.id.trim() == 2 && selstate.trim() == 2){
                    $("select[name=sub_specialist]").empty();
                    $("select[name=sub_specialist]").append($('<option>').text('تخصص خود را انتخاب کنید').attr('value', '00'));
                    $("select#sub_specialist").prop('disabled', true);
                    var subselstate = $( "#prefix option:selected" ).val();
                    jQuery.get('../json/specialists.json', function (res2) {
                        var elements = [];
                        for (var i = 0; i < res2.length; i++) {
                            if (res2[i].phds_id == 2){
                                if (subselstate == 2){
                                    elements.push($('<option>').text(res2[i].name).attr('value', res2[i].id));
                                }
                                if (elements.length > 0){
                                    $('select#specialist').material_select();
                                    $("select#specialist").prop('disabled', false);
                                    $("select#specialist").empty().append(elements);
                                }
                                if (elements.length == 0){
                                    $('select#specialist').material_select();
                                    $("select#specialist").prop('disabled', true);
                                    $("select#specialist").empty().append('<option value="00">برای رشته ی شما فوق تخصص ثبت نشده است.</option>');
                                }
                            }

                        }
                    });
                }

                if (value.id.trim() == 5 && selstate.trim() == 5){
                    $("select[name=sub_specialist]").empty();
                    $("select[name=sub_specialist]").append($('<option>').text('تخصص خود را انتخاب کنید').attr('value', '00'));
                    $("select#sub_specialist").prop('disabled', true);
                    var subselstate = $( "#prefix option:selected" ).val();
                    jQuery.get('../json/specialists.json', function (res2) {
                        var elements = [];
                        for (var i = 0; i < res2.length; i++) {
                            if (res2[i].phds_id == 5){
                                if (subselstate == 5){
                                    elements.push($('<option>').text(res2[i].name).attr('value', res2[i].id));
                                }
                                if (elements.length > 0){
                                    $('select#specialist').material_select();
                                    $("select#specialist").prop('disabled', false);
                                    $("select#specialist").empty().append(elements);
                                }
                                if (elements.length == 0){
                                    $('select#specialist').material_select();
                                    $("select#specialist").prop('disabled', true);
                                    $("select#specialist").empty().append('<option value="00">برای رشته ی شما فوق تخصص ثبت نشده است.</option>');
                                }
                            }

                        }
                    });
                }

                if (selstate.trim() == 1 || selstate.trim() == 4 || selstate.trim() == 6){
                    $('select#specialist').material_select();
                    $("select[name=specialist]").empty();
                    $("select[name=specialist]").append($('<option>').text('تخصص خود را انتخاب کنید').attr('value', '00'));
                    $("select#specialist").prop('disabled', true);
                    $('select#sub_specialist').material_select();
                    $("select[name=sub_specialist]").empty();
                    $("select[name=sub_specialist]").append($('<option>').text('تخصص خود را انتخاب کنید').attr('value', '00'));
                    $("select#sub_specialist").prop('disabled', true);
                }

                if (value.id.trim() == 3 && selstate.trim() == 3){

                    $("select[name=sub_specialist]").empty();
                    $("select[name=sub_specialist]").append($('<option>').text('تخصص خود را انتخاب کنید').attr('value', '00'));
                    $("select#sub_specialist").prop('disabled', true);
                    var subselstate = $( "#prefix option:selected" ).val();
                    jQuery.get('../json/specialists.json', function (res2) {
                        var elements = [];
                        for (var i = 0; i < res2.length; i++) {
                            if (res2[i].phds_id == 2){
                                if (subselstate == 3){
                                    elements.push($('<option>').text(res2[i].name).attr('value', res2[i].id));
                                }
                                if (elements.length > 0){
                                    $('select#specialist').material_select();
                                    $("select#specialist").prop('disabled', false);
                                    $("select#specialist").empty().append(elements);
                                }
                                if (elements.length == 0){
                                    $('select#specialist').material_select();
                                    $("select#specialist").prop('disabled', true);
                                    $("select#specialist").empty().append('<option value="00">برای رشته ی شما فوق تخصص ثبت نشده است.</option>');
                                }
                            }
                        }
                    });
                }

            });
        });
    });

    $('select#specialist').change(function() {
        var subselstate = $( "#specialist option:selected" ).val();
        jQuery.get('../json/sub_specialists.json', function (res2) {
            var elements = [];
            for (var i = 0; i < res2.length; i++) {
                if (res2[i].specialist_id == subselstate){
                    elements.push($('<option>').text(res2[i].name).attr('value', res2[i].id));
                }
                if (elements.length > 0){

                    $('select#sub_specialist').material_select();
                    $("select#sub_specialist").prop('disabled', false);
                    $("select#sub_specialist").empty().append(elements);
                }
                if (elements.length == 0){
                    $('select#sub_specialist').material_select();
                    $("select#sub_specialist").prop('disabled', true);
                    $("select#sub_specialist").empty().append('<option value="00">برای رشته ی شما فوق تخصص ثبت نشده است.</option>');
                }
            }

        });
    });


    //province code
    var code;
    //data temp for making autocomplate array
    var data = [];
    //parse json from remote server
    jQuery.get('../json/province.json', function (obj) {
        for (i = 0; i < obj.RECORDS.length; i++) {
            data[obj.RECORDS[i].name] = null
        }
        //start province auto complate
        $('input.autocomplete').autocomplete({
            data: data,
            limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
            onAutocomplete: function (val1) {
                //province call back
                $.each(obj.RECORDS, function (i, v) {
                    //make array for province
                    if (v.name.toString().trim() == val1.toString().trim()) {
                        $('input[name=mapstring]').val(v.name);
                        code = v.id;
                        $("input[name=provinceout]").val(code);
                    }
                });
                //enable city picker after setting code
                $("input[name=city").prop('disabled', false);
                //focus on city
                setTimeout(function () {
                    $("input[name=city]").focus();
                    $("input[name=city]").val('');
                }, 100);
                //get city json
                var data2 = [];
                jQuery.get('../json/city.json', function (obj2) {
                    //make array for city
                    for (i = 0; i < obj2.RECORDS.length; i++) {
                        if (obj2.RECORDS[i].province == code) {
                            data2[obj2.RECORDS[i].name] = null
                        }
                    }
                    //start city autocomplate
                    $('input.autocomplete2').autocomplete({
                        data: data2,
                        limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                        onAutocomplete: function (val2) {
                            //city callback
                            $.each(obj2.RECORDS, function (i, v) {
                                //check if city name equal to json name to get
                                if (v.name.toString().trim() == val2.toString().trim()) {
                                    var ccode = v.id;
                                    $("input[name=cityout]").val(ccode);
                                    $('input[name=mapstring]').val(val1 + ' ' + val2);
                                    setTimeout(function () {
                                        $("input#us2-address").focus();
                                        $("input#us2-address").trigger(jQuery.Event('keydown', {which: '13'.charCodeAt(0)}));
                                    }, 100);
                                }
                            });
                        },
                        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                    });
                });
            },
            minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
        });
        //end province callback
    });

    //open date picker
    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
    });


    //select  initializing
    $('select').material_select();

    //mutiple selector data extractor
    $("select").change(function () {
        var valu = $('select#multiple').val();
        $('#offday').val(valu);
    });

    //map location picker setting
    $('#us2').locationpicker({
        enableAutocomplete: true,
        enableReverseGeocode: true,
        radius: 50,
        zoom: 16,
        location: {
            latitude: 35.74484034043084,
            longitude: 51.37531033917844
        },
        inputBinding: {
            latitudeInput: $('#us2-lat'),
            longitudeInput: $('#us2-lon'),
            radiusInput: $('#us2-radius'),
            locationNameInput: $('#us2-address')
        },
        onlocationnotfound: function (locationName) {
            alert("not found");
        },
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            var addressComponents = $(this).locationpicker('map').location.addressComponents;
            updateControls(addressComponents); //Data
        }
    });

    function updateControls(addressComponents) {
        console.log(addressComponents);
    }

    //set lat
    $('input[name="lat"]').change(function () {
        $('input[name="lat"]').val($(this).val());
    });
    //set lon
    $('input[name="lon"]').change(function () {
        $('input[name="lon"]').val($(this).val());
    });
});